Document: ifddam
Title: Debian ifddam Manual
Author: <insert document author here>
Abstract: This manual describes what ifddam is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/ifddam/ifddam.sgml.gz

Format: postscript
Files: /usr/share/doc/ifddam/ifddam.ps.gz

Format: text
Files: /usr/share/doc/ifddam/ifddam.text.gz

Format: HTML
Index: /usr/share/doc/ifddam/html/index.html
Files: /usr/share/doc/ifddam/html/*.html
