apt update
apt -y install software-properties-common
add-apt-repository --yes universe
apt update
apt -y install qt6-base-dev
apt-get -y install libqt6svg6-dev
apt-get -y install libqt6svg6
apt-get -y install libglu1-mesa libglu1-mesa-dev
apt -y install build-essential
apt -y install gfortran
apt -y install fftw*
apt-get -y  install hdf
apt-get -y  install  hdf5
apt-get -y  install  hdf5-static
apt-get -y install libhdf5-dev
