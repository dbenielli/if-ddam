#include "cdmChi2ConfigDialog.h"


Chi2ConfigDialog::Chi2ConfigDialog(QWidget *parent,Qt::WindowFlags fl,
					 Options *_options)
: QDialog(parent,fl)
{
   this->setWindowTitle(tr("Chi2 properties"));
   options = _options;
   layout = new QVBoxLayout(this);
   
   okButton = new QPushButton("Ok");
   connect(okButton, SIGNAL(clicked()), this, SLOT(ok()));
   layout->addWidget(okButton);

   tabchi2config = new QTabWidget();
   layout->addWidget(tabchi2config);
   
   QSignalMapper *signalMapper = new QSignalMapper(this);

   QLOG_DEBUG() << "Chi2ConfigDialog::Chi2ConfigDialog> Object number: " 
	       << options->getObjectNumber();

   for (int i = 0 ; i < options->getObjectNumber(); i++) {
      
       QWidget *widget = new QWidget(this);
       QFormLayout *chi2configlayout = new QFormLayout(widget);
       QScrollArea *scrollArea = new QScrollArea(this);
       scrollArea->setWidgetResizable(true);
       scrollArea->setWidget(widget);
       
       chi2xxxr.push_back(new QLineEdit(QString::number(real(options->getChi2xxx().at(i)))));
       chi2xxxi.push_back(new QLineEdit(QString::number(imag(options->getChi2xxx().at(i)))));
       chi2xyxr.push_back(new QLineEdit(QString::number(real(options->getChi2xyx().at(i)))));
       chi2xyxi.push_back(new QLineEdit(QString::number(imag(options->getChi2xyx().at(i)))));
       chi2xzxr.push_back(new QLineEdit(QString::number(real(options->getChi2xzx().at(i)))));
       chi2xzxi.push_back(new QLineEdit(QString::number(imag(options->getChi2xzx().at(i)))));
       chi2yxxr.push_back(new QLineEdit(QString::number(real(options->getChi2yxx().at(i)))));
       chi2yxxi.push_back(new QLineEdit(QString::number(imag(options->getChi2yxx().at(i)))));
       chi2yyxr.push_back(new QLineEdit(QString::number(real(options->getChi2yyx().at(i)))));
       chi2yyxi.push_back(new QLineEdit(QString::number(imag(options->getChi2yyx().at(i)))));
       chi2yzxr.push_back(new QLineEdit(QString::number(real(options->getChi2yzx().at(i)))));
       chi2yzxi.push_back(new QLineEdit(QString::number(imag(options->getChi2yzx().at(i)))));
       chi2zxxr.push_back(new QLineEdit(QString::number(real(options->getChi2zxx().at(i)))));
       chi2zxxi.push_back(new QLineEdit(QString::number(imag(options->getChi2zxx().at(i)))));
       chi2zyxr.push_back(new QLineEdit(QString::number(real(options->getChi2zyx().at(i)))));
       chi2zyxi.push_back(new QLineEdit(QString::number(imag(options->getChi2zyx().at(i)))));
       chi2zzxr.push_back(new QLineEdit(QString::number(real(options->getChi2zzx().at(i)))));
       chi2zzxi.push_back(new QLineEdit(QString::number(imag(options->getChi2zzx().at(i)))));
       
       chi2xxyr.push_back(new QLineEdit(QString::number(real(options->getChi2xxy().at(i)))));
       chi2xxyi.push_back(new QLineEdit(QString::number(imag(options->getChi2xxy().at(i)))));
       chi2xyyr.push_back(new QLineEdit(QString::number(real(options->getChi2xyy().at(i)))));
       chi2xyyi.push_back(new QLineEdit(QString::number(imag(options->getChi2xyy().at(i)))));
       chi2xzyr.push_back(new QLineEdit(QString::number(real(options->getChi2xzy().at(i)))));
       chi2xzyi.push_back(new QLineEdit(QString::number(imag(options->getChi2xzy().at(i)))));
       chi2yxyr.push_back(new QLineEdit(QString::number(real(options->getChi2yxy().at(i)))));
       chi2yxyi.push_back(new QLineEdit(QString::number(imag(options->getChi2yxy().at(i)))));
       chi2yyyr.push_back(new QLineEdit(QString::number(real(options->getChi2yyy().at(i)))));
       chi2yyyi.push_back(new QLineEdit(QString::number(imag(options->getChi2yyy().at(i)))));
       chi2yzyr.push_back(new QLineEdit(QString::number(real(options->getChi2yzy().at(i)))));
       chi2yzyi.push_back(new QLineEdit(QString::number(imag(options->getChi2yzy().at(i)))));
       chi2zxyr.push_back(new QLineEdit(QString::number(real(options->getChi2zxy().at(i)))));
       chi2zxyi.push_back(new QLineEdit(QString::number(imag(options->getChi2zxy().at(i)))));
       chi2zyyr.push_back(new QLineEdit(QString::number(real(options->getChi2zyy().at(i)))));
       chi2zyyi.push_back(new QLineEdit(QString::number(imag(options->getChi2zyy().at(i)))));
       chi2zzyr.push_back(new QLineEdit(QString::number(real(options->getChi2zzy().at(i)))));
       chi2zzyi.push_back(new QLineEdit(QString::number(imag(options->getChi2zzy().at(i)))));
       
       chi2xxzr.push_back(new QLineEdit(QString::number(real(options->getChi2xxz().at(i)))));
       chi2xxzi.push_back(new QLineEdit(QString::number(imag(options->getChi2xxz().at(i)))));
       chi2xyzr.push_back(new QLineEdit(QString::number(real(options->getChi2xyz().at(i)))));
       chi2xyzi.push_back(new QLineEdit(QString::number(imag(options->getChi2xyz().at(i)))));
       chi2xzzr.push_back(new QLineEdit(QString::number(real(options->getChi2xzz().at(i)))));
       chi2xzzi.push_back(new QLineEdit(QString::number(imag(options->getChi2xzz().at(i)))));
       chi2yxzr.push_back(new QLineEdit(QString::number(real(options->getChi2yxz().at(i)))));
       chi2yxzi.push_back(new QLineEdit(QString::number(imag(options->getChi2yxz().at(i)))));
       chi2yyzr.push_back(new QLineEdit(QString::number(real(options->getChi2yyz().at(i)))));
       chi2yyzi.push_back(new QLineEdit(QString::number(imag(options->getChi2yyz().at(i)))));
       chi2yzzr.push_back(new QLineEdit(QString::number(real(options->getChi2yzz().at(i)))));
       chi2yzzi.push_back(new QLineEdit(QString::number(imag(options->getChi2yzz().at(i)))));
       chi2zxzr.push_back(new QLineEdit(QString::number(real(options->getChi2zxz().at(i)))));
       chi2zxzi.push_back(new QLineEdit(QString::number(imag(options->getChi2zxz().at(i)))));
       chi2zyzr.push_back(new QLineEdit(QString::number(real(options->getChi2zyz().at(i)))));
       chi2zyzi.push_back(new QLineEdit(QString::number(imag(options->getChi2zyz().at(i)))));
       chi2zzzr.push_back(new QLineEdit(QString::number(real(options->getChi2zzz().at(i)))));
       chi2zzzi.push_back(new QLineEdit(QString::number(imag(options->getChi2zzz().at(i)))));
       
       QBoxLayout   *chi2xxxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xxxlayout->addWidget(chi2xxxr.at(i));
       chi2xxxlayout->addWidget(chi2xxxi.at(i));
       QBoxLayout   *chi2xyxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xyxlayout->addWidget(chi2xyxr.at(i));
       chi2xyxlayout->addWidget(chi2xyxi.at(i));
       QBoxLayout   *chi2xzxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xzxlayout->addWidget(chi2xzxr.at(i));
       chi2xzxlayout->addWidget(chi2xzxi.at(i));
       QBoxLayout   *chi2yxxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yxxlayout->addWidget(chi2yxxr.at(i));
       chi2yxxlayout->addWidget(chi2yxxi.at(i));
       QBoxLayout   *chi2yyxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yyxlayout->addWidget(chi2yyxr.at(i));
       chi2yyxlayout->addWidget(chi2yyxi.at(i));
       QBoxLayout   *chi2yzxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yzxlayout->addWidget(chi2yzxr.at(i));
       chi2yzxlayout->addWidget(chi2yzxi.at(i));
       QBoxLayout   *chi2zxxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zxxlayout->addWidget(chi2zxxr.at(i));
       chi2zxxlayout->addWidget(chi2zxxi.at(i));
       QBoxLayout   *chi2zyxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zyxlayout->addWidget(chi2zyxr.at(i));
       chi2zyxlayout->addWidget(chi2zyxi.at(i));
       QBoxLayout   *chi2zzxlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zzxlayout->addWidget(chi2zzxr.at(i));
       chi2zzxlayout->addWidget(chi2zzxi.at(i));
       
       QBoxLayout   *chi2xxylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xxylayout->addWidget(chi2xxyr.at(i));
       chi2xxylayout->addWidget(chi2xxyi.at(i));
       QBoxLayout   *chi2xyylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xyylayout->addWidget(chi2xyyr.at(i));
       chi2xyylayout->addWidget(chi2xyyi.at(i));
       QBoxLayout   *chi2xzylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xzylayout->addWidget(chi2xzyr.at(i));
       chi2xzylayout->addWidget(chi2xzyi.at(i));
       QBoxLayout   *chi2yxylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yxylayout->addWidget(chi2yxyr.at(i));
       chi2yxylayout->addWidget(chi2yxyi.at(i));
       QBoxLayout   *chi2yyylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yyylayout->addWidget(chi2yyyr.at(i));
       chi2yyylayout->addWidget(chi2yyyi.at(i));
       QBoxLayout   *chi2yzylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yzylayout->addWidget(chi2yzyr.at(i));
       chi2yzylayout->addWidget(chi2yzyi.at(i));
       QBoxLayout   *chi2zxylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zxylayout->addWidget(chi2zxyr.at(i));
       chi2zxylayout->addWidget(chi2zxyi.at(i));
       QBoxLayout   *chi2zyylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zyylayout->addWidget(chi2zyyr.at(i));
       chi2zyylayout->addWidget(chi2zyyi.at(i));
       QBoxLayout   *chi2zzylayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zzylayout->addWidget(chi2zzyr.at(i));
       chi2zzylayout->addWidget(chi2zzyi.at(i));
       
       QBoxLayout   *chi2xxzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xxzlayout->addWidget(chi2xxzr.at(i));
       chi2xxzlayout->addWidget(chi2xxzi.at(i));
       QBoxLayout   *chi2xyzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xyzlayout->addWidget(chi2xyzr.at(i));
       chi2xyzlayout->addWidget(chi2xyzi.at(i));
       QBoxLayout   *chi2xzzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2xzzlayout->addWidget(chi2xzzr.at(i));
       chi2xzzlayout->addWidget(chi2xzzi.at(i));
       QBoxLayout   *chi2yxzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yxzlayout->addWidget(chi2yxzr.at(i));
       chi2yxzlayout->addWidget(chi2yxzi.at(i));
       QBoxLayout   *chi2yyzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yyzlayout->addWidget(chi2yyzr.at(i));
       chi2yyzlayout->addWidget(chi2yyzi.at(i));
       QBoxLayout   *chi2yzzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2yzzlayout->addWidget(chi2yzzr.at(i));
       chi2yzzlayout->addWidget(chi2yzzi.at(i));
       QBoxLayout   *chi2zxzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zxzlayout->addWidget(chi2zxzr.at(i));
       chi2zxzlayout->addWidget(chi2zxzi.at(i));
       QBoxLayout   *chi2zyzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zyzlayout->addWidget(chi2zyzr.at(i));
       chi2zyzlayout->addWidget(chi2zyzi.at(i));
       QBoxLayout   *chi2zzzlayout = new QBoxLayout(QBoxLayout::LeftToRight);
       chi2zzzlayout->addWidget(chi2zzzr.at(i));
       chi2zzzlayout->addWidget(chi2zzzi.at(i));
   
       chi2configlayout->addRow("Chi2xxx:",chi2xxxlayout);
       chi2configlayout->addRow("Chi2xyx:",chi2xyxlayout);
       chi2configlayout->addRow("Chi2xzx:",chi2xzxlayout);
       chi2configlayout->addRow("Chi2yxx:",chi2yxxlayout);
       chi2configlayout->addRow("Chi2yyx:",chi2yyxlayout);
       chi2configlayout->addRow("Chi2yzx:",chi2yzxlayout);
       chi2configlayout->addRow("Chi2zxx:",chi2zxxlayout);
       chi2configlayout->addRow("Chi2zyx:",chi2zyxlayout);
       chi2configlayout->addRow("Chi2zzx:",chi2zzxlayout);
       
       chi2configlayout->addRow("Chi2xxy:",chi2xxylayout);
       chi2configlayout->addRow("Chi2xyy:",chi2xyylayout);
       chi2configlayout->addRow("Chi2xzy:",chi2xzylayout);
       chi2configlayout->addRow("Chi2yxy:",chi2yxylayout);
       chi2configlayout->addRow("Chi2yyy:",chi2yyylayout);
       chi2configlayout->addRow("Chi2yzy:",chi2yzylayout);
       chi2configlayout->addRow("Chi2zxy:",chi2zxylayout);
       chi2configlayout->addRow("Chi2zyy:",chi2zyylayout);
       chi2configlayout->addRow("Chi2zzy:",chi2zzylayout);
       
       chi2configlayout->addRow("Chi2xxz:",chi2xxzlayout);
       chi2configlayout->addRow("Chi2xyz:",chi2xyzlayout);
       chi2configlayout->addRow("Chi2xzz:",chi2xzzlayout);
       chi2configlayout->addRow("Chi2yxz:",chi2yxzlayout);
       chi2configlayout->addRow("Chi2yyz:",chi2yyzlayout);
       chi2configlayout->addRow("Chi2yzz:",chi2yzzlayout);
       chi2configlayout->addRow("Chi2zxz:",chi2zxzlayout);
       chi2configlayout->addRow("Chi2zyz:",chi2zyzlayout);
       chi2configlayout->addRow("Chi2zzz:",chi2zzzlayout);
       
       tabchi2config->setCurrentIndex(tabchi2config->addTab(scrollArea,
		"object " + 
		QString::number(i+1)));
   }

}
Chi2ConfigDialog::~Chi2ConfigDialog()
{
  QLOG_DEBUG ( ) << "Deleting Chi2ConfigDialog";
}

void
Chi2ConfigDialog::ok()
{
  this->updateOptions();
  this->close();
}
void
Chi2ConfigDialog::updateOptions()
{
   QVector<dcmplx> _chi2xxx;
   QVector<dcmplx> _chi2xyx;
   QVector<dcmplx> _chi2xzx;
   QVector<dcmplx> _chi2yxx;
   QVector<dcmplx> _chi2yyx;
   QVector<dcmplx> _chi2yzx;
   QVector<dcmplx> _chi2zxx;
   QVector<dcmplx> _chi2zyx;
   QVector<dcmplx> _chi2zzx;
   
   QVector<dcmplx> _chi2xxy;
   QVector<dcmplx> _chi2xyy;
   QVector<dcmplx> _chi2xzy;
   QVector<dcmplx> _chi2yxy;
   QVector<dcmplx> _chi2yyy;
   QVector<dcmplx> _chi2yzy;
   QVector<dcmplx> _chi2zxy;
   QVector<dcmplx> _chi2zyy;
   QVector<dcmplx> _chi2zzy;
   
   QVector<dcmplx> _chi2xxz;
   QVector<dcmplx> _chi2xyz;
   QVector<dcmplx> _chi2xzz;
   QVector<dcmplx> _chi2yxz;
   QVector<dcmplx> _chi2yyz;
   QVector<dcmplx> _chi2yzz;
   QVector<dcmplx> _chi2zxz;
   QVector<dcmplx> _chi2zyz;
   QVector<dcmplx> _chi2zzz;
   

    for (int i = 0 ; i < options->getObjectNumber(); i++) {
      
       _chi2xxx.push_back(dcmplx(chi2xxxr.at(i)->text().toDouble(),chi2xxxi.at(i)->text().toDouble()));
       _chi2xyx.push_back(dcmplx(chi2xyxr.at(i)->text().toDouble(),chi2xyxi.at(i)->text().toDouble()));
       _chi2xzx.push_back(dcmplx(chi2xzxr.at(i)->text().toDouble(),chi2xzxi.at(i)->text().toDouble()));
       _chi2yxx.push_back(dcmplx(chi2yxxr.at(i)->text().toDouble(),chi2yxxi.at(i)->text().toDouble()));
       _chi2yyx.push_back(dcmplx(chi2yyxr.at(i)->text().toDouble(),chi2yyxi.at(i)->text().toDouble()));
       _chi2yzx.push_back(dcmplx(chi2yzxr.at(i)->text().toDouble(),chi2yzxi.at(i)->text().toDouble()));
       _chi2zxx.push_back(dcmplx(chi2zxxr.at(i)->text().toDouble(),chi2zxxi.at(i)->text().toDouble()));
       _chi2zyx.push_back(dcmplx(chi2zyxr.at(i)->text().toDouble(),chi2zyxi.at(i)->text().toDouble()));
       _chi2zzx.push_back(dcmplx(chi2zzxr.at(i)->text().toDouble(),chi2zzxi.at(i)->text().toDouble()));
       
       _chi2xxy.push_back(dcmplx(chi2xxyr.at(i)->text().toDouble(),chi2xxyi.at(i)->text().toDouble()));
       _chi2xyy.push_back(dcmplx(chi2xyyr.at(i)->text().toDouble(),chi2xyyi.at(i)->text().toDouble()));
       _chi2xzy.push_back(dcmplx(chi2xzyr.at(i)->text().toDouble(),chi2xzyi.at(i)->text().toDouble()));
       _chi2yxy.push_back(dcmplx(chi2yxyr.at(i)->text().toDouble(),chi2yxyi.at(i)->text().toDouble()));
       _chi2yyy.push_back(dcmplx(chi2yyyr.at(i)->text().toDouble(),chi2yyyi.at(i)->text().toDouble()));
       _chi2yzy.push_back(dcmplx(chi2yzyr.at(i)->text().toDouble(),chi2yzyi.at(i)->text().toDouble()));
       _chi2zxy.push_back(dcmplx(chi2zxyr.at(i)->text().toDouble(),chi2zxyi.at(i)->text().toDouble()));
       _chi2zyy.push_back(dcmplx(chi2zyyr.at(i)->text().toDouble(),chi2zyyi.at(i)->text().toDouble()));
       _chi2zzy.push_back(dcmplx(chi2zzyr.at(i)->text().toDouble(),chi2zzyi.at(i)->text().toDouble()));
       
       _chi2xxz.push_back(dcmplx(chi2xxzr.at(i)->text().toDouble(),chi2xxzi.at(i)->text().toDouble()));
       _chi2xyz.push_back(dcmplx(chi2xyzr.at(i)->text().toDouble(),chi2xyzi.at(i)->text().toDouble()));
       _chi2xzz.push_back(dcmplx(chi2xzzr.at(i)->text().toDouble(),chi2xzzi.at(i)->text().toDouble()));
       _chi2yxz.push_back(dcmplx(chi2yxzr.at(i)->text().toDouble(),chi2yxzi.at(i)->text().toDouble()));
       _chi2yyz.push_back(dcmplx(chi2yyzr.at(i)->text().toDouble(),chi2yyzi.at(i)->text().toDouble()));
       _chi2yzz.push_back(dcmplx(chi2yzzr.at(i)->text().toDouble(),chi2yzzi.at(i)->text().toDouble()));
       _chi2zxz.push_back(dcmplx(chi2zxzr.at(i)->text().toDouble(),chi2zxzi.at(i)->text().toDouble()));
       _chi2zyz.push_back(dcmplx(chi2zyzr.at(i)->text().toDouble(),chi2zyzi.at(i)->text().toDouble()));
       _chi2zzz.push_back(dcmplx(chi2zzzr.at(i)->text().toDouble(),chi2zzzi.at(i)->text().toDouble()));

   }
   
   options->setChi2xxx(_chi2xxx);
   options->setChi2xyx(_chi2xyx);
   options->setChi2xzx(_chi2xzx);
   options->setChi2yxx(_chi2yxx);
   options->setChi2yyx(_chi2yyx);
   options->setChi2yzx(_chi2yzx);
   options->setChi2zxx(_chi2zxx);
   options->setChi2zyx(_chi2zyx);
   options->setChi2zzx(_chi2zzx);
   
   options->setChi2xxy(_chi2xxy);
   options->setChi2xyy(_chi2xyy);
   options->setChi2xzy(_chi2xzy);
   options->setChi2yxy(_chi2yxy);
   options->setChi2yyy(_chi2yyy);
   options->setChi2yzy(_chi2yzy);
   options->setChi2zxy(_chi2zxy);
   options->setChi2zyy(_chi2zyy);
   options->setChi2zzy(_chi2zzy);
   
   options->setChi2xxz(_chi2xxz);
   options->setChi2xyz(_chi2xyz);
   options->setChi2xzz(_chi2xzz);
   options->setChi2yxz(_chi2yxz);
   options->setChi2yyz(_chi2yyz);
   options->setChi2yzz(_chi2yzz);
   options->setChi2zxz(_chi2zxz);
   options->setChi2zyz(_chi2zyz);
   options->setChi2zzz(_chi2zzz);
   
}
void 
Chi2ConfigDialog::closeEvent(QCloseEvent* event)
{
  event->accept();  
  QLOG_DEBUG ( ) << "Closing Chi2ConfigDialog";
  this->close();
}
void 
Chi2ConfigDialog::showWarning(QString message) {
  QMessageBox::warning(this, "Error:", message);
}
