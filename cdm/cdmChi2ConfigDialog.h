#ifndef CHI2CONFIGDIALOG_H
#define CHI2CONFIGDIALOG_H

#include <QtSql>
#include <QtGui>
#if QT_VERSION < 0x050000
#else
#include <QtWidgets>
#endif
#include "QsLog.h"
#include "cdmOptions.h"

class Chi2ConfigDialog : public QDialog 
{
  
  Q_OBJECT
    
    public:
  
  Chi2ConfigDialog(QWidget *parent = 0, Qt::WindowFlags fl = Qt::SubWindow,
		      Options *_options = 0);
  virtual ~Chi2ConfigDialog();

 public slots:

  void ok();

  signals:

  void handleSelectionChanged(int index);

 protected:
  void closeEvent(QCloseEvent *event);
  void showWarning(QString message);

 private:

  QVBoxLayout *layout;

  void         updateOptions();

  Options      *options;

  QVector<QLineEdit*>    chi2xxxr;
  QVector<QLineEdit*>    chi2xxxi;
  QVector<QLineEdit*>    chi2xxyr;
  QVector<QLineEdit*>    chi2xxyi;
  QVector<QLineEdit*>    chi2xxzr;
  QVector<QLineEdit*>    chi2xxzi;
  QVector<QLineEdit*>    chi2yxxr;
  QVector<QLineEdit*>    chi2yxxi;
  QVector<QLineEdit*>    chi2yxyr;
  QVector<QLineEdit*>    chi2yxyi;
  QVector<QLineEdit*>    chi2yxzr;
  QVector<QLineEdit*>    chi2yxzi;
  QVector<QLineEdit*>    chi2zxxr;
  QVector<QLineEdit*>    chi2zxxi;
  QVector<QLineEdit*>    chi2zxyr;
  QVector<QLineEdit*>    chi2zxyi;
  QVector<QLineEdit*>    chi2zxzr;
  QVector<QLineEdit*>    chi2zxzi;
  
  QVector<QLineEdit*>    chi2xyxr;
  QVector<QLineEdit*>    chi2xyxi;
  QVector<QLineEdit*>    chi2xyyr;
  QVector<QLineEdit*>    chi2xyyi;
  QVector<QLineEdit*>    chi2xyzr;
  QVector<QLineEdit*>    chi2xyzi;
  QVector<QLineEdit*>    chi2yyxr;
  QVector<QLineEdit*>    chi2yyxi;
  QVector<QLineEdit*>    chi2yyyr;
  QVector<QLineEdit*>    chi2yyyi;
  QVector<QLineEdit*>    chi2yyzr;
  QVector<QLineEdit*>    chi2yyzi;
  QVector<QLineEdit*>    chi2zyxr;
  QVector<QLineEdit*>    chi2zyxi;
  QVector<QLineEdit*>    chi2zyyr;
  QVector<QLineEdit*>    chi2zyyi;
  QVector<QLineEdit*>    chi2zyzr;
  QVector<QLineEdit*>    chi2zyzi;
  
  QVector<QLineEdit*>    chi2xzxr;
  QVector<QLineEdit*>    chi2xzxi;
  QVector<QLineEdit*>    chi2xzyr;
  QVector<QLineEdit*>    chi2xzyi;
  QVector<QLineEdit*>    chi2xzzr;
  QVector<QLineEdit*>    chi2xzzi;
  QVector<QLineEdit*>    chi2yzxr;
  QVector<QLineEdit*>    chi2yzxi;
  QVector<QLineEdit*>    chi2yzyr;
  QVector<QLineEdit*>    chi2yzyi;
  QVector<QLineEdit*>    chi2yzzr;
  QVector<QLineEdit*>    chi2yzzi;
  QVector<QLineEdit*>    chi2zzxr;
  QVector<QLineEdit*>    chi2zzxi;
  QVector<QLineEdit*>    chi2zzyr;
  QVector<QLineEdit*>    chi2zzyi;
  QVector<QLineEdit*>    chi2zzzr;
  QVector<QLineEdit*>    chi2zzzi;

  QTabWidget             *tabchi2config;
  QPushButton            *okButton;
};



#endif
