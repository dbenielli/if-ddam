#include "cdmOptionsWindow.h"
#include "cdmMain.h"

OptionsWindow::OptionsWindow( QMainWindow *_parent, Options *_options, Qt::WindowFlags fl)
  : QMainWindow( _parent, fl )
{
  parent = _parent;
  optionswidget = NULL;
  optionstable = NULL;
  options = _options;
  
  QWidget *centralWidget = new QWidget(this);
  this->setCentralWidget(centralWidget);
  vboxlayout = new QVBoxLayout();
  centralWidget->setLayout(vboxlayout);
  this->setWindowTitle("Configurations saved");
  this->setMinimumHeight(300);
  this->setMinimumWidth(400);
  
  optionstitle = new QLabel("Select a configuration:");
  vboxlayout->addWidget(optionstitle);
  optionsview = new QTableView();
  QPalette palette = optionsview->palette();
  palette.setBrush(QPalette::Base, Qt::transparent);
  optionsview->setPalette(palette);
  optionsview->setItemDelegate(new ColorDelegate());
  optionsview->setEditTriggers(QAbstractItemView::AllEditTriggers);
  //optionsview->setSelectionBehavior(QAbstractItemView::SelectRows);
  vboxlayout->addWidget(optionsview);

  saveButton = new QPushButton("Save",this);
  saveButton->setToolTip("Enter a new name or select a line to save the current calculation");
  saveButton->setFixedHeight(30);
  saveButton->setFixedWidth(150);
  QObject::connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
  vboxlayout->addWidget(saveButton);
  removeButton = new QPushButton("Delete",this);
  removeButton->setToolTip("Select a line to delete");
  removeButton->setFixedHeight(30);
  removeButton->setFixedWidth(150);
  QObject::connect(removeButton, SIGNAL(clicked()), this, SLOT(remove()));
  vboxlayout->addWidget(removeButton);
  loadButton = new QPushButton("Load",this);
  loadButton->setToolTip("Select a line to load");
  loadButton->setFixedHeight(30);
  loadButton->setFixedWidth(150);
  QObject::connect(loadButton, SIGNAL(clicked()), this, SLOT(load()));
  vboxlayout->addWidget(loadButton);
  exportButton = new QPushButton("Export",this);
  exportButton->setToolTip("Select a line to export");
  exportButton->setFixedHeight(30);
  exportButton->setFixedWidth(150);
  QObject::connect(exportButton, SIGNAL(clicked()), this, SLOT(tofile()));
  vboxlayout->addWidget(exportButton);
  initOptionsTbl();
} 

OptionsWindow::~OptionsWindow()
{
  QLOG_DEBUG ( ) << "Deleting OptionsWindow";
  if (optionstable){ delete optionstable; optionstable = NULL;}
}

void OptionsWindow::closeEvent(QCloseEvent* event)
{
  event->accept();  
  QLOG_DEBUG ( ) << "Closing OptionsWindow";
  this->hide();
}
void OptionsWindow::initOptionsTbl() {
  QTabWidget *tabwidget = NULL;
  QMainWindow *currentWindow = NULL;
  tabwidget = (QTabWidget*) parent->findChild<QTabWidget*>("TabWidget");
  if (tabwidget) {
   currentWindow = (QMainWindow*)tabwidget->currentWidget();
   optionswidget = currentWindow->findChild<OptionsWidget *>("Options");
  }
  /*if (optionswidget)
    options = optionswidget->options;
  else {
   if (options) {delete options; options = NULL;}
   options = new Options();
   options->initDb();
  } */
  if (optionstable) {delete optionstable; optionstable = NULL;}
  dbpath = "options.db3";
  optionstable = new ManifestModel(this,QSqlDatabase::database(dbpath));
  optionstable->setTable("options_tbl");
  optionstable->setEditStrategy(QSqlTableModel::OnManualSubmit);
  optionsview->setModel(optionstable);
  optionstable->setFilter("name not like 'new'");
  optionstable->select();
  optionstable->insertRow(optionstable->rowCount());
  optionsview->resizeColumnsToContents();
  optionsview->resizeRowsToContents();
  optionsview->setCurrentIndex (optionsview->model()->index(optionstable->rowCount() - 1, 
		    0, QModelIndex()));
}
void 
OptionsWindow::save(){
  if (optionsview->selectionModel()->hasSelection() == false) {
     this->showWarning("Select a valid configuration to save");
     return;
  }
  QModelIndex selected = optionsview->selectionModel()->currentIndex();
  QSqlRecord record = optionstable->record(selected.row());
  QString name = record.value(0).toString();
  QString description = record.value(1).toString();
  if (name =="") {
   this->showWarning("Select a valid configuration to save");
   return;
  }
  QTabWidget *tabwidget = (QTabWidget*) parent->findChild<QTabWidget*>("TabWidget");
  QMainWindow *currentWindow = (QMainWindow*)tabwidget->currentWidget();
  optionswidget = currentWindow->findChild<OptionsWidget *>("Options");
  if (optionswidget)
    optionswidget->updateOptions();
  else
    options->loadDb(name);
  options->saveDb(name,description);
  initOptionsTbl();
}
void 
OptionsWindow::load(){
  if (optionsview->selectionModel()->hasSelection() == false) {
    this->showWarning("Select a valid configuration to load");
  return;
  }
  QModelIndexList selectedlist = optionsview->selectionModel()->selectedIndexes();
  for (int i = selectedlist.size() - 1 ; i >=0 ; i--) {
    QModelIndex selected = selectedlist.at(i);
    QSqlRecord record = optionstable->record(selected.row());
    QString name = record.value(0).toString();
    options->loadDb(name);
    CdmMain *mainwindow = (CdmMain*) parent; 
    mainwindow->openWidget(options);
 }
 this->hide();
}
void 
OptionsWindow::remove(){
  if (optionsview->selectionModel()->hasSelection() == false) {
    this->showWarning("Select a valid configuration to delete");
    return;
  }
  QModelIndexList selectedlist = optionsview->selectionModel()->selectedIndexes();
  for (int i = selectedlist.size() - 1 ; i >=0 ; i--) {
    QModelIndex selected = selectedlist.at(i);
    QSqlRecord record = optionstable->record(selected.row());
    QString name = record.value(0).toString();
    QString description = record.value(1).toString();
    if (name =="") {
      this->showWarning("Select a valid configuration to delete");
      return;
    }
    options->removeDb(name);
  }
  initOptionsTbl();
}
void 
OptionsWindow::updatewindow(){
  initOptionsTbl();
}
void 
OptionsWindow::tofile(){
 if (optionsview->selectionModel()->hasSelection() == false) {
    this->showWarning("Select a valid configuration to export");
    return;
  }
  QModelIndex selected = optionsview->selectionModel()->currentIndex();
  QSqlRecord record = optionstable->record(selected.row());
  QString name = record.value(0).toString();
  if (name =="") {
   this->showWarning("Select a valid configuration to export");
   return;
  }
  options->loadDb(name);
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                     name + ".opt", tr("Options (*.opt)"));
  QFile optfile(fileName);
  if (!optfile.open(QIODevice::WriteOnly | QIODevice::Text))
     return;
  QTextStream opt(&optfile);
  // Fill ASCII options here
  opt << "Computation of the near field [0=Rigorous, 1=Renor. Born], 2=Born, 3=Born order 1, 4 Scalar appr.: u*.Gu, 5 Homogeneous, 6 Homogeneous+scalar: exp(ikr)/r, 7 Homogeneous+scalar: u*.Gu: " << options->getNrig();
  opt << "Reread from a file: " << options->getNread() ;
  opt << "Database file: " << options->getNmatlab() ;
  opt << "H5 file used: " << options->getH5File() ;
  opt << "Advanced interface used: " << options->getAdvancedinterface() ;
  opt << "------------------------------------------" ;
  opt << "Illumination properties options:" ;
  opt << " Wavelength:" << options->getWavelength() ;
  opt << " P0:" << options->getP0() ;
  opt << " W0:" << options->getW0() ; 
  opt << " Green function [0=Rigorous, 1=Level 1, 2=Level 2, 3=Level 3, 4=Level 4]:" << options->getNinterp() ;
  opt << " Beam:" << options->getBeam() ;
  if (options->getBeam() == "Circular plane wave") {
    opt << "  incidence angle (theta with respect to z):" << options->getIncidenceangle_theta_z() ;
    opt << "  incidence angle (phi with respect to x):" << options->getIncidenceangle_phi_x() ;
    opt << "  polarization L (-1) R (1):" << options->getPolarizationRL() ;
  }
  else if (options->getBeam() == "Linear plane wave") {
    opt << "  incidence angle (theta with respect to z):" << options->getIncidenceangle_theta_z() ;
    opt << "  incidence angle (phi with respect to x):" << options->getIncidenceangle_phi_x() ;
    opt << "  polarization TE->TM (0->1) (2=x) (3=y):" << options->getPolarizationTM() ;
  }
  else if (options->getBeam() == "Circular Gaussian" ) {
    opt << "  incidence angle (theta with respect to z):" << options->getIncidenceangle_theta_z() ;
    opt << "  incidence angle (phi with respect to x):" << options->getIncidenceangle_phi_x() ;
    opt << "  polarization L (-1) R (1):" << options->getPolarizationRL() ;
    opt << "  gaussian X:" << options->getXgaus() ;
    opt << "  gaussian Y:" << options->getYgaus() ;
    opt << "  gaussian Z:" << options->getZgaus() ;
  }
  else if (options->getBeam() == "Linear Gaussian"  ) {
    opt << "  incidence angle (theta with respect to z):" << options->getIncidenceangle_theta_z() ;
    opt << "  incidence angle (phi with respect to x):" << options->getIncidenceangle_phi_x() ;
    opt << "  polarization TM (1) TE (0):" << options->getPolarizationTM() ;
    opt << "  gaussian X:" << options->getXgaus() ;
    opt << "  gaussian Y:" << options->getYgaus() ;
    opt << "  gaussian Z:" << options->getZgaus() ;
  }
  else if (options->getBeam() == "Speckle"  ) {
    opt << "  polarization TM (1) TE (0):" << options->getPolarizationTM() ;
    opt << "  Seed :" << options->getSpeckseed() ;
    opt << "  gaussian X:" << options->getXgaus() ;
    opt << "  gaussian Y:" << options->getYgaus() ;
    opt << "  gaussian Z:" << options->getZgaus() ;
  }
  else if (options->getBeam() == "Multiple wave"  ) {
    for (int i = 0 ; i < options->getWaveMultiNumber(); i++) {
      opt << "  incidence angle (theta with respect to z):" << options->getThetam().at(i) ;
      opt << "  incidence angle (phi with respect to x):" << options->getPhim().at(i) ;
      opt << "  polarization TM (1) TE (0):" << options->getPpm().at(i) ;
      opt << "  Magnitude field real:" << (real(options->getE0m().at(i))) ;
      opt << "  Magnitude field imag:" << (imag(options->getE0m().at(i))) ;
    }
  }
  else if (options->getBeam() == "Antenna" ) {
    opt << "  incidence angle (theta with respect to z): " << options->getIncidenceangle_theta_z() ;
    opt << "  orientation angle (phi with respect to x)" << options->getIncidenceangle_phi_x() ;
    opt << "  position X: " << options->getXgaus() ;
    opt << "  position Y: " << options->getYgaus() ;
    opt << "  position Z: " << options->getZgaus() ;
  }
  opt << "------------------------------------------" ;  
  opt << "Object properties options:" ;
  for (int i = 0 ; i < options->getObjectNumber(); i++) {
    opt << " object : " << i << " : " << options->getObject() ;
    if (options->getObject() == "sphere" || options->getObject() == "multiple spheres") {
      opt << " radius:" << options->getSphereradius().at(i) ;
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
    }
    else if (options->getObject() == "inhomogeneous sphere") {
      opt << " radius:" << options->getSphereradius().at(i) ;
      opt << " seed:" << options->getSphereseed() ;
      opt << " coherence length:" << options->getSpherecoherencelength() ;
      opt << " standard deviation:" << options->getSpherestandardev() ;
    }
    else if (options->getObject() == "concentric spheres") {
      if (i == 0) {
        opt << " position X:" << options->getPositionx().at(i) ;
        opt << " position Y:" << options->getPositiony().at(i) ;
        opt << " position Z:" << options->getPositionz().at(i) ;
      }
      opt << " radius:" << options->getSphereradius().at(i) ;
    }
    else if (options->getObject() == "cube") {
      opt << " cube side:" << options->getCubeside() ;
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
    }
    else if (options->getObject() == "inhomogeneous cuboid (length)") {
      opt << " cube side X:" << options->getCubesidex() ;
      opt << " cube side Y:" << options->getCubesidey() ;
      opt << " cube side Z:" << options->getCubesidez() ;
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
      opt << " seed:" << options->getSphereseed() ;
      opt << " coherence length:" << options->getSpherecoherencelength() ;
      opt << " standard deviation:" << options->getSpherestandardev() ;
    }
    else if (options->getObject() == "inhomogeneous cuboid (meshsize)") {
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
      opt << " number of subunit X:" << options->getNxx() ;
      opt << " number of subunit Y:" << options->getNyy() ;
      opt << " number of subunit Z:" << options->getNzz() ;
      opt << " meshsize:" << options->getMeshsize() ;
      opt << " seed:" << options->getSphereseed() ;
      opt << " coherence length:" << options->getSpherecoherencelength() ;
      opt << " standard deviation:" << options->getSpherestandardev() ;
    }
    else if (options->getObject() == "cuboid (length)") {
      opt << " cube side X:" << options->getCubesidex() ;
      opt << " cube side Y:" << options->getCubesidey() ;
      opt << " cube side Z:" << options->getCubesidez() ;
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
      opt << " theta:" << options->getThetaobj() ;
      opt << " phi:" << options->getPhiobj() ;
      opt << " psi:" << options->getPsiobj() ;
    }
    else if (options->getObject() == "cuboid (meshsize)") {
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
      opt << " number of subunit X:" << options->getNxx() ;
      opt << " number of subunit Y:" << options->getNyy() ;
      opt << " number of subunit Z:" << options->getNzz() ;
      opt << " meshsize:" << options->getMeshsize() ;      
    }
       else if (options->getObject() == "random spheres (length)") {
      opt << " cube side X:" << options->getCubesidex() ;
      opt << " cube side Y:" << options->getCubesidey() ;
      opt << " cube side Z:" << options->getCubesidez() ;
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
      opt << "  radius:" << options->getSphereradius().at(i) ;
      opt << "  seed:" << options->getSphereseed() ;
      opt << "  density:" << options->getDensity() ;
     }
    else if (options->getObject() == "random spheres (meshsize)") {
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
      opt << " number of subunit X:" << options->getNxx() ;
      opt << " number of subunit Y:" << options->getNyy() ;
      opt << " number of subunit Z:" << options->getNzz() ;
      opt << " meshsize:" << options->getMeshsize() ;      
      opt << " radius:" << options->getSphereradius().at(i) ;
      opt << " seed:" << options->getSphereseed() ;
      opt << " density:" << options->getDensity() ;
     }
    else if (options->getObject() == "ellipsoid") {
      opt << " half axe A:" << options->getDemiaxea() ;
      opt << " half axe B:" << options->getDemiaxeb() ;
      opt << " half axe C:" << options->getDemiaxec() ;
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
    }
    else if (options->getObject() == "cylinder") {
      opt << " radius:" << options->getSphereradius().at(i) ;
      opt << " height:" << options->getHauteur() ;
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
      opt << " theta:" << options->getThetaobj() ;
      opt << " phi:" << options->getPhiobj() ;
    }
    else if (options->getObject() == "multiple spheres") {
      opt << " radius:" << options->getSphereradius().at(i) ;
      opt << " position X:" << options->getPositionx().at(i) ;
      opt << " position Y:" << options->getPositiony().at(i) ;
      opt << " position Z:" << options->getPositionz().at(i) ;
    }
  }
    opt << " anisotropy:" << options->getAnisotropy() ;
  for (int i = 0 ; i < options->getObjectNumber(); i++) {
    if ( options->getAnisotropy() == "iso" ) {
      opt << "  material" << i << ":" << options->getMaterial()[i] ;
      opt << "  epsilon real:" << QString::number(real(options->getEpsilon().at(i))) ;
      opt << "  epsilon imag:" << QString::number(imag(options->getEpsilon().at(i))) ;
    }
    else if ( options->getAnisotropy() == "ani" ) {
      opt << "  epsilon11 real:" << QString::number(real(options->getEpsilon11().at(i))) ;
      opt << "  epsilon11 imag:" << QString::number(imag(options->getEpsilon11().at(i))) ;
      opt << "  epsilon12 real:" << QString::number(real(options->getEpsilon12().at(i))) ;
      opt << "  epsilon12 imag:" << QString::number(imag(options->getEpsilon12().at(i))) ;
      opt << "  epsilon13 real:" << QString::number(real(options->getEpsilon13().at(i))) ;
      opt << "  epsilon13 imag:" << QString::number(imag(options->getEpsilon13().at(i))) ;
      opt << "  epsilon21 real:" << QString::number(real(options->getEpsilon21().at(i))) ;
      opt << "  epsilon21 imag:" << QString::number(imag(options->getEpsilon21().at(i))) ;
      opt << "  epsilon22 real:" << QString::number(real(options->getEpsilon22().at(i))) ;
      opt << "  epsilon22 imag:" << QString::number(imag(options->getEpsilon22().at(i))) ;
      opt << "  epsilon23 real:" << QString::number(real(options->getEpsilon23().at(i))) ;
      opt << "  epsilon23 imag:" << QString::number(imag(options->getEpsilon23().at(i))) ;
      opt << "  epsilon31 real:" << QString::number(real(options->getEpsilon31().at(i))) ;
      opt << "  epsilon31 imag:" << QString::number(imag(options->getEpsilon31().at(i))) ;
      opt << "  epsilon32 real:" << QString::number(real(options->getEpsilon32().at(i))) ;
      opt << "  epsilon32 imag:" << QString::number(imag(options->getEpsilon32().at(i))) ;
      opt << "  epsilon33 real:" << QString::number(real(options->getEpsilon33().at(i))) ;
      opt << "  epsilon33 imag:" << QString::number(imag(options->getEpsilon33().at(i))) ;
    }
  }
  opt << "------------------------------------------" ;
  opt << "Study options:" ;
  opt << "Dipole/epsilon checked:" << options->getDipolepsilon() ;
  opt << "Farfield checked:" << options->getFarfield() ;
  if ( options->getFarfield() ) {
    opt << " cross section checked:" << options->getCrosssection() ;
    opt << " cross section + poynting checked:" << options->getCrosssectionpoynting() ;
    opt << " quick computation:" << options->getQuickdiffract() ;
    opt << " ntheta:" << options->getNtheta() ;
    opt << " nphi:" << options->getNphi() ;   
    opt << " Emissivity:" << options->getNenergie() ;   
  }

 opt << "Microscopy checked:" << options->getMicroscopy() ;
  if ( options->getMicroscopy() ) {
    opt << " Numerical aperture in reflexion:" << options->getNAR() ;
    opt << " Numerical aperture in transmission:" << options->getNAT() ;
    opt << " Numerical aperture for condenser:" << options->getNAinc() ;
    opt << " Central aperture :" << options->getNAinc2() ;
    opt << " Shieren position x :" << options->getKcnax() ;
    opt << " Shieren position y :" << options->getKcnay() ;    
    opt << " Magnification:" << options->getGross() ;
    opt << " Side [0=Side kz<0, 1=Both side (NA=1), 2=Side kz<0]:" << options->getNside() ;
    opt << "Microscope [0=Holographic, 1=Brightfield, 2=Darkfield & phase, 3=Schieren, 4=Darkfield cone & phase, 5=Phase experimental]:" << options->getNtypemic() ;
    opt << " Quick with FFT:" << options->getMicroscopyFFT() ;
    opt << " Focal plane ref:" << options->getZlensr() ;
    opt << " Focal plane trans:" << options->getZlenst() ;
  }
  
  opt << "Force checked:" << options->getForce() ;
  if ( options->getForce() ) {
    opt << "  optical force checked:" << options->getOpticalforce() ;
    opt << "  optical force density checked:" << options->getOpticalforcedensity() ;
    opt << "  optical torque checked:" << options->getOpticaltorque() ;
    opt << "  optical torque density checked:" << options->getOpticaltorquedensity() ;
  }
  opt << "Nearfield checked:" << options->getNearfield() ;
  if ( options->getNearfield() ) {
    opt << " local field checked:" << options->getLocalfield() ;
    opt << " macroscopic field checked:" << options->getMacroscopicfield() ;
    int nproche = options->getNproche();
    opt << " range of study (0,1,2):" << options->getNproche() ;
    opt << "  Discretization: " << options->getDiscretization() ;
    opt << " nxm:" << options->getNxm() ;
    opt << " nym:" << options->getNym() ;
    opt << " nzm:" << options->getNzm() ;
    opt << " nxmp:" << options->getNxmp() ;
    opt << " nymp:" << options->getNymp() ;
    opt << " nzmp:" << options->getNzmp() ;
  }
  opt << "------------------------------------------" ;
  opt << "Numerical parameters options:" ;
  opt << " Tolerance:" << options->getTolerance() ;
  opt << " Maximum of iteration: " << options->getNlim() ;
  opt << " Methode:" << options->getMethodeit() ;  
  opt << " Preconditionner: " << options->getPrecon() ;
  opt << " Initial guess: " << options->getNinitest() ;
  opt << " Polarizability:" << options->getPolarizability() ;
  opt << " FFT:" << options->getnfft2d() ;
  optfile.close();
  initOptionsTbl();
}
void 
OptionsWindow::showWarning(QString message) {
  QMessageBox::warning(this, "Error:", message);
}

