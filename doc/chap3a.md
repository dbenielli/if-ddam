Properties of the illumination  {#chap3}
==============================

Introduction
------------

In the section properties of the illumination, the field *Wavelength*
enables us to enter the using wavelength in vacuum. This one is entered
in nanometer. The field $P_0$ enables to enter the power of the laser
beam in Watt. The field $W_0$ in nanometer enables to enter for a plane
wave the radius of the laser beam and for a Gaussian beam, the waist of
the beam.

Note that the beam always propagates in the direction of the positive
$z$ axis, hence for $k_z>0$ whatever the beam chosen.

Beam
----

### Introduction

There are six beams predefined, their propagation direction is always
defined in the same way, with two angles $\theta$ and $\varphi$, except
for he speckle. They are connected to the given direction by the wave
vector as follows: $$\begin{aligned}
k_x & = & k_0 \sin \theta \cos\varphi \\
k_y & = & k_0 \sin \theta \sin\varphi \\
k_z & = & k_0 \cos \theta \end{aligned}$$ where
$\boldsymbol{k}_0=(k_x,k_y,k_z)$ is the wave vector parallel to the
direction of the incident beam and $k_0$ the wave number, see
Fig. [1.1](#faisceau){reference-type="ref" reference="faisceau"}.

![Definition of the beam's direction](faisceau.eps){#faisceau
width="8.0cm"}

For the polarization, we use the plane $(x,y)$ as referential surface.
Then, we can determine a polarization TM ($p$) and TE ($s$) with the
presence of a surface, see Fig. [1.2](#pola){reference-type="ref"
reference="pola"} or a polarization along he $x$ or $y$ axis, depending
of the beam.

![Definition of the beam's polarization.](pola.eps){#pola width="8.0cm"}

The frame $(x,y,z)$ is used as an absolute referential. We define the
polarization vector $\boldsymbol{s}$ corresponding to $TE$ polarisation
and $\boldsymbol{p}$ corresponding to $TM$ as, $$\begin{aligned}
\boldsymbol{s} & = & \frac{\hat{\boldsymbol{z}} \times \boldsymbol{k}_0 }{\| \hat{\boldsymbol{z}}
  \times \boldsymbol{k}_0\|} \\
\boldsymbol{p} & = & \frac{\boldsymbol{k}_0 \times \boldsymbol{s}}{\|\boldsymbol{k}_0 \times \boldsymbol{s}\|}
.  \end{aligned}$$

### Linear plane wave 

*Linear plane wave* is a plane wave linearly polarized. The first line
is relative to $\theta$ and the second to $\varphi$. The third line is
connected to the polarization, polarization TM(1) or TE(0) as
$\boldsymbol{E}_0=E_p \boldsymbol{p} +E_s\boldsymbol{s}$ with
$E_p={\rm pola}$ and $E_s=\sqrt{1-\rm {pola}^2}$. Note that the
polarization is not necessarily purely in TE or TM: ${\rm pola}\in[0~1]$
such as $E^2_{\rm TM}={\rm pola}^2E^2$ and
$E^2_{\rm TE}=(1-{\rm pola}^2)E^2$.

Notice that if one wants that
$\boldsymbol{E}_0 \cdot \hat{\boldsymbol{x}} =0$
($\boldsymbol{E_0 } \cdot \hat{\boldsymbol{y}} =0$), one can choose
pola=3 (2) and the code will compute the right value of pola to get the
polarization asked.

Note that the phase is always taken null at the origin of the frame,
with ${\rm Irradiance}=P_0/S$ where $S=\pi w_0^2$ is the surface of the
beam and $E_0=\sqrt{2 {\rm Irradiance}/c/\varepsilon_0}$.

### Circular plane wave 

*pwavecircular* is a plane wave circularly polarized. The first line is
relative to $\theta$ and the second to $\varphi$. The third line is
connected to the polarization that we can choose right (1) or left (-1)
circular.

Note that the phase is taken null at the origin of the frame, with
${\rm Irradiance}=P_0/S$ where $S=\pi w_0^2$ is the surface of the beam
and $E_0=\sqrt{2 {\rm Irradiance}/c/\varepsilon_0}$.

### Multiple plane wave

*Multiple wave* consists to take many planes waves. The first thing to
do is to choose the number of plane wave, and then for each plane wave
we choose $\theta$ and $\varphi$ and the polarization. We have to write
also the complex magnitude of each plane wave. The sum of the power of
all the plane wave is equal to $P_0$.

### Linear Gaussian beam

*Linear Gaussian* is a Gaussian wave polarized linearly. The first line
is relative to $\theta$ and the second to $\varphi$. The third line is
connected to the angle between the polarization and the $x$ axis (0) or
along the $y$ axis (90).

The three following lines help to fix the position of the centre
$(x_0,y_0,z_0)$ of the waist in nanometers in the frame $(x,y,z)$.

Note that this Gaussian beam may have a very weak waist, because it is
calculated without any approximation through an angular spectrum
representation done with FFT with always $k_z>0$ whatever the
inclination $\theta$ of the Beam. The definition of the waist, for a
beam propagating along the $z$ axis is :[@Agrawal_JOSA_79]
$$\begin{aligned}
E(x,y,0)= E_0 e^{-\rho^2/(2 w_0^2)}, \end{aligned}$$ with
$\rho=\sqrt{x^2+y^2}$. Then to compute a Gaussian beam polarized along
the $x$ axis, we have the magnitude of the Fourier component as:
$$\begin{aligned}
\boldsymbol{A}(k_x,k_y)= E_0 (k_z\boldsymbol{i}-k_x\boldsymbol{k} )
\frac{1}{\sqrt{k_x^2+k_z^2}} w_0 e^{-(k_x^2+k_y^2)w_0^2/2}, \end{aligned}$$
then we compute the reference field,
$\boldsymbol{A}_{\rm ref} (k_x,k_y,z)$, through the multilayer system,
and the reference field reads: $$\begin{aligned}
\boldsymbol{E}_{\rm ref}(x,y,z)= \int \int_{k_0} \boldsymbol{A}_{\rm ref}
(k_x,k_y,z) e^{i(k_x (x-x_0)+k_y (y-y_0)-k_zz_0 )} {\rm d}
\boldsymbol{k}_{\parallel}. \end{aligned}$$

### Circular Gaussian

*Circular Gaussian* is a Gaussian wave circularly polarized. The first
line is relative to $\theta$ and the second to $\varphi$. The third line
is connected to the polarization that we can choose right (1) or left
(-1) circular.

The next three lines enable us to fix the position of the centre of the
waist in nanometers in the frame $(x,y,z)$.

Note that this Gaussian wave may have a very weak waist, because it is
calculated without any approximation through a plane wave spectrum.

### Speckle

*Speckle* is done as the Gaussian beam with a FFT but with a magnitude
with a random phase. For a speckle polarized along the $x$ axis the
magnitude of the Fourier component is: $$\begin{aligned}
\boldsymbol{A}(k_x,k_y)= E_0 (k_z\boldsymbol{i}-k_x\boldsymbol{k} )
\frac{1}{\sqrt{k_x^2+k_z^2}} e^{i \varphi} , \end{aligned}$$ where
$\varphi$ is random variable between 0 and $2\pi$. Then we compute the
reference field, $\boldsymbol{A}_{\rm ref} (k_x,k_y,z)$, through the
multilayer system, and the reference field reads: $$\begin{aligned}
\boldsymbol{E}_{\rm ref}(x,y,z)=\int \int_{k_0 {\rm NA}} \boldsymbol{A}_{\rm ref}
(k_x,k_y,z) e^{i(k_x (x-x_0)+k_y (y-y_0)-k_zz_0 )} {\rm d} \boldsymbol{k}_{\parallel}, \end{aligned}$$
where ${\rm NA}$ is the numerical aperture of the microscope.
$\boldsymbol{r}_0$ permits to shift the speckle and the seed to change
the distribution of the speckle.

### Antenna

It is possible to illuminate the object with a dipolar antenna. For this
illumination the orientation of the dipole must be given ($\theta$ angle
betwwen the dipole and the $z$ axis, and $\varphi$ angle that the
projection of the dipole on the $(x,y)$ plane done with the $x$ axis)
and its position in $x$, $y$ and $z$.

The location of the dipole can be outside the object or inside the
object. When the antenna is in the object, it is then placed at the
position of an element of discretization, as close as possible to the
position given by the user.

The power given in the code ($P_0$) then corresponds to the total power
radiated by the antenna.

Note that the radiation of a dipole is such that there are many energy
radiated on the edge of the numerical aperture of the microscope. It is
therefore necessary to take many points in the numerical aperture if you
want to be precise.

Moreover, we should pay attention as the conservation of energy it is
not necessarily respected. Indeed, a dipole which emits 1 W in vacuum,
will not emit 1 W when it is close to an object or an interface. For
example for the configuration shown
Fig. [1.3](#antenna){reference-type="ref" reference="antenna"}(a), the
normalized power is a function of the distance from the antenna to the
interface, see Fig. .[1.3](#antenna){reference-type="ref"
reference="antenna"}(b). The radiation of a dipole is not an intrinsic
quantity but depends on its environment. If one traces the inverse of
the total power radiated by the dipole, it is proportional to the
lifetime of a fluorescent atom, see 
Fig. [1.3](#antenna){reference-type="ref" reference="antenna"}(c). For
more details, please refer to the items 
[@Rahmani_PRA_97; @Rahmani_PRA_01], but we can see that when a dipole
approaches an interface then its evanescent waves are passing in
propagating waves which increases the total power radiated when the
dipole is very close to the interface.

![(a) Configuration: Antenna in a vacuum above an interface of
permittivity $\varepsilon=20$ (b) Total radiated power versus the
distance from the antenna to the interface. (c) Lifetime of a
fluorescent atom as a function of its distance from the
interface.](antenna.eps){#antenna width="15.0cm"}

### Arbitrary wave

In the case of an arbitrary field, the characteristic are determined by
the user. In other words, he has to create the field himself, and it is
mandatory to create these files respecting the chosen conventions by the
code.

The description of the discretization of the incident field is done
within a file which is asked for when we click on *Props*. For example,
for the real part of the component $x$ of the field, it has to be
constructed as follows:

nx,ny,nz

dx,dy,dz

xmin,ymin,zmin

-   nx is the number of meshsize according to the axis $x$

-   ny is the number of meshsize according to the axis $y$

-   nz is the number of meshsize according to the axis $z$

-   dx is the step according to the axis $x$

-   dy is the step according to the axis $y$

-   dz is the step according to the axis $z$

-   xmin the smallest abscissa

-   ymin the smallest ordinate

-   zmin the smallest azimuth

Then, the files of the electric field are created as follows for each of
the components of the real part and separated imaginary field:

open(11, file='Exr.mat', status='new', form='formatted',
access='direct', recl=22)

**do** k=1,nz

**do** j=1,ny

**do** i=1,nx

ii=i+nx\*(j-1)+nx\*ny\*(k-1)

write(11,FMT='(D22.15)',rec=ii) dreal(Ex)

**enddo**

**enddo**

**enddo**

Be careful, the mesh size of the discretization of the object has to be
larger than the meshsize of the discretization of the field.
