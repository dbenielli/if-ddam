#  INDEX IFFDDA M    @image  html  images/schematic.png

IFDDA M provided by
> INSTITUT FRESNEL Faculte des Sciences de Saint Jérôme <br/>
> Avenue Escadrille Normandie-Niemen 13397 MARSEILLE CEDEX  <br/>
> & INSTITUT ARCHIMEDE AIX-MARSEILLE UNIVERSITE <br/>


[TOC]

## REFERENCE

- [cdm] (@ref cdm )  folder containing main code to launch cdm 
- [cdmlib] ( @ref cdmlib)  folder containing fortran library for computing
- [qwt-6.2.0] ( @ref qwt-6.2.0 )  folder for Qt 6.0.2
- [qwtplot3d] ( @ref qwtplot3d )  folder for Modified version of the original sintegrial/QwtPlot3D library to use QOpenGLWidget


## USER DOCUMENTATION

 - [chapitre 1: Generality] (chap1a.md)  
 - [chapitre 2: Managing of the configurations] (chap2a.md)  
 - [chapitre 3: Properties of the illumination] (chap3a.md) 
 - [chapitre 4: Definition of the object] (chap4a.md) 
 - [chapitre 5: Possible study with the code] (chap5a.md) 
 - [chapitre 6: Representation of the results] (chap6a.md) 
 - [chapitre 7: Ouput files for matlab, octave, scilab,... ] (chap7a.md) 
 - [chapitre 8: Approximation to increase the efficiency of the code] (chapapproxa.md) 
 - [chapitre 9: Properties of the multilayer] (chaplayera.md) 
 - [chapitre 10: Numerical details ] (chappolaa.md) 
 - [chapitre 11: Some examples] (chapteesta.md) 
