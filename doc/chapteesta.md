Some examples {#chaptest}
=============

Introduction
------------

In bin/tests there is the file options.db3. You should copy it in the
directory bin as "cp options.db3.. /.', and then you launch the code
after the load is appear four test configurations that allow you to see
all the options in action.

Test1
-----

The aim of test1 is to test a simple case and many options of the code
to validate them. Figure [1.1](#test1conf){reference-type="ref"
reference="test1conf"} shows the options of the chosen configuration.

![Test1: configuration taken.](test1conf.eps){#test1conf width="15.0cm"}

The following figures show the results obtained. The plots are done with
Matlab and these are directly the eps files from the ifdda.m script that
are used. The advantage of matlab in this case is to give all the
figures in one go.

![Modulus of the local field in $(x,y)$
plane.](test1local.eps){width="15.0cm"}

![Modulus of the macroscopic field in $(x,y)$
plane.](test1macro.eps){width="15.0cm"}

Because the incident field is polarized along the $y$ direction (TE),
hence the $y$ component of the field inside the sphere is the largest.

![Modulus of the Poynting vector.](test1poynting2d.eps){width="15.0cm"}

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   ![Modulus of the diffracted field in the Fourier plane in transmission (left) and in reflection (right) for optical diffraction microscope.](test1fourierpos.eps){width="7.0cm"}   ![Modulus of the diffracted field in the Fourier plane in transmission (left) and in reflection (right) for optical diffraction microscope.](test1fourierneg.eps){width="7.0cm"}
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ![Modulus of the field in the image plane in transmission (left) and in reflection (right) for optical diffraction tomography. Diffracted field (above) and total field (below).](test1imagepos.eps){width="7.0cm"}      ![Modulus of the field in the image plane in transmission (left) and in reflection (right) for optical diffraction tomography. Diffracted field (above) and total field (below).](test1imageneg.eps){width="7.0cm"}
   ![Modulus of the field in the image plane in transmission (left) and in reflection (right) for optical diffraction tomography. Diffracted field (above) and total field (below).](test1imageincpos.eps){width="7.0cm"}   ![Modulus of the field in the image plane in transmission (left) and in reflection (right) for optical diffraction tomography. Diffracted field (above) and total field (below).](test1imageincneg.eps){width="7.0cm"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Test2
-----

The aim of the test2 is to test a more complex case. Figure 
[1.2](#test2conf){reference-type="ref" reference="test2conf"} shows the
options of the chosen configuration. The illumination is done with a
Gaussian beam with $w_0=\lambda$ and the object under study is a sphere
with a radius of 500 nm and an inhomogeneous permittivity ($l_c=100$ nm
and $\sigma=0.1$).

![Test2: configuration taken.](test2conf.eps){#test2conf width="15.0cm"}

![Test2: relative permittivity. Real part (left) and imaginary part
(right).](test2epsilon.eps){width="15.0cm"}

The following figures show the results obtained.

![Modulus of the local field in the $(x,y)$
plane.](test2local.eps){width="15.0cm"}

![Modulus of the macroscopic field in the $(x,y)$
plane.](test2macro.eps){width="15.0cm"}

![Modulus of the Poynting vector.](test2poynting2d.eps){width="15.0cm"}

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ![Modulus of the field in the Fourier plane in transmission (left) and in reflection (right) for an optical diffraction tomography microscope. Diffracted field (above) and total field (below).](test2fourierpos.eps){width="7.0cm"}      ![Modulus of the field in the Fourier plane in transmission (left) and in reflection (right) for an optical diffraction tomography microscope. Diffracted field (above) and total field (below).](test2fourierneg.eps){width="7.0cm"}
   ![Modulus of the field in the Fourier plane in transmission (left) and in reflection (right) for an optical diffraction tomography microscope. Diffracted field (above) and total field (below).](test2fourierincpos.eps){width="7.0cm"}   ![Modulus of the field in the Fourier plane in transmission (left) and in reflection (right) for an optical diffraction tomography microscope. Diffracted field (above) and total field (below).](test2fourierincneg.eps){width="7.0cm"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ![Modulus of the field in the image plane in transmission (left) and in reflection (right) for an optical diffraction tomography microscope. Diffracted field (above) and total field (below).](test2imagepos.eps){width="7.0cm"}      ![Modulus of the field in the image plane in transmission (left) and in reflection (right) for an optical diffraction tomography microscope. Diffracted field (above) and total field (below).](test2imageneg.eps){width="7.0cm"}
   ![Modulus of the field in the image plane in transmission (left) and in reflection (right) for an optical diffraction tomography microscope. Diffracted field (above) and total field (below).](test2imageincpos.eps){width="7.0cm"}   ![Modulus of the field in the image plane in transmission (left) and in reflection (right) for an optical diffraction tomography microscope. Diffracted field (above) and total field (below).](test2imageincneg.eps){width="7.0cm"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Test3
-----

The aim of the test3 is to test the brightfield microscope with a sphere
of radius of 500 nm and a permittivity of 1.1 put upon a glass substrate
($\varepsilon=2.25$).

![Test3: configuration taken.](test3conf.eps){#test3conf width="15.0cm"}

![Test3: Incident field used to simulate the
microscope.](test3angleincbf.eps){width="8.0cm"}

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ![Modulus of the field in the image plane in case of transmission (left) and reflection (right) for a darkfield (above) and brightfield (below) microscope.](test3imageposwf.eps){width="7.0cm"}      ![Modulus of the field in the image plane in case of transmission (left) and reflection (right) for a darkfield (above) and brightfield (below) microscope.](test3imagenegwf.eps){width="7.0cm"}
   ![Modulus of the field in the image plane in case of transmission (left) and reflection (right) for a darkfield (above) and brightfield (below) microscope.](test3imageincposwf.eps){width="7.0cm"}   ![Modulus of the field in the image plane in case of transmission (left) and reflection (right) for a darkfield (above) and brightfield (below) microscope.](test3imageincnegwf.eps){width="7.0cm"}
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Test4
-----

The aim of test4 is to test the dark field and phase microscope with a
sphere of radius of 500 nm and a permittivity of 1.1 put upon a glass
substrate ($\varepsilon=2.25$).

![Test3: configuration taken.](test4conf.eps){width="15.0cm"}

![Test4: Incident field used to simulate the
microscope.](test4angleincdf.eps){width="8.0cm"}

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ![Modulus of the field in the image plane in case of transmission (left) and reflection (right) for a darkfield (above) and phase (below) microscope.](test4imageposwf.eps){width="7.0cm"}      ![Modulus of the field in the image plane in case of transmission (left) and reflection (right) for a darkfield (above) and phase (below) microscope.](test4imagenegwf.eps){width="7.0cm"}
   ![Modulus of the field in the image plane in case of transmission (left) and reflection (right) for a darkfield (above) and phase (below) microscope.](test4imageincposwf.eps){width="7.0cm"}   ![Modulus of the field in the image plane in case of transmission (left) and reflection (right) for a darkfield (above) and phase (below) microscope.](test4imageincnegwf.eps){width="7.0cm"}
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
