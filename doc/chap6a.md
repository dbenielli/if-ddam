Representation of the results {#chap6}
=============================

Introduction
------------

Three windows enable us to manage and represent the requested results.
The one on the top enables us to manage the different figures; the one
at the bottom on the left present the digital values requested, and the
one at the bottom on the right is kept for the graphic representations.

Digital exits
-------------

All the results are given in the SI system.

-   *Object subunits*: Number of elements of discretization of the
    object under study.

-   *Mesh subunits* : Number of elements of discretization of the cuboid
    containing the object under study.

-   *Mesh size* : Size of the element of discretization.

-   $\lambda/(10|n|)$ : In order to obtain a good precision, it is
    advised to have a discretization under the value of $\lambda/10$ in
    the considered material of optical index $n$.

-   $k_0$ :Wave number.

-   *Irradiance*: Beam irradiance, for a Gaussian beam, it is estimated
    at the center of the waist.

-   *Field modulus*: Modulus of the field, for a Gaussian beam, it is
    estimated at the center of the waist.

-   *Tolerance obtained*: Tolerance obtained for the chosen iterative
    method. Logically under the requested value.

-   *Number of products Ax (iterations)*: Number of matrix vector
    products completed by the iterative method. Between brackets the
    iteration number of the iterative method.

-   *Absorptivity* Fraction of radiation absorbed in %, equal to zero if
    all the permittivity are real.

-   *Reflectivity* Fraction of radiation reflected in %.

-   *Tansmittivity* Fraction of radiation transmitted in %.

-   *Extinction cross section*: Value of the extinction cross section.

-   *Absorbing cross section*: Value of the absorbing cross section.

-   *Scattering cross section*: Value of the scattering cross section
    obtained by = extinction cross section- absorbing cross section.

-   *Scattering cross section with integration*: Value of the scattering
    cross section obtained by integration of the far field field
    radiated by the object.

-   *Scattering asymmetric parameter*: Asymmetric factor.

Graphics
--------

### Plot epsilon/dipoles

The button *Plot epsilon/dipoles* enables us to see the position of each
element of discretization. The colour of each point is associated with
the value of the permittivity of the considered meshsize.

### Far field and microscopy

#### Plot Poynting vector

*Plot Poynting*: enables us to draw the modulus of the Poynting vector
in 3D. If the option quick computation is taken, then the results come
from a interpolation of the points in the $(k_x,k_y)$ space: if it is
not smooth increase the number of point of the FFT.

#### Plot microscopy

*Plot microscopy* : enables us to draw the diffracted field in the
Fourier plane for holographic microscope or the image through the dark
field, brightfield, phase or holographic microscope. We can plot the
modulus, intensity or the component $x$, $y$ or $z$.

We have 8 different plot:

-   *Fourier plane: Scattered field: $k_z>0$* The diffracted field by
    the object in the Fourier plane for a holographic microscope in
    transmission.

-   *Fourier plane: Total field: $k_z>0$* The diffracted field by the
    object plus the incident field (if the incident field is a plane
    wave, then we have a Dirac) in the Fourier plane for a holographic
    microscope in transmission.

-   *Fourier plane: Scattered field: $k_z<0$* The diffracted field by
    the object in the Fourier plane for a holographic microscope in
    reflection.

-   *Fourier plane: Total field: $k_z<0$* The diffracted field by the
    object plus the incident field reflected on the multilayer system
    (if the incident field is a plane wave, then we have a Dirac) in the
    Fourier plane for a holographic microscope in reflection.

-   *Image plane: Scattered field: $z>0$* Image of the diffracted field
    by the object through the microscope in transmission.

-   *Image plane: Total field: $z>0$* Image of the diffracted field by
    the object plus the incident field through the microscope in
    transmission.

-   *Image plane: Scattered field: $z<0$* Image of the diffracted field
    by the object through the microscope in reflection.

-   *Image plane: Total field: $z<0$* Image of the diffracted field by
    the object plus the incident field through the microscope in
    reflection.

[Remarks]{.underline}

-   The diffracted field is represented upon a regular mesh in
    $\Delta k_x=\Delta k_y$ such as $\sqrt{k_x^2+k_y^2} \le k_0$ NA. If
    the computation is done by radiation of the dipoles, then the code
    choose to have at least 21 points in the numerical aperture. If
    quick computation is chosen then he code used FFT transform, then,
    the size of the picture is fixed by discretization of the object $d$
    with the relation $d \Delta k=2\pi/N$ and $N$ the size of the FFT.

-   The computation in the image plane is always done with FFT.

-   For the brightfield, darkfield and phase microscope only the field
    in the image plane can be plotted. When the option $x$, $y$ and $z$
    component is chosen, the phase can not be plotted as we sum
    incoherently all the incidences, then we get only the modulus.

### Study of the near field

-   The first button *Field* enables us to choose to represent the
    incident field, local field or macroscopic field.

-   The button *Type* enables us to represent the modulus or the
    component $x$, $y$ or $z$ of the studied field.

-   The button *Cross section $x$* ($y$ or $z$) enables us to choose the
    abscissa of the cut (ordinate or dimension). *Plot $x$* ($y$ or $z$)
    draws the cut in plane $x$. *Plot all $x$* draws all the cut at
    once.
