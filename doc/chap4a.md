Definition of the object {#chap4}
========================

Introduction
------------

The code proposes several predefined objects, and we are going to
precise in this section how to enter their optogeometrical
characteristics. Note that all the distances have to be entered in
nanometers. The code is doing the conversion in meters.

Type of the object
------------------

The list of the predefined objects is the following:

sphere, cube, cuboid, ellipsoid, cylinder, concentric spheres, multiple
spheres, inhomogeneous sphere, inhomogeneous cuboid and arbitrary
object.

When the objects as the cuboid, cylinder or ellipsoid have their edges
turned with respect to the axes of the system of coordinates, the angles
of Euler are used as defined in Fig. [1.1](#euler){reference-type="ref"
reference="euler"}. The rotation centre being the inertia centre of the
object and the matrix of rotation reads: $$\begin{aligned}
\boldsymbol{A} & = & \begin{pmatrix}\cos(\psi )\cos(\varphi )-\sin(\psi
  )\cos(\theta )\sin(\varphi )& -\cos(\psi )\sin(\varphi )-\sin(\psi
  )\cos(\theta )\cos(\varphi )& \sin(\psi )\sin(\theta )\\ \sin(\psi
  )\cos(\varphi )+\cos(\psi )\cos(\theta )\sin(\varphi )& -\sin(\psi
  )\sin(\varphi )+\cos(\psi )\cos(\theta )\cos(\varphi )& -\cos(\psi
  )\sin(\theta )\\ \sin(\theta )\sin(\varphi )& \sin(\theta
  )\cos(\varphi )& \cos(\theta )\end{pmatrix} \nonumber\end{aligned}$$

![Definition of the angles of Euler according to the convention $Z-X-Z$.
Scheme taken from Wikipedia](euler.eps){#euler width="8.0cm"}

### Sphere

For the sphere, there are four fields to be filled:

-   The radius of the sphere in nanometer

-   The abscissa of the centre of the sphere in nanometer

-   The ordinate of the centre of the sphere in nanometer

-   The azimuth of the centre of the sphere in nanometer

### Inhomogeneous sphere 

The permittivity of the sphere have a Gaussian noise with a correlation
length $l_c$, standard deviation, $A$ and an average $\varepsilon_r$.

For the inhomogeneous sphere there are seven fields to be filled:

-   The radius of the sphere in nanometer

-   The seed

-   The abscissa of the centre of the sphere in nanometer

-   The ordinate of the centre of the sphere in nanometer

-   The azimuth of the centre of the sphere in nanometer

-   The correlation length $l_c$

-   The standard deviation $A$

### Cube

For the cube, there are four fields to be filled:

-   The edge of the cube in nanometer

-   The abscissa of the centre of the sphere in nanometer

-   The ordinate of the centre of the sphere in nanometer

-   The azimuth of the centre of the sphere in nanometer

### Cuboid (length)

For the cuboid, there are nine fields to be filled:

-   The edge of the cube in nanometer according to the axis $x$

-   The edge of the cube in nanometer according to the axis $y$

-   The edge of the cube in nanometer according to the axis $z$

-   The abscissa of the centre of the cuboid in nanometer

-   The ordinate of the centre of the cuboid in nanometer

-   The azimuth of the centre of the sphere in nanometer

-   First angle of Euler $\psi$ by rotation around the axis $z$

-   Second angle of Euler $\theta$ by rotation around the axis $x$

-   Third angle of Euler $\varphi$ by rotation around the axis $z$

### Cuboid (meshsize)

For the cuboid, there are seven fields to be filled:

-   The abscissa of the centre of the cuboid in nanometer

-   The ordinate of the centre of the cuboid in nanometer

-   The azimuth of the centre of the sphere in nanometer

-   Number of meshsize long $x$

-   Number of meshsize long $y$

-   Number of meshsize long $z$

-   Meshsize in nanometer

### Inhomogeneous Cuboid (length)

The permittivity of the cuboid have a Gaussian noise with a correlation
length $l_c$, standard deviation $A$ and an average $\varepsilon_r$. For
the cuboid, there are nine fields to be filled:

-   The edge of the cube in nanometer according to the axis $x$

-   The edge of the cube in nanometer according to the axis $y$

-   The edge of the cube in nanometer according to the axis $z$

-   The abscissa of the centre of the cuboid in nanometer

-   The ordinate of the centre of the cuboid in nanometer

-   The azimuth of the centre of the sphere in nanometer

-   The seed

-   The correlation length $l_c$

-   The standard deviation $A$

### Inhomogeneous Cuboid (meshsize)

The permittivity of the cuboid have a Gaussian noise with a correlation
length $l_c$, standard deviation $A$ and an average $\varepsilon_r$. For
the cuboid, there are nine fields to be filled:

-   The abscissa of the centre of the cuboid in nanometer

-   The ordinate of the centre of the cuboid in nanometer

-   The azimuth of the centre of the sphere in nanometer

-   Number of meshsize long $x$

-   Number of meshsize long $y$

-   Number of meshsize long $z$

-   Meshsize in nanometer

-   The seed

-   The correlation length $l_c$

-   The standard deviation $A$

### Ellipsoid

For the ellipsoid, there are nine fields to be fulfilled:

-   The half axis in nanometer according to the axis $x$

-   The half axis in nanometer according to the axis $y$

-   The half axis in nanometer according to the axis $z$

-   The abscissa of the centre of the ellipse in nanometer

-   The ordinate of the centre of the ellipse in nanometer

-   The azimuth of the centre of the ellipse in nanometer

-   First angle of Euler $\psi$ by rotation around the axis $z$

-   Second angle of Euler $\theta$ by rotation around the axis $x$

-   Third angle of Euler $\varphi$ by rotation around the axis $z$

### Multiple spheres

For multiple spheres, it is convenient first to choose with the line
from the under *number of objects* the number $N$ of the expected
spheres. Then, when we click on *Props* $N$ windows, that we fill in the
same way as for the unique sphere, appear. Beware, the spheres must be
disconnected, otherwise, the code stops and shows error.

### Cylinder

For the cylinder, there are eight fields to be fulfilled:

-   The radius of the cylinder in nanometers

-   The length of the cylinder in nanometer

-   The abscissa of the centre of the cylinder in nanometer

-   The ordinate of the centre of the cylinder in nanometer

-   The azimuth of the centre of the cylinder in nanometer

-   First angle of Euler $\psi$ by rotation around the axis $z$

-   Second angle of Euler $\theta$ by rotation around the axis $x$

-   Third angle of Euler $\varphi$ by rotation around the axis $z$

### Concentric spheres

For concentric spheres, it is convenient first to choose with the under
line *number of objects* the number $N$ of concentric spheres. Then,
when we click on *Props* $N$ windows appear. The first window is filled
the same way as for the sphere, and for the next windows, it is enough
to enter the radius in nanometer. The radii must be entered in
increasing order, otherwise, the code shows the error.

### Arbitrary object

In the case of an arbitrary object, it is defined by the user. In other
words, he has to create the object himself, and then, it is convenient
to create this entry file by respecting the conventions chosen by the
code. *namefile* is the name of the file containing the arbitrary object
and it is asked for when we choose the arbitrary object. It is coded in
sequential and in ascii, and is necessarily described inside a cuboid
box. Below are given the lines of the code enabling to create this file:

open(15,file=namefile,status='old',iostat=ierror)

write(15,\*) nx,ny,nz

write(15,\*) aretecube

**do** i=1,nz

**do** j=1,ny

**do** k=1,nx

write(15,\*) xs(i,j,k),ys(i,j,k),zs(i,j,k)

**enddo**

**enddo**

**enddo**

**do** i=1,nz

**do** j=1,ny

**do** k=1,nx

**if** objet isotrope

write(15,\*) eps(i,j,k)

**elseif** objet anisotrope

**do** ii=1,3

**do** jj=1,3

write(15,\*) epsani(ii,jj,i,j,k)

**enddo**

**enddo**

**endif**

**enddo**

**enddo**

**enddo**

-   nx : size of the cuboid according to the axis $x$.

-   ny : size of the cuboid according to the axis $y$.

-   nz : size of the cuboid according to the axis $z$.

-   aretecube : size of the meshsize of discretization in nanometers.

-   x : abscissa of the mesh of discretization according the axis $x$.

-   y : ordinate of the mesh of discretization according the axis $y$.

-   z : azimuth of the mesh of discretization according the axis $z$.

-   eps : epsilon of the object if isotropic

-   epsani : epsilon of the object if anisotropic

Choose the relative permittivity
--------------------------------

When the object or objects are chosen, it is then convenient to enter
the relative permittivity. For the homogeneous object they may be
isotropic or anisotropic. So, we choose *iso* or *aniso* and we click on
*Epsilon*.

-   *iso*: A board appears, where either we enter the relative
    permittivity by hand (real and imaginary part) or we choose a
    material in the data base.

-   *aniso*: A board appears where we enter the relative permittivity by
    hand (real and imaginary part) for all the components of anisotropic
    tensor.

Choose the discretization
-------------------------

The number $N_c$ entered in the field of the discretization corresponds
to the number of layers forming the object in its largest direction.

A few examples:

-   For an ellipse of half axis $(a,b,c)$, it is going to be the
    greatest half axis $a$ that is going to be selected and the edge of
    discretization is going to be of $2a/N_c$.

-   For a cube the number of meshsize is so going to be of $N=N_c^3$.
