Generality {#chap1}
==========

Introduction
------------

This software computes the diffraction of an electromagnetic wave by a
three-dimensional object in a multilayer system. This interaction is
taken into account rigorously by solving the Maxwell's equations, but
can also do with the approximation of Born at the order 0 or 1. The code
has an user-friendly interface and allows you to choose canonical
objects (sphere, cube, \...) as well as predefined incident waves (plane
wave, Gaussian beam, \...) or arbitrary objects and incidents waves.
After by drop-down menus, it is easy to study cross sections,
diffraction near field and far field as well as microscopy in
transmission or reflection (holography, brigthfield, dark field,\...).

There are numerous methods that enable the study of the diffraction of
an electromagnetic wave by an object of arbitrary form and relative
permittivity. We are not going here to set up an exhaustive list of
these methods, but the curious reader may refer to the article by F. M.
Kahnert who details the advantages and weaknesses of the most common
methods. [@Kahnert_JQSRT_03]

The method we use is called coupled dipoles method (CDM) or the discrete
dipole approximation (DDA). This method is a volume method, because the
diffracted field is obtained from an integral, the support of which is
the volume of the considered object. It had been introduced by E. M.
Purcell and C. R. Pennypacker in 1973, in order to study the scattering
of light by grains in interstellar medium. [@Purcell_AJ_73]

The principle of discrete dipole approximation in a multilayer system
---------------------------------------------------------------------

Take an object of arbitrary form and relative permittivity in a
multilayer system. This multilayer is submitted to a incident field
electromagnetic wave of wavelength $$\lambda$ ($k_0=2\pi/\lambda$$). In
the absence of the object under study a reference field takes place in
the multilayer. The principle of the DDA consists in representing the
object as a set of $$N$$ small cubes of an edge $a$ \[by little, we mean
smaller than the wavelength in the object : 
$$a\ll \lambda/\sqrt{\varepsilon}$$
(Fig. [1.1]).

![discretisation] (discretisation.png)  "Principle of the DDA : the object under study (on the left) is discretized in a set of small dipoles (on the right) ."  

 

Each one of the small cubes under the action of the reference wave is
going to get polarized, and as such, to acquire a dipolar moment, whose
value is going to depend on the reference field and on its interaction
with its neighbours. The local field of a dipole located at
$\boldsymbol{r}_i$, $\boldsymbol{E}(\boldsymbol{r}_i)$, is the sum of
the incident wave and the field radiated by the dipoles :
$$\begin{aligned}
\label{cdms} \boldsymbol{E}(\boldsymbol{r}_i)=\boldsymbol{E}_{\rm
  ref}(\boldsymbol{r}_i)+\sum_{j=1}^{N}
\boldsymbol{G}(\boldsymbol{r}_i,\boldsymbol{r}_j)\alpha(\boldsymbol{r}_j)\boldsymbol{E}(\boldsymbol{r}_j). \end{aligned}$$
$\boldsymbol{E}_{\rm ref}$ is the reference wave, $\boldsymbol{G}$ the
linear susceptibility of the field of the multilayer system. $\alpha$ is
the polarizability of each discretization element obtained from the
Clausius-Mossotti relation. Note that the polarizability $\alpha$, in
order to respect the optical theorem, needs to contain a term called the
radiative reaction term. [@Draine_AJ_88]
Equation ([\[cdms\]](#cdms){reference-type="ref" reference="cdms"}) is
valid for $i=1,\cdots,N$, and so represents a system of $3N$ linear
equations where the local fields, $\boldsymbol{E}(\boldsymbol{r}_i)$,
being the unknowns. Once the system of linear equation is solved, the
field scattered by the object at an arbitrary position $\boldsymbol{r}$
is obtained by making the sum of all the radiated fields by each one of
the dipoles : $$\begin{aligned}
\label{cdmd} \boldsymbol{E}(\boldsymbol{r})=\sum_{j=1}^{N} \boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}_j)
\alpha(\boldsymbol{r}_j) \boldsymbol{E}(\boldsymbol{r}_j). \end{aligned}$$

We have just presented the DDA as E. M. Purcell and C. R. Pennypacker
had presented it earlier. [@Purcell_AJ_73] Note that another method very
close to the DDA does exist. This method called the method of the
moments starts from the integral equation of Lippman Schwinger, which is
strictly identical to the DDA. The demonstration of the equivalence
between these two methods being a little technical, it is explained in
Ref. [@Chaumet_PRE_04].

The advantages of the DDA are that it is applicable to objects of
arbitrary forms, inhomogeneous (that is hardly achievable in case of
surface method), and anisotropic (the polarizability associated to the
mesh becomes a tensor). The outgoing wave condition is automatically
satisfied through the linear susceptibility of the field. Finally, note
that only the object is discretized unlike the methods of finite
differences and finite elements. [@Kahnert_JQSRT_03] The main
inconvenience of the DDA consists in the fast increase of computation
time together with the increase of the number of discretization
elements, *i.e.*, the increase in size of the system of linear equations
to be solved.

A word about the code
---------------------

The code is thought to have a user-friendly interface so that everyone
can use it without any problems including non specialists. This allows
undergraduate students to study, for example, the basics of microscopy
(Rayleigh's criteria, notion of numerical aperture, \...) or diffraction
without any problem; and researchers, typically biologists, having no
notion of Maxwell's equations to simulate what gives a microscope
(brightfield, phase microscope, dark field, \...) in function of the
usual parameters and the object. Nevertheless, this code can also serve
physicists specializing in electromagnetism in performing, for example,
calculations of diffraction, cross sections, near field and this with
many incident beams.

The code thus has by default a simple interface where all numerical
parameters are hidden and where many options are then chosen by default.
But it's easy to access all Code options by checking the Advanced
Interface option. This user guide explains how to use the advanced
interface in starting with the different approaches used by the code to
solve the Maxwell equations.

Note that the usability of the code is made to the detriment of the
optimization of the RAM and the code can used large memory for large
objects.

How to compile the code
-----------------------

The application is based on Qt-4.8 and gfortran To install it you need :
qt, qt-devel, gcc-c++ et gfortran. Notice that there is three versions
of the code, the first one is sequential and uses FFTE (Fast Fourier
Transform in the east), the second one uses FFTW (Fast Fourier Transform
in the west) which needs openmp 4.5 minimum and the third uses HDF5
format to save data file. Currently according to the age of the linux
you use, you have Qt4 or Qt5. The code has been tested under the two
environments, but to compile you need adapt qt4 in qt5 on recent
versions, I will note for make compact qt6(5). Then to compile:

   Code par défaut         Code avec FFTW               Code avec FFTW et HDF5
  ----------------- ----------------------------- ----------------------------------
    qmake-qt6(5)     qmake-qt6(5) "CONFIG+=fftw"   qmake-qt6(5) "CONFIG+=fftw hdf5"
        make                    make                             make
    make install            make install                     make install

To run the application, cd bin, and ./cdm.

On linux system with the library FFTW, it requires to install FFTW
packages with " dnf install \* fftw \* ". For the version that uses HDF5
file you should install the following packages "dnf install hdf hdf5
hdf5-static hdf5-devel".

The code works on windows system but it is tricky to compile it if you
want to use FFTW.

A word about the authors
------------------------

-   P. C. Chaumet is Professor at Fresnel Institute of Aix-Marseille
    University, and deals with the development of the fortran source
    code.

-   A. Sentenac is research director at the CNRS, and works at Fresnel
    Institute of Aix-Marseille University, and participates to the
    development of the code connected to the far field diffraction.

-   D. Sentenac of European Gravitational Observatory in Italia develops
    the convivial interface of the code.

Licence
-------

Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)

You are free to:

-   Share - copy and redistribute the material in any medium or format

-   Adapt - remix, transform, and build upon the material

The licensor cannot revoke these freedoms as long as you follow the
license terms.

-   Attribution - You must give appropriate credit, provide a link to
    the license, and indicate if changes were made. You may do so in any
    reasonable manner, but not in any way that suggests the licensor
    endorses you or your use.

-   NonCommercial - You may not use the material for commercial
    purposes.

-   ShareAlike - If you remix, transform, or build upon the material,
    you must distribute your contributions under the same license as the
    original.

How to quote the code
---------------------

-   P. C. [Chaumet]{.smallcaps}, D. [Sentenac]{.smallcaps}, G.
    [Maire]{.smallcaps}, M. [Rasedujjaman]{.smallcaps}, T.
    [Zhang]{.smallcaps} and A. [Sentenac]{.smallcaps},\
    *IFDDA, an easy-to-use code for simulating the field scattered by 3D
    inhomogeneous objects in a stratified medium: tutorial.*\
    J. Opt. Soc. Am. A **38**, 1841 (2021).

-   S. [Khadir]{.smallcaps}, P. C. [Chaumet]{.smallcaps}, G.
    [Baffou]{.smallcaps} and A. [Sentenac]{.smallcaps},\
    *Quantitative model of the image of a radiating dipole through a
    microscope.*\
    J. Opt. Soc. Am. A **36**, 478 (2019).
