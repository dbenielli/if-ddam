\chapter{Possible study with the code}\label{chap5}
\markboth{\uppercase{Possible study with the
    code}}{\uppercase{Possible study with the code}}

\minitoc

\section{Introduction}

To determine the object with the appropriate orientation is not an
easy task.  That is why the first option {\it Only dipoles with
  epsilon}, enables us to check quickly if the object entered is well
the one intended without any calculation being launched. Once this has
been done, there are three great fields: the study in far field, the
study in near field and the optical forces.

\vskip10mm

{\underline{Important}}: Note that in the DDA the computation that
takes the longest time is the calculation of the local field due to
the necessity to solve the system of linear equations.  One option has
been added which consists in reading again the local field starting
with a file. When this option is selected, the name of a file is asked
for; either we enter an old file or a new name:

\begin{itemize}
\item If this is a new name, the calculation of the local field is
  going to be accomplished, then, stored together with the chosen
  configuration.
\item If this is an old name, the local field is going to be read
  again with a checking that the configuration has not been changed
  between the writing and the second reading. This makes it easier to
  relaunch calculations very quickly for the same configuration but
  for different studies.
\end{itemize}

\section{Study in far field}

When the option far field is selected, three possibilities appear:

\begin{itemize}

\item {\it Cross section}: This option enables us to calculate the
  extinction ($C_{\rm ext}$), absorbing ($C_{\rm abs}$) and scattering
  cross section ($C_{\rm sca}$). The scattering cross section is
  obtained through $C_{\rm sca}=C_{\rm ext}-C_{\rm abs}$. The
  extinction and absorption cross sections may be evaluated as:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \be C_{\rm ext} & = & \frac{4\pi k_0}{\|\ve{E}_0\|^2} \sum_{j=1}^{N}
  {\rm Im} \left[ \ve{E}^*_0(\ve{r}_j).  \ve{p}(\ve{r}_j) \right] \\
  C_{\rm abs} & = & \frac{4\pi k_0}{\|\ve{E}_0\|^2} \sum_{j=1}^{N}
  \left[ {\rm Im} \left[ \ve{p}(\ve{r}_j). (\alpha^{-1}(\ve{r}_j))^*
      \ve{p}^*(\ve{r}_j) \right] -\frac{2}{3} k_0^3
    \| \ve{p}^*(\ve{r}_j) \|^2 \right] .\ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  Note that the cross section can only be computed if the background
  is homogeneous. The free space code is more adapted in that case,
  but it permits to check easily if the discretization is well adapted
  when a sphere is studied. Notice that the cross section can be
  computed if there is no interface.



  
\item {\it Cross section+Poynting}: This option calculates also the
  scattering cross section from the integration of the far field
  diffracted by the object upon 4$\pi$ steradians, the asymmetric
  factor when the background is homogeneous, else it computes only the
  differential cross section, {\it i.e.}
  $\left< {\cal P} \right>=\left< \ve{S} \right> .\ve{n} R^2$ with
  $\ve{S}$ the Poynting vector, $\ve{n}$ the direction of observation,
  which is going to be represented in 3D. The values {\it Ntheta} and
  {\it Nphi} enable us to give the number of points used in order to
  calculate the scattering cross and to represent the Poynting
  vector. The larger the object is, the larger {\it Ntheta} and {\it
    Nphi} must be, which leads to time consuming calculations for
  objects of several wavelengths.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \be C_{\rm sca} & = & \frac{k_0^4}{\|\ve{E}_0\|^2} \int \left\|
    \sum_{j=1}^N \left[ \ve{p}(\ve{r}_j)-\ve{n}(\ve{n}.
      \ve{p}(\ve{r}_j)) \right] e^{-i k_0 \ve{n}.\ve{r}_j} \right\|^2
  {\rm d}\Omega \\ g & = & \frac{k_0^3}{C_{\rm sca} \|\ve{E}_0\|^2}
  \int \ve{n}.\ve{k}_0 \left\| \sum_{j=1}^N \left[
      \ve{p}(\ve{r}_j)-\ve{n}(\ve{n}.  \ve{p}(\ve{r}_j)) \right] e^{-i
      k_0 \ve{n}.\ve{r}_j} \right\|^2 {\rm d}\Omega \\
  \frac{{\rm d} \left< {\cal P} \right>}{{\rm d}\Omega} & = & \frac
  {1}{2} c \varepsilon_0 n \left\| \ve{E}_{\rm d} (\ve{k}_{\parallel})
  \right\|^2,  \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  where $ \ve{E}_{\rm d} (\ve{k}_{\parallel})$ is the diffracted field
  in far field.

  A solution in order to go faster (option {\it quick computation})
  and to pass by FFT for the calculation of the diffracted field.  In
  this case, of course, it is convenient to discretize keeping in mind
  that the relation $\Delta x \Delta k=2\pi/N$ connects the mesh size
  of the discretization with the size of the FFT.  This is convenient
  for objects larger than the wavelength. Indeed, $L=N\Delta x$
  corresponds to the size of the object which gives $\Delta k=2\pi/L$,
  and if the size of the object is too small, then, the $\Delta k$ is
  too large, and the quadrature is imprecise. Note that since the
  integration is performed on two planes parallel to the plane
  $(x,y)$, is not convenient if the incident makes an angle more than
  70 degrees with the $z$ axis. The 3D representation of the vector of
  Poynting is done as previously, i.e. with {\it Ntheta} and {\it
    Nphi} starting with an interpolation upon the calculated points
  with the FFT.

\item {\it Emissivity}. This study computes the reflectance,
  transmittance and absorptance. If the object under study is no
  absorbing then the absorptance should be zero. Then it traduces the
  level of energy conservation of our solver. It can depend of the
  precision of the iterative method and of the polarizability chosen.

\end{itemize}
  
\section{Microscopy}
  
This option first asks for the type of microscope required:
Holographic microscope holographic microscope, brightfield microscope,
darkfield and phase microscope and phase microscope, etc. We consider
a microscope made of an objective lens and a tube lens in $4f$
configuration and sine-Abbe condition~\cite{Abbe} By default, the
lenses are placed parallel to the plane $(x,y)$ and their optical axis
are confounded with the $z$ axis. The focus plane of the lenses are
placed to the origin of the frame but can be changed via the field
``Position of the focal plane'' for the microscope in transmission and
reflection (Fig.~\ref{lentille}). The magnification of the microscope
is $M$ and should be above 1.  The drop menu side computation permits
to simulate microscope in transmission (Side $k_Z>0$), in reflection
(Side $k_Z>0$), or both cases.


\begin{figure}[h]
\begin{center}
\includegraphics*[draft=false,width=150mm]{microscopie.eps}
\caption{Simplified figure of the microscope. The object focus of the
  objective lens are at the origin of the frame but can be changed. The
  axis of the lens is confounded with the $z$ axis.}
\label{lentille}
\end{center}
\end{figure}


The calculation for the diffracted field may be completed starting
with the sum of the radiation of the dipoles (very long when the
object has a lot of dipoles) or with FFT (option {\it quick
  computation}) with a value $N=128$ by default here as well. In this
case, $\Delta x \Delta k=2\pi/N$ with $\Delta x$ the mesh size of
discretization of the object which corresponds also to the
discretization of the picture plane. Consequently, this one has a size
of $L=G N \Delta x$.


The principle of the computation of the far field diffracted by the
object and how to get the image through a microscope with a magnifying
factor $M>1$ has been detailed in Ref~\cite{Khadir_JOSAA_19}. Then we
recall it briefly.  The diffracted field in far field at a distance
$r$ of the origin in the direction $(k_x,k_y)$ can be written as
$\ve{E}= \ve{S}(k_x,k_y) \frac{e^{i k r}}{r}$. The field after the
first lens (field in the Fourier space) is then defined as:
$\ve{e}(k_{\parallel})=\frac{\ve{S}(k_x,k_y)}{-2 i \pi \gamma}$ with
$\gamma=\sqrt{\epsilon_{\rm mul} k_0^2-k_x^2-k_y^2}$ where the value
of $\epsilon_{\rm mul}$ corresponds to the permittivity of the
substrate for the microscope in reflection and to the permittivity of
the superstrate for the microscope in transmission.  A microscope
transforms a plane wave with wavevector $\ve{k}$ into a plane wave
with wavevector $\ve{k}'$ with $\ve{k}'=[\ve{k}'_{\parallel},\gamma']$
where $\ve{k}'_{\parallel}=(-k_x/M,-k_y/M)$ and
$\gamma'=\sqrt{k_0^2-k_{\parallel}^{'2}}$ (the refractive index in the
image space is considered equal to 1). Then the field in the image
space, after the tube lens, reads as:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be \ve{E}_\mathrm{ob}(\ve{r})=\frac{1}{M} \iint
\sqrt{\frac{\gamma}{\gamma'}} \tilde{h}(\ve{k}_{\parallel})
\ve{e}'(\ve{k}_{\parallel}) \exp [i \ve{k}' \cdot (\ve{r} -\ve{r}_f) ]
   {\rm d} \ve{k}_{\parallel},
\label{objectfield}
\ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
where $\tilde{h}(\ve{k}_{\parallel})$ is cutoff function allowing the
transmission of only the signal included in the numerical aperture
($NA$) of the objective lens, it reads $\tilde{h}(\ve{k}_{\parallel})
= 1$ for $\mid \ve{k}_{\parallel} \mid < k_0 \mathrm{NA} $ and $0$
elsewhere and $\ve{r}_f$ the position of the lens. We have
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be \ve{e}'(\ve{k}_{\parallel}) =
\ve{R}(\ve{k}_{\parallel})\ve{e}(\ve{k}_{\parallel}),   \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
and $\ve{R}(\ve{k}_\parallel)$ is given by:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{equation}
\ve{R}(\ve{k}_\parallel)=
\begin{pmatrix}
  u_{x}^2(1-\cos\theta)+\cos\theta & u_{x} u_{y}(1-\cos\theta) & u_{y}
  \sin\theta \\ u_{x} u_{y}(1-\cos\theta) &
  u_{y}^2(1-\cos\theta)+\cos\theta & -u_{x} \sin\theta\\ -u_{y}
  \sin\theta & u_{x} \sin\theta & \cos\theta
\end{pmatrix},
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
where $\ve{u}=\frac{\hat{\ve{k}}\times\ve{z}}{\mid \hat{\ve{k}} \times
  \ve{z} \mid}$ is the rotation axis. Notice that $\ve{u}$ has no
component along the $z$ direction. $\theta$ is defined as $\cos
\theta=\hat{\ve{k}}.\hat{\ve{k}'} $ and $\sin
\theta=\|\hat{\ve{k}}\times \hat{\ve{k}'}\|$


In the microscopy menu, different microscopes are proposed.


The first microscope in the list is the holographic microscope, {\it
  Holographic}, which is a rather a special microscope because it
illuminated by a coherent incident (often a plane wave or a Gaussian
beam but this is not not mandatory), then the diffracted field is
measured in modulus and phase through an interferential system
(off-axis for example) and this for the different $x$, $y$ or $z$
components (generally the experimenter does not have access to the $z$
component, but this component is very small due to the high
magnification of the microscope). The result given by the code is
therefore the field diffracted by the object (Fourier plane) in
modulus and phase and the image obtained through the microscope image
obtained through the microscope at the image focus position (Plane
image) in modulus and phase with and without the presence of the
incident field.  The incident field is the one defined in the code in
the the illumination properties section.

The other proposed microscopes are more classical in the sense that
the illumination is incoherent and we finally obtain the light
intensity in the image focal plane of the microscope. To obtain the
incoherent illumination we illuminate by numerous plane waves with
polarizations and we sum up all the images obtained in intensity. This
calculation requires many illuminations and can therefore be very time
consuming.  The step of discretization of the incident illuminations
in the Fourier domain is chosen such that $\Delta k_{\rm inc}<\pi/l$
where $l$ is the maximum size of the sample and with the condition
that $\Delta k_{\rm inc}=m \Delta k$ with $m\in \mathbb{N}^*$. Note
that if we use the matlab interface to plot the images then the
incidents chosen by the code will be indicated in figure 560.

\begin{itemize}

\item {\it Brightfield} For this microscope it is necessary to define
  NA=the numerical aperture of the condenser, see
  Fig.~\ref{mask}(a). Then the intensity diffracted by the object
  alone is calculated, as well as the total intensity which
  corresponds to the brightfield microscope.

  
\item {\it Darkfield \& phase contrast}: Darkfield microscopy
  microscopy illuminates the object along a ring between NA (NA
  condenser in the graphical interface) and NA$_{central~aperture}$,
  see Fig.~\ref{mask}(b). The incoherent sum of all the fields
  diffracted fields between NA and NA$_{center~aperture}$ is made. The
  result is given in the ``plane image'' without the incident field
  (dark field) and for the phase microscope the incident field is
  added to the diffracted field obtained with the dark field. The
  incident field out of phase by $\pi/2$.
  
\item {\it Darkfield cone\& phase contrast}: This is the same as the
  same as the previous microscope except that the sum is made on the
  generators of the cone, see Fig.~\ref{mask}(c). To be preferred if
  NA-NA$_{central~aperture}$ is very small.

\item {\it Schieren}: The illumination pupil of radius NA can be
  off-centre at any position, see Fig.~ref{mask}(d). The code returns
  the intensity diffracted by the object alone and the total intensity
  (incident+diffracted).

\item {Experimental phase contrast}: This microscope uses the
  Darkfield illumination with an illumination between NA and
  NA$_{central~aperture}$, see Fig.~\ref{mask}(b). Then the phase of
  the diffracted field plus the incident is shifted of $\pi/2$ in the
  Fourier domain between NA and NA$_{central~aperture}$. The shift of
  the phase of the diffracted field plus incident field in the
  illumination ring corresponds exactly to what happens
  experimentally.


\end{itemize}


\begin{figure}[H]
\begin{center}
\includegraphics*[draft=false,width=120mm]{masque.eps}
\caption{Different masks in the Fourier domain for the illumination to
  simulate different types of microscopes. (a) Illumination in a NA
  cone. (b) Illumination in a NA-NA$_{central aperture}$ cone. (c)
  Illumination along the generatrix of an NA aperture cone. (d)
  Illumination in a pupil of NA aperture centred on the point
  $(k_x/k_0,k_y/k_0)$.}
\label{mask}
\end{center}
\end{figure}

\section{Study in near field}

When the option near field is selected, two possibilities appear:

\begin{itemize}

\item {\it Local field}: This option enables us to draw the local
  field to the position of each element of discretization. The local
  field being the field at the position of each element of
  discretization in absence of itself. 

\item {\it Macroscopic field}: This option enables us to draw the
  macroscopic field to the position of each element of
  discretization. The connection between the local field and the
  macroscopic field is given Ref.~\cite{Chaumet_PRE_04} :
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \be \ve{E}_{\rm macro} & = & 3 \varepsilon_{\rm mul} \left(
  \varepsilon+2\varepsilon_{\rm mul} -i \frac{k_0^3 d^3
    \varepsilon_{\rm mul}^{3/2}}{2 \pi} (\varepsilon-\varepsilon_{\rm
    mul})\right)^{-1} \ve{E}_{\rm local} \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\end{itemize}

The last option enables us to choose the mesh in which the local and
macroscopic fields are represented.

\begin{itemize}

\item {\it Object}: Only the field in the object is
  represented. Notice that when FFT is used for the beam or for the
  computation of the diffracted field then this options is passed in
  the option {\it Cube}. This is same for the computation of the
  emissivity, the reread option and the use of the BPM(R).

\item {\it Cube}: The field is represented within a cuboid containing
  the object.

\item {\it Wide field}: The field is represented within a box greater
  than the object.  The size of the box correspond to the size of the
  object plus the Additional side band ($x$, $y$ or $z$) on each
  side. For example for a sphere with a radius $r=100$~nm and
  discretization of 10, {\it i.e.} a meshsize of 10 nm, with an
  Additional side band $x$ of 2, 3 for $y$ and 4 for $z$, we get a box
  of size:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \be l_x & = & 100 + 2\times 2 \times 10 = 140~{\rm nm} \\
  l_y & = & 100 + 2\times 3 \times 10 = 160~{\rm nm} \\
  l_z & = & 100 + 2\times 4 \times 10 = 180~{\rm nm}
  \ee
\end{itemize}
The field inside the wide field area in near field is computed with
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \be \ve{E}=\ve{E}_0+\ve{A} \ve{D} \ve{E}, \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
which gives the field inside the near field zone and in the object.


%\section{Optical force and torque}

%When the force option is selected, four possibilities appear:
%%\begin{itemize}

%\item {\it Optical force}: Calculation of the optical force exerting
%  on one or more objects.

%\item {\it Optical force density}: Enables us to draw the density of
%  the optical force.

%\item {\it Optical torque}: Calculation of the optical torque exerting
%  on one or more objects.  The torque is computed for an origin placed
%  in the gravity center of the object.

%\item {\it Optical torque density}: Enables us to draw the density of
%  the optical force torque.
%\end{itemize}
%The net optical force and troque experienced by the object are
%computed with~\cite{Chaumet_OL_00,Chaumet_JAP_07a}:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\be \ve{F} & = & (1/2) \sum_{j=1}^N {\rm Re}\left(\sum_{v=1}^{3}
%  p_v(\ve{r}_j) \frac{\partial (E_v(\ve{r}_j))^*}{\partial u}\right) \\
%\ve{\Gamma} & = & \sum_{j=1}^N \left[ \ve{r}_{j} \times
%  \ve{F}(\ve{r}^g_{j})+ \frac{1}{2} {\rm Re} \left\{ \ve{p}(\ve{r}_{j})
%    \times \left[ \ve{p}(\ve{r}_{j})/{\alpha_{\rm
%          CM}}(\ve{r}_{j})\right]^* \right\} \right].  \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%where $u$ or $v$, stand for either $x$ ,$y$, or $z$. The symbol $*$
%denotes the complex conjugate. $\ve{r}^g_{j}$ is the vector bewteen
%$j$ and the center of masse of the object.
