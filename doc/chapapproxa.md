Approximation to increase the efficiency of the code {#chapapprox}
====================================================

Introduction
------------

In the previous chapter we have presented the DDA in a simple way where
the object under study is a set of radiating dipole. In an approach more
rigorous, with the Maxwell's equation, we get in Gaussian unit:
$$\begin{aligned}
\boldsymbol{\nabla}\times \boldsymbol{E}^{\rm m}(\boldsymbol{r}) & = & i \frac{\omega}{c}
\boldsymbol{B}(\boldsymbol{r}) \\
\boldsymbol{\nabla}\times \boldsymbol{B}(\boldsymbol{r}) & = & -i \frac{\omega}{c}
\varepsilon(\boldsymbol{r}) \boldsymbol{E}^{\rm m}(\boldsymbol{r}), \end{aligned}$$
where $\varepsilon(\boldsymbol{r})$ denotes the relative permittivity of
the object and $\boldsymbol{E}^{\rm m}$ the macroscopic field inside the
object, then we get $$\begin{aligned}
\boldsymbol{\nabla}\times ( \boldsymbol{\nabla}\times \boldsymbol{E}^{\rm m}(\boldsymbol{r}) ) & = &
\varepsilon(\boldsymbol{r}) k_0^2 \boldsymbol{E}^{\rm m}(\boldsymbol{r}), \end{aligned}$$
with $k_0=\omega^2/c^2$. Using the relationship
$\varepsilon=\varepsilon_{\rm mul}+4\pi \chi$, where $\chi$ denotes the
linear field susceptibility and $\varepsilon_{\rm mul}$ the relative
permittivity of the multilayer system. Note that $\varepsilon_{\rm mul}$
depends only of $z$. Then, we have: $$\begin{aligned}
\boldsymbol{\nabla}\times ( \boldsymbol{\nabla}\times \boldsymbol{E}^{\rm m}(\boldsymbol{r}) )
-\varepsilon_{\rm mul} k_0^2 \boldsymbol{E}^{\rm m}(\boldsymbol{r}) & = & 4\pi
\chi(\boldsymbol{r}) k_0^2 \boldsymbol{E}^{\rm m}(\boldsymbol{r}) . \label{champref}\end{aligned}$$
To solve this equation one needs the Green function defined as:
$$\begin{aligned}
\boldsymbol{\nabla}\times ( \boldsymbol{\nabla}\times \boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}') )
-\varepsilon_{\rm mul} k_0^2 \boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}') & = & 4\pi k_0^2
\boldsymbol{I} \delta(\boldsymbol{r}-\boldsymbol{r}'), \end{aligned}$$
and the solution of Eq. ([\[champref\]](#champref){reference-type="ref"
reference="champref"}) reads: $$\begin{aligned}
\boldsymbol{E}^{\rm m}(\boldsymbol{r}) = \boldsymbol{E}_{\rm ref}(\boldsymbol{r}) +\int_{\Omega}
\boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}') \chi(\boldsymbol{r}') \boldsymbol{E}^{\rm m}(\boldsymbol{r}') {\rm d}
\boldsymbol{r}',\end{aligned}$$ where $\boldsymbol{E}_{\rm ref}$ is the
field in the absence of the object and $\Omega$ the support of the
object under study. When we solve
Eq. ([\[champref\]](#champref){reference-type="ref"
reference="champref"}) the field $\boldsymbol{E}^{\rm m}$ corresponds to
the macroscopic field inside the object. To solve
Eq. ([\[champref\]](#champref){reference-type="ref"
reference="champref"}) we discretize the object in a set of $N$ subunits
with a cubic meshsize $d$, then the integral equation becomes the sum of
$N$ integrals: $$\begin{aligned}
\boldsymbol{E}^{\rm m}(\boldsymbol{r}_i) = \boldsymbol{E}_{\rm ref}(\boldsymbol{r}_i)
+\sum_{j=1}^{N} \int_{V_j} \boldsymbol{G}(\boldsymbol{r}_i,\boldsymbol{r}') \chi(\boldsymbol{r}')
\boldsymbol{E}^{\rm m}(\boldsymbol{r}') {\rm d} \boldsymbol{r}',\end{aligned}$$
with $V_j=d^3$. Assuming the field, the Green function and the
susceptibility constant over a subunit we get: $$\begin{aligned}
\boldsymbol{E}^{\rm m}(\boldsymbol{r}_i) = \boldsymbol{E}_{\rm ref}(\boldsymbol{r}_i) +\sum_{j=1}^N
\boldsymbol{G}(\boldsymbol{r}_i,\boldsymbol{r}_j) \chi(\boldsymbol{r}_j) \boldsymbol{E}^{\rm m}(\boldsymbol{r}_j)
d^3.\label{eqmd}\end{aligned}$$ We can share into two parts the Green
function, $\boldsymbol{G}=\boldsymbol{M}+\boldsymbol{T}$, where
$\boldsymbol{M}$ is the Green function who take into account of the
multiple reflection between the different layers and $\boldsymbol{T}$ is
the Green function of the homogeneous space. Using, in first
approximation (the radiative reaction term neglected)
$\int_{V_i}\boldsymbol{T}(\boldsymbol{r}_i,\boldsymbol{r}') {\rm d} \boldsymbol{r}'=
-4\pi/(3\varepsilon_{\rm mul}(\boldsymbol{r}_i))$ [@Yaghjian_PIEEE_80],
we get: $$\begin{aligned}
\boldsymbol{E}^{\rm m}(\boldsymbol{r}_i) = \boldsymbol{E}_{\rm ref}(\boldsymbol{r}_i) +\sum_{j=1}^N
\boldsymbol{G}'(\boldsymbol{r}_i,\boldsymbol{r}_j) \chi(\boldsymbol{r}_j) d^3 \boldsymbol{E}^{\rm
  m}(\boldsymbol{r}_j)-\frac{4\pi}{3\varepsilon_{\rm
    mul}(\boldsymbol{r}_i)}\chi(\boldsymbol{r}_i) \boldsymbol{E}^{\rm m}(\boldsymbol{r}_i),\end{aligned}$$
where $\boldsymbol{G}'=\boldsymbol{M}+\boldsymbol{T}$ for $i \neq j$ and
$\boldsymbol{G}'=\boldsymbol{M}$ for $i=j$, then we can write
$$\begin{aligned}
\boldsymbol{E}(\boldsymbol{r}_i) & = & \boldsymbol{E}_{\rm ref}(\boldsymbol{r}_i) +\sum_{j=1}^N
\boldsymbol{G}'(\boldsymbol{r}_i,\boldsymbol{r}_j) \alpha_{\rm CM}(\boldsymbol{r}_j) \boldsymbol{E}(\boldsymbol{r}_j)
\\ {\rm with} \phantom{000} \boldsymbol{E}(\boldsymbol{r}_i) & = &
\frac{\varepsilon(\boldsymbol{r}_i)+2\varepsilon_{\rm
    mul}(\boldsymbol{r}_i)}{3\varepsilon_{\rm mul}(\boldsymbol{r}_i)} \boldsymbol{E}^{\rm
  m}(\boldsymbol{r}_i) \\ \alpha_{\rm CM}(\boldsymbol{r}_j) & = & \frac{3}{4\pi}
\varepsilon_{\rm mul}(\boldsymbol{r}_i) d^3
\frac{\varepsilon(\boldsymbol{r}_i)-\varepsilon_{\rm
    mul}(\boldsymbol{r}_i)}{\varepsilon(\boldsymbol{r}_i)+2\varepsilon_{\rm
    mul}(\boldsymbol{r}_i)} .\end{aligned}$$ The field
$\boldsymbol{E}(\boldsymbol{r}_i)$ is the local field, *i.e.* the field
at the position $i$ in the absence of the subunit $i$. Then the linear
system can be written formally as $$\begin{aligned}
\boldsymbol{E} =  \boldsymbol{E}_{\rm ref}+ \boldsymbol{A} \boldsymbol{D}_\alpha \boldsymbol{E}, \label{eqmsym}\end{aligned}$$
where $\boldsymbol{A}$ is a matrix which contains all the Green function
and $\boldsymbol{D}_\alpha$ is a tridiagonal matrix with the
polarizabilities of each element of discretization. In the next chapter
we detail how to solve Eq. ([\[eqmsym\]](#eqmsym){reference-type="ref"
reference="eqmsym"}) rigorously, but in this present chapter we detail
different approached methods to avoid the tedious resolution of
Eq. ([\[eqmsym\]](#eqmsym){reference-type="ref" reference="eqmsym"}).
The scattered field is computed through $$\begin{aligned}
\boldsymbol{E}^{\rm d}(\boldsymbol{r}) & = & \sum_{j=1}^N \boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}_j)
\alpha(\boldsymbol{r}_j) \boldsymbol{E}(\boldsymbol{r}_j). \end{aligned}$$

Approximated method
-------------------

### Born

The most simple approximation is the Born approximation which consists
to assume the field inside the object equal to the reference field for
each element of discretization: $$\begin{aligned}
\boldsymbol{E}^{\rm m}(\boldsymbol{r}_i) = \boldsymbol{E}_{\rm ref}(\boldsymbol{r}_i), \end{aligned}$$
This approximation hold if the contrast is weak and the object small
compare to the wavelength of illumination.

### Renormalized Born 

The renormalized Born approximation consists to assume the local field
inside the object equal to the reference field : $$\begin{aligned}
\boldsymbol{E}(\boldsymbol{r}_i) = \boldsymbol{E}_{\rm ref}(\boldsymbol{r}_i). \end{aligned}$$
In that case the macroscopic field reads: $$\begin{aligned}
\boldsymbol{E}^{\rm m}(\boldsymbol{r}_i) = \frac{3 \varepsilon_{\rm mul}
}{\varepsilon(\boldsymbol{r}_i)+2 \varepsilon_{\rm mul}} \boldsymbol{E}_{\rm
    ref}(\boldsymbol{r}_i). \end{aligned}$$ This approximation is better
that the classical Born approximation when the permittivity is high.

### Born at the order 1

To be more precise that the renormalized Born approximation, one can
perform the Born series at the order one: $$\begin{aligned}
\boldsymbol{E}(\boldsymbol{r}_i) & = & \boldsymbol{E}_{\rm ref}(\boldsymbol{r}_i) +\sum_{j=1}^N
\boldsymbol{G}'(\boldsymbol{r}_i,\boldsymbol{r}_j) \alpha(\boldsymbol{r}_j) \boldsymbol{E}_{\rm
  ref}(\boldsymbol{r}_j). \end{aligned}$$ In that case we take into
account the simple scattering.

### Scalar Approximation revisited : Scalar approximation $\boldsymbol{u}^*\boldsymbol{G}\boldsymbol{u}$

We only consider configurations where the reference field in $\Omega$
can be written as
$\boldsymbol{E}_{\rm ref}(\boldsymbol{r})=E_{\rm ref}(\boldsymbol{r}) \boldsymbol{u}$
with $\boldsymbol{u}$ a complex vector such that
$\boldsymbol{u}\cdot\boldsymbol{u}^{*}=1$ where $*$ stands for the
complex conjugate and $E_{\rm ref} (\boldsymbol{r})$ a complex function.
This is the case if the reference field in $\Omega$ is a plane wave and
the object is located in the superstrate.

In our approach, we assume that the field inside $\Omega$ is directed
along $\boldsymbol{u}$ so that
$\boldsymbol{E}^{\rm m}(\boldsymbol{r})\approx E_u(\boldsymbol{r})\boldsymbol{u}$
where $E_u$ is a complex function. In this case, taking the scalar
product of Eq. ([\[eqmd\]](#eqmd){reference-type="ref"
reference="eqmd"}) with $\boldsymbol{u}^{*}$ yields an integral scalar
equation for $E_u$, $$\begin{aligned}
 \boldsymbol{u}^{*}\cdot \boldsymbol{E}^{\rm m}(\boldsymbol{r}) & =
  \boldsymbol{u}^{*}\cdot \boldsymbol{E}_{\rm ref}(\boldsymbol{r}) \boldsymbol{u}^{*} \cdot
  \int_\Omega \boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}') \chi(\boldsymbol{r}') E^{\rm
    m}_u(\boldsymbol{r}')\boldsymbol{u}
                                                       {\rm d}\boldsymbol{r}' \nonumber \\
  E^{\rm m}_u(\boldsymbol{r}) & = {E}_{\rm ref}(\boldsymbol{r})+ \int_\Omega \left[
                        \boldsymbol{u}^{*} \cdot \boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}') \boldsymbol{u}
                        \right] \chi(\boldsymbol{r}') E^{\rm m}_u(\boldsymbol{r}')
                        {\rm d}\boldsymbol{r}',
\label{eqmdu} \end{aligned}$$ where the green tensor has been replaced
by the scalar function,
$g_u(\boldsymbol{r},\boldsymbol{r}')=\boldsymbol{u}^{*} \cdot \boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}') \boldsymbol{u}$.
Then the field inside $\Omega$ is computed through: $$\begin{aligned}
E^{\rm m}_u(\boldsymbol{r}_i)= {E}_{\rm ref}(\boldsymbol{r}_i)+ \sum_{j=1}^N
g_u(\boldsymbol{r}_i,\boldsymbol{r}_j) \chi(\boldsymbol{r}_j) E^{\rm m}_u(\boldsymbol{r}_j) d^3,
\label{eqmdun} \end{aligned}$$ with $i=1,\cdots,N$. It is obvious that
the size of the vector and the matrix are decreased by a factor 3. Then,
when we solve iteratively the linear system, as it needs only to treat
one component, we guess that the numerical computation will be faster by
a factor 3 at least. Notice that once the near field is obtained, we
transform it in vectorial form with
$\boldsymbol{E}^{\rm m}(\boldsymbol{r}_i)=E^{\rm m}_u(\boldsymbol{r}_i) \boldsymbol{u}$
and we use it to obtain the far field.

It is of course obvious that in this case we always calculate the Green
function of the multilayer rigorously, which takes time.

### Homogeneous

To save a lot of time, if the object is entirely included in a layer (
*i.e.* no interface crosses the object), we can assume that the
interaction between the object and the interfaces is negligible (low
contrast object, far from the interface, low contrast interfaces,\...)
and in this case we take the Green's tensor of the homogeneous space
with the index corresponding to the layer where the object is located.
The advantage is that the Green's tensor in this case is very easy to
calculate because it is analytical: $$\begin{aligned}
F(\boldsymbol{r}_i,\boldsymbol{r}_j) & = & e^{ik_0 r}
\left[\left(3\frac{\boldsymbol{r}\bigotimes\boldsymbol{r}}{r^2}- \boldsymbol{I}\right)
  \left(\frac{1}{r^3}-\frac{ik_0}{r^2}\right) +
  \left(\boldsymbol{I}-\frac{\boldsymbol{r}\bigotimes\boldsymbol{r}}{r^2}\right)
  \frac{k_0^2}{r}\right], \end{aligned}$$ and moreover the matrix-vector
product to solve the system of linear equations is done by
three-dimensional FFT, therefore very fast. On the other hand, the
incident field or the the calculation of the diffracted field, is
rigorously done, the approximation remains confined to the the near
field interaction matrix.

### Homogeneous plus scalar approximation: Homogeneous $e(ikr)/r$

The approximation is the same as in the previous paragraph but in
addition the Green's tensor is assimilated to the scalar Green's
function *i.e.* :
$\boldsymbol{F} \approx k_0^2 \frac{e^{i kr}}{r}\boldsymbol{I}$. The
advantage is of course to divided, at least, by three the time of
computation to solve the system of linear equations. This type of
calculation is suitable for objects with weak contrast, such as in
biology. The advantage of this type of calculation is therefore that it
greatly accelerates the solution of the system of linear equations,
which can be very useful when using the microscopy option where the
calculation must be done for many incidences.

### Homogeneous plus scalar approximation revisited: Homogeneous $\boldsymbol{u}^*\boldsymbol{F}\boldsymbol{u}$

This approximation is the same as in the previous paragraph but the
Green's tensor is taken equal to
$f_u(\boldsymbol{r},\boldsymbol{r}')=\boldsymbol{u}^{*} \cdot \boldsymbol{F}(\boldsymbol{r},\boldsymbol{r}') \boldsymbol{u}$.
This scalar approximation allowing to have higher permittivities.

Computation of the Green function
---------------------------------

Even though we are using an efficient integration scheme to evaluate the
Green tensor, see Ref. , it takes a lot of time to evaluate it for all
the different pair of points covering the object. Note that due to the
translational invariance of the reference medium in the $(x,y)$ plane,
$\boldsymbol{G}(\boldsymbol{r},\boldsymbol{r}')=\boldsymbol{G}(\|\boldsymbol{r}_{\parallel}-\boldsymbol{r}_{\parallel}'\|,z,z')$.
For each couple $(z,z')$, the number of pairs with different distances
of a Cartesian $(x,y)$ mesh with $n_x\times n_y$ points ($n_x>n_y)$ is
equal to $n_y(2 n_x-n_y+1)/2$. To accelerate the computation, we
approximate
$\boldsymbol{G}(\|\boldsymbol{r}_{\parallel}-\boldsymbol{r}_{\parallel}'\|,z,z')$
using an interpolation of a discrete set of points,
$\boldsymbol{G}(qd/n_d,z,z')$ with
$q=1,\cdots, {\rm int}\sqrt{ n_x^2+n_y^2}$ and $n_d$ a natural number.
Linear and polynomial interpolations could not evaluate the Green tensor
properly when
$\|\boldsymbol{r}_{\parallel}-\boldsymbol{r}_{\parallel}'\|<\lambda$, as
the fast decay of the evanescent waves was not accounted for accurately.
We obtained much better results using rational functions, that is
quotients of polynomials. Rational functions have the ability to model
functions with poles (Press et al. 1986) and permit an accurate
approximation of the $1/r^3$ behavior of the Green tensor in the near
field range.

In the code, in the section numerical parameters, the drop menu Green
function permits to choose to compute rigorously the Green function or
evaluate the Green tensor with interpolation for $n_d=1, 2, 3, 4$.
Notice that by default the code uses $n_d=2$ which is enough accurate.
Note that only the computation with interpolation is parallelized.
