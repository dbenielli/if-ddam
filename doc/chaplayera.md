Properties of the multilayer  {#chapl}
============================

Introduction
------------

In the section properties of the multilayer, you should first choose the
number of interface. Note that you can not choose zero, as in that case
the free space code is more efficient. Once the number $n$ of interface
is fixed click on *Props*. It appears a new window with $n$ interfaces
and $n+1$ media. The first medium corresponds to the substrate, *i.e.*
the medium through which the light comes. The permittivity of this
medium is obviously transparent (no absorption). Then the position of
the first interface in nanometer is asked followed by the second medium,
etc. The sign -is associated to the substrat and the sign + to the
superstrat, then $\varepsilon_{-}$ and $\varepsilon_{+}$ for the
permittivity, respectiveley.

An example is given in Fig. [1.1](#configlayer){reference-type="ref"
reference="configlayer"}

![How to configure the layer.](images/configlayer.png){#configlayer
width="15.0cm"}

The last medium corresponds to the superstrate.

Remarks
-------

-   The last medium if one studies a microscope in transmission can not
    be absorbing.

-   If the object under study contains one or more interfaces, note that
    any dipole (element of discretization) can be on a interface, then
    the code slightly moves the interface (less than one half mesh) to
    avoid this.

-   The code is limited to 10 interfaces.

-   Notice that if there is a large distance between two layers (several
    hundred of wavelength) the computation of the Green function can
    fail.

-   The multilayer support guided mode resonance as the Green function
    is computed with the residue theorem.
