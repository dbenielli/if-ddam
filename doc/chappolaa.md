Numerical details {#chappola}
=================

Polarizability
--------------

The DDA discretizes the object into a set of punctual dipoles, where a
polarizability $\alpha$ is associated to each punctual dipoles. There
are different forms for this polarizability. The first to have been
used, and the simplest, is the relation of Clausius Mossotti
(CM) [@Purcell_AJ_73]: $$\begin{aligned}
\alpha_{\rm CM} & = & \frac{3}{4\pi}\varepsilon_{\rm mul}
\frac{\varepsilon-\varepsilon_{\rm mul}}{\varepsilon+2\varepsilon_{\rm
    mul}}d^3= \varepsilon_{\rm mul} \frac{\varepsilon-\varepsilon_{\rm
    mul}}{\varepsilon+2\varepsilon_{\rm mul}}a^3, \end{aligned}$$ where
$\varepsilon$ denotes the permittivity of the object, $d$ the size of
the cubic meshsize and $a=\left(\frac{3}{4\pi}\right)^{\frac{1}{3}}d$
the radius of the sphere of the same volume than the cubic meshsize of
the side $d$. Unfortunately, this relation does not keep the energy and,
then, it is necessary to introduce a radiative reaction term that takes
into account the fact that charges in movement lose energy, and the
polarizability is, then, written as  [@Draine_AJ_88]: $$\begin{aligned}
\alpha_{\rm RR} & = & \frac{\alpha_{\rm CM}}{1-\frac{2}{3} i k_0^3
  n_{\rm mul} \alpha_{\rm CM}}. \end{aligned}$$ After different forms of
the polarizability have been established in order to improve the
precision of the DDA and take into account the non-punctual character of
the dipole, and we may quote, among the best known, the ones by Goedecke
and O'Brien [@Goedecke_AO_88], $$\begin{aligned}
\alpha_{\rm GB} & = & \frac{\alpha_{\rm CM}}{1-\frac{2}{3} i k_0^3
  n_{\rm mul} \alpha_{\rm CM}-k_0^2 \alpha_{\rm CM}/a}, \end{aligned}$$
by Lakhtakia [@Lakhtakia_IJMPC_92]: $$\begin{aligned}
\alpha_{\rm LA} & = & \frac{ \alpha_{\rm CM} }{1- 2
  \frac{\varepsilon-\varepsilon_{\rm mul}}{\varepsilon+2\varepsilon_{\rm
      mul}} \left[ (1-i k_0 n_{\rm mul} a)e^{i k_0 n_{\rm mul}
      a}-1\right] } \end{aligned}$$ and Draine and
Goodman [@Draine_AJ_93] $$\begin{aligned}
\alpha_{\rm LR} & = & \frac{ \alpha_{\rm CM}}{ 1 + \alpha_{\rm CM}
  \left[ \frac{(b_1+\varepsilon b_2/\varepsilon_{\rm mul} +\varepsilon
      b_3/\varepsilon_{\rm mul} S)k_0^2}{d}-\frac{2}{3} i n_{\rm mul}
    k_0^3 \right] },\end{aligned}$$ with $b_1=-1.891531$,
$b_2=0.1618469$, $b_3=-1.7700004$ and $S=1/5$.

Inside the code by default, it is $\alpha_{\rm RR}$ which is used. In
the case when the permittivity is anisotropic only $\alpha_{\rm RR}$ is
going to be used.

A last polarizability is introduced (PS) that only works for homogeneous
spheres and is particularly precise for metals. This consists of making
a change of the polarizability of the elements on the edge of the sphere
taking into account the factor of depolarization of the
sphere. [@Rahmani_AJ_04] Note that the sphere should be embedded in only
one layer.

Solve the system of linear equation
-----------------------------------

In order to know the electric field in the object, *i.e.* the field at
the position of the $N$ elements of discretization, we have to solve the
following system of linear equation: $$\begin{aligned}
\boldsymbol{E} = \boldsymbol{E}_0 + \boldsymbol{A} D_\alpha \boldsymbol{E},\end{aligned}$$
where $\boldsymbol{E}_0$ is a vector of size $3N$ which contains the
incident field at the discretization elements. $\boldsymbol{A}$ is a
matrix $3N\times 3N$ which contains all the field tensor susceptibility
and $D_\alpha$ is a diagonal matrix $3N\times 3N$, if the object is
isotropic, or diagonal block $3\times 3$ if the object is anisotropic.
$\boldsymbol{E}$ is the vector $3N$ which contains the unknown electric
local fields. The equation is solved by a non-linear iterative method.
The code proposes numerous iterative methods, and the one used by
default is GPBICG because it is the most efficient in most cases
 [@Chaumet_OL_09]. The code stops when the residue, $$\begin{aligned}
r & = & \frac{ \|\boldsymbol{E}-\boldsymbol{A} D_\alpha \boldsymbol{E} -\boldsymbol{E}_0\|} {
  \|\boldsymbol{E}_0 \|}, \end{aligned}$$ is under the tolerance given
by the user. $10^{-4}$ is the tolerance used by default, because it is a
good compromise between speed and precision. Please find below the
different iterative method possible in the code:

-   GPBICG1 : Ref. 

-   GPBICG2 : Ref. 

-   GPBICGsafe : Ref. 

-   GPBICGAR1 : Ref. 

-   GPBICGAR2 : Ref. 

-   QMRCLA : Ref. 

-   TFQMR : Ref. 

-   CG : Ref. 

-   BICGSTAB : Ref. 

-   QMRBICGSTAB1 : Ref. 

-   QMRBICGSTAB2 : Ref. 

-   GPBICOR : Ref. 

-   CORS : Ref. 

-   BiCGstar-plus Ref. 

Change of the initial guess
---------------------------

When the system of linear equations is solved iteratively, we have the
possibility to choose the starting point, *i.e.* the initial field
$\boldsymbol{E}_i$ to start the iterative method. The closer the
solution chosen at the beginning will be close to the "good solution",
the more the number of iterations will be reduced. We therefore propose
the possibility to choose as initial estimate for the field:

-   $\boldsymbol{E}_i=\boldsymbol{0}$ : null field at the beginning.

-   $\boldsymbol{E}_i=\boldsymbol{E}_0$: Born approximation.

-   Use of the scalar approximation
    $\boldsymbol{u}.\boldsymbol{G}\boldsymbol{u}$. In this case, the
    scalar approximation is also solved iteratively but for $r=0.01$. An
    additional precision would not be of interest, because we just want
    a correct starting point.

Preconditioning the system of linear equations
----------------------------------------------

Another solution is to precondition the matrix to be inverted on the
left to make the iterative method faster. That is to say instead of
solving
$(\boldsymbol{I}-\boldsymbol{A} \boldsymbol{D}_\alpha) \boldsymbol{E}= \boldsymbol{E}_0$,
we must then solve
$\boldsymbol{P}^{-1}(\boldsymbol{I}-\boldsymbol{A} \boldsymbol{D}_\alpha) \boldsymbol{E}= \boldsymbol{P}^{-1}\boldsymbol{E}_0$
where $\boldsymbol{P}$ is a matrix close to
$(\boldsymbol{I}-\boldsymbol{A} \boldsymbol{D}_\alpha)$ and whose
inverse can be computed easily. For this matrix we have chosen a matrix
of Chan [@Chan_SIAM_88] on the two dimensions of space $x$ and
$y$ [@Groth_JQSRT_20]. This preconditioning is particularly efficient
when the object under study is homogeneous or weakly inhomogeneous and
has a small thickness in $z$ compared to its dimensions in $x$ and $y$.
The preconditioning can also be also be done on the right side, *i.e.*
we have to find $\boldsymbol{X}$ such that
$(\boldsymbol{I}-\boldsymbol{A} \boldsymbol{D}_\alpha) \boldsymbol{P}^{-1} \boldsymbol{X}= \boldsymbol{E}_0$,
then deduce the field with
$\boldsymbol{E}= \boldsymbol{P}^{-1}\boldsymbol{X}$.

Note that this preconditioning is also implemented for the scalar
approximation.

The default options and how to change them
------------------------------------------

The default options chosen are:

-   The polarizability: $\alpha_{\rm RR}$.

-   The iterative method: GPBICG1.

-   The tolerance of the iterative method: $10^{-4}$.

-   The maximum number of iterations of the iterative method: 1000

-   The initial guess for the iterative method: Born approximation.

-   The preconditioning: no preconditioning.

-   Green's function interpolation: level 2

All these options can be changed. To do this you must click on "Advanced
interface", and appears at the bottom a xcwhole section section called
"Numerical parameters" where all the parameters related to the iterative
method and the polarizability can be adapted.
