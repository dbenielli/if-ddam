Managing of the configurations {#chap2}
==============================

Introduction
------------

The Code is launched by ./cdm inside the bin folder for a Linux
configuration. It has been created to be as convenient as possible and
so needing few explanations for its use. However, certain conventions
have been taken and need to be clarified.

Creation and saving of a new configuration
------------------------------------------

In order to start a new calculation, go to the tab *calculation* and
*New*. A new configuration shows up with values by default. Once the new
configuration is chosen, in order to be saved, the tab *Calculation* and
*Save* have to be selected again. Then, we select the name of the
configuration, and we may add a short description of the calculation
that has been made. Another way to save a configuration is to click
directly on the panel of the configuration *Save configuration*. Then,
two fields appear, one for the name of the configuration and the second
one for its description.

Managing of the configurations {#managing-of-the-configurations}
------------------------------

In order to manage all the selected configurations, we have go to the
tab *Calculation* and *Load*. So, a new window appears with all the
saved configurations. For each configuration there is a short
description that the user has entered, the date, when the configuration
file has been saved, then the principal characteristics of the
configuration (wave length, power, the beam's waist, object, material,
discretization and tolerance of the iterative method). It is enough to
click on a configuration and to click on *load* in order to load a
configuration.

The *delete* button is used to delete a saved configuration and the
*export* enables to export inside a file (name of the configuration.opt)
all the characteristics of the configuration.

Note that by double clicking on the line, we can modify the description
field.
