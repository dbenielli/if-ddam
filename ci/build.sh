#!/usr/bin/env bash

set -e
set -o pipefail
set -u
set -x

if [ -f /etc/os-release ]; then
    cat /etc/os-release
    . /etc/os-release
    if [ "${OSTYPE}" = "msys" ]; then
        # MSYS2/MinGW doesn't have VERSION_ID in /etc/os-release
        VERSION_ID=$( uname -r )
    fi
else
    ID=$( uname -s )
    # remove trailing text after actual version
    VERSION_ID=$( uname -r | sed "s/\([0-9\.]*\).*/\1/")
fi

# FIXME: the build system sets redundant RPATHs which trigger rpmbuild errors on
# Fedora ≥ 35, so suppress this rpmbuild check
if [ "${ID}" = "fedora" ]; then
  export QA_RPATHS=$(( 0x0001 ))
fi

META_DATA_DIR=Metadata/${ID}/${VERSION_ID}
mkdir -p ${META_DATA_DIR}
DIR=$(pwd)/Packages/${ID}/${VERSION_ID}
mkdir -p ${DIR}
ARCH=$( uname -m )
build_system=${build_system:-autotools}
GV_VERSION=$( cat IFDDAM_VERSION )
