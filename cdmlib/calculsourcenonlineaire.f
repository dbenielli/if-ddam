      subroutine calculsourcenonlineaire(nxm,nym,nzm,nepsmax,neps
     $     ,ndipole,nbsphere,numberobjetmax,tabdip,FFloc,epsilon
     $     ,polarisa,chi2 ,epscouche ,zcouche,zs ,object ,aretecube,k0
     $     ,FF,methode,trope ,namefile ,infostr ,nstop)
      implicit none 

      integer nxm,nym,nzm,nepsmax,neps,ndipole,nbsphere,nstop
     $     ,numberobjetmax
      integer Tabdip(nxm*nym*nzm)
      double complex FFloc(3*nxm*nym*nzm),epsilon(nxm*nym*nzm,3,3)
     $     ,polarisa(nxm*nym*nzm,3,3),chi2(27,numberobjetmax)
     $     ,epscouche(0:nepsmax+1) ,FF(3*nxm*nym*nzm)
      double precision zcouche(nepsmax),zs(nxm*nym*nzm),k0,aretecube
      character(64) object,namefile,infostr
      character(2) methode
      character(3) trope


      integer i,ii,jj,kk,ll,k,nsens,numerocouche,ntest,test1,test2
     $     ,test3,ndis,dddis,inv,long,ierror
      double complex Eloc(3),Em(3),eps0,epsani(3,3),ctmp,eps,polaeps(3
     $     ,3)
      dddis=1
      inv=1
      nsens=1
      write(*,*) 'coucou'
      if (object(1:9).eq.'arbitrary') then

c     faire une test sur fichier toto.nl si 1 lit chi si 2 lit epscouche
c     et espilon nouveau????
         long= len(trim(namefile))
         open(15,file=namefile(1:long)//'nl',status='old',iostat
     $        =ierror)
         write(*,*) 'cpico',long,namefile(1:long)
         if (ierror.ne.0) then
            infostr='arbitrary object for chi2: file does not exist'
            nstop=1
            return
         endif
         read(15,*) ndis
         do i=1,ndipole
            k=3*(i-1)
c     lit le chi2
            do ii=1,27
               read(15,*) chi2(ii,1)
            enddo
c     champ local en macro
            Eloc(1)= FFloc(k+1)
            Eloc(2)= FFloc(k+2)
            Eloc(3)= FFloc(k+3)
            do kk=1,3
               do ll=1,3
                  epsani(kk,ll)=epsilon(i,kk,ll)
               enddo
            enddo 
            eps0=epscouche(numerocouche(zs(i),neps,nepsmax,zcouche))
            call local_macro_surf(Eloc,Em,epsani,eps0,aretecube,k0
     $           ,nsens)
c     calcul source: chi ijk Ej Ek
c     xxx   1 ExEx
c     xxy   2 ExEy
c     xxz   3 ExEz
c     xyx   4 EyEx
c     xyy   5 EyEy
c     xyz   6 EyEz
c     xzx   7 EzEx
c     xzy   8 EzEy
c     xzz   9 EzEz

c     yxx   10 ExEx
c     yxy   11 ExEy
c     yxz   12 ExEz
c     yyx   13 EyEx
c     yyy   14 EyEy
c     yyz   15 EyEz
c     yzx   16 EzEx
c     yzy   17 EzEy
c     yzz   18 EzEz

c     zxx   19 ExEx
c     zxy   20 ExEy
c     zxz   21 ExEz
c     zyx   22 EyEx
c     zyy   23 EyEy
c     zyz   24 EyEz
c     zzx   25 EzEx
c     zzy   26 EzEy
c     zzz   27 EzEz         
            
            FF(k+1)=(chi2(1,1)*Em(1)*Em(1)+chi2(2,1)*Em(1)*Em(2)
     $           +chi2(3,1)*Em(1)*Em(3)+chi2(4,1)*Em(2)*Em(1)
     $           +chi2(5,1)*Em(2)*Em(2)+chi2(6,1)*Em(2)*Em(3)
     $           +chi2(7,1)*Em(3)*Em(1)+chi2(8,1)*Em(3)*Em(2)
     $           +chi2(9,1)*Em(3)*Em(3))/2.d0
            FF(k+2)=(chi2(10,1)*Em(1)*Em(1)+chi2(11,1)*Em(1)*Em(2)
     $           +chi2(12,1)*Em(1)*Em(3)+chi2(13,1)*Em(2)*Em(1)
     $           +chi2(14,1)*Em(2)*Em(2)+chi2(15,1)*Em(2)*Em(3)
     $           +chi2(16,1)*Em(3)*Em(1)+chi2(17,1)*Em(3)*Em(2)
     $           +chi2(18,1)*Em(3)*Em(3))/2.d0
            FF(k+3)=(chi2(19,1)*Em(1)*Em(1)+chi2(20,1)*Em(1)*Em(2)
     $           +chi2(21,1)*Em(1)*Em(3)+chi2(22,1)*Em(2)*Em(1)
     $           +chi2(23,1)*Em(2)*Em(2)+chi2(24,1)*Em(2)*Em(3)
     $           +chi2(25,1)*Em(3)*Em(1)+chi2(26,1)*Em(3)*Em(2)
     $           +chi2(27,1)*Em(3)*Em(3))/2.d0
         enddo
         write(*,*) 'double frequence',ndis
         k0=2.d0*k0
c     RR pour 2 k0 + changement pour après
         if (ndis.eq.1) then
            do i=0,neps+1
               read(15,*) epscouche(i)
               write(*,*) 'epscouch',epscouche(i)
            enddo
            do i=1,ndipole
               eps0=epscouche(numerocouche(zs(i),neps,nepsmax
     $              ,zcouche))
               if (trope.eq.'iso') then
                  read(15,*) eps
                  epsilon(i,1,1)=eps
                  epsilon(i,2,2)=eps
                  epsilon(i,3,3)=eps
                  epsilon(i,1,2)=0.d0
                  epsilon(i,1,3)=0.d0
                  epsilon(i,2,1)=0.d0
                  epsilon(i,2,3)=0.d0
                  epsilon(i,3,1)=0.d0
                  epsilon(i,3,2)=0.d0
                  call poladiffcomp(aretecube,eps,eps0,k0,dddis
     $                 ,methode,ctmp)  
                  polarisa(i,1,1)=ctmp
                  polarisa(i,2,2)=ctmp
                  polarisa(i,3,3)=ctmp
                  polarisa(i,1,2)=0.d0
                  polarisa(i,1,3)=0.d0
                  polarisa(i,2,1)=0.d0
                  polarisa(i,2,3)=0.d0
                  polarisa(i,3,1)=0.d0
                  polarisa(i,3,2)=0.d0
               else
                  do ii=1,3
                     do jj=1,3
                        read(15,*) epsani(ii,jj)
                     enddo
                  enddo
                  call polaepstenscomp(aretecube,epsani,eps0,k0,dddis
     $                 ,methode,inv,polaeps)
                  do ii=1,3
                     do jj=1,3
                        polarisa(i,ii,jj)=polaeps(ii,jj)
                        epsilon(i,ii,jj)=epsani(ii,jj)
                     enddo
                  enddo
               endif
            enddo
         else
c     reprend les valeurs de epsilon mais change la RR
            if (trope.eq.'iso') then
               write(*,*) 'recalcul pola pour 2 k0'
               do i=1,nbsphere
                  eps=epsilon(i,1,1)
                  eps0=epscouche(numerocouche(zs(i),neps,nepsmax
     $                 ,zcouche))
                  call poladiffcomp(aretecube,eps,eps0,k0,dddis,methode
     $                 ,ctmp)
                  polarisa(i,1,1)=ctmp
                  polarisa(i,2,2)=ctmp
                  polarisa(i,3,3)=ctmp
                  polarisa(i,1,2)=0.d0
                  polarisa(i,1,3)=0.d0
                  polarisa(i,2,1)=0.d0
                  polarisa(i,2,3)=0.d0
                  polarisa(i,3,1)=0.d0
                  polarisa(i,3,2)=0.d0
               enddo
            else
               do i=1,nbsphere
                  do ii=1,3
                     do jj=1,3
                        epsani(ii,jj)=epsilon(i,ii,jj)
                     enddo
                  enddo
                  call polaepstenscomp(aretecube,epsani,eps0,k0,dddis
     $                 ,methode,inv,polaeps)
                  do ii=1,3
                     do jj=1,3
                        polarisa(i,ii,jj)=polaeps(ii,jj)
                     enddo
                  enddo
               enddo
            endif
         endif
         close(15)
      else
c     cas objet homogene
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,Eloc,Em,ii,jj,epsani,eps0)  
!$OMP& PRIVATE(kk,test1,test2,test3,ntest) 
!$OMP DO SCHEDULE(STATIC)   
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               kk=3*(k-1)
c     champ local en macro
               Eloc(1)= FFloc(kk+1)
               Eloc(2)= FFloc(kk+2)
               Eloc(3)= FFloc(kk+3)
               do ii=1,3
                  do jj=1,3
                     epsani(ii,jj)=epsilon(k,ii,jj)
                  enddo
               enddo 
               eps0=epscouche(numerocouche(zs(k),neps,nepsmax,zcouche))
               call local_macro_surf(Eloc,Em,epsani,eps0,aretecube,k0
     $              ,nsens)
               call comparaisoncomplexe(Eloc(1),Em(1),test1)
               call comparaisoncomplexe(Eloc(2),Em(2),test2)
               call comparaisoncomplexe(Eloc(3),Em(3),test3)
               ntest=test1+test2+test3
               if (ntest.ne.0) then
c     calcul source
                  FF(kk+1)=(chi2(1,1)*Em(1)*Em(1)+chi2(2,1)*Em(1)*Em(2)
     $                 +chi2(3,1)*Em(1)*Em(3)+chi2(4,1)*Em(2)*Em(1)
     $                 +chi2(5,1) *Em(2)*Em(2)+chi2(6,1)*Em(2)*Em(3)
     $                 +chi2(7,1)*Em(3) *Em(1)+chi2(8,1)*Em(3)*Em(2)
     $                 +chi2(9,1)*Em(3)*Em(3)) /2.d0
                  FF(kk+2)=(chi2(10,1)*Em(1)*Em(1)+chi2(11,1)*Em(1)*Em(2
     $                 )+chi2(12,1)*Em(1)*Em(3)+chi2(13,1)*Em(2)*Em(1)
     $                 +chi2(14,1)*Em(2)*Em(2)+chi2(15,1)*Em(2)*Em(3)
     $                 +chi2(16,1)*Em(3)*Em(1)+chi2(17,1)*Em(3)*Em(2)
     $                 +chi2(18,1)*Em(3)*Em(3))/2.d0
                  FF(kk+3)=(chi2(19,1)*Em(1)*Em(1)+chi2(20,1)*Em(1)*Em(2
     $                 )+chi2(21,1)*Em(1)*Em(3)+chi2(22,1)*Em(2)*Em(1)
     $                 +chi2(23,1)*Em(2)*Em(2)+chi2(24,1)*Em(2)*Em(3)
     $                 +chi2(25,1)*Em(3)*Em(1)+chi2(26,1)*Em(3)*Em(2)
     $                 +chi2(27,1)*Em(3)*Em(3))/2.d0

               else
                  FF(kk+1)=0.d0
                  FF(kk+2)=0.d0
                  FF(kk+3)=0.d0
               endif
            endif

         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

c     champ pola pour RR a 2k0
         k0=2.d0*k0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC)
         do i=1,nbsphere
            polarisa(i,1,1)=0.d0
            polarisa(i,1,2)=0.d0
            polarisa(i,1,3)=0.d0
            polarisa(i,2,1)=0.d0
            polarisa(i,2,2)=0.d0
            polarisa(i,2,3)=0.d0
            polarisa(i,3,1)=0.d0
            polarisa(i,3,2)=0.d0
            polarisa(i,3,3)=0.d0     
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  
         if (trope.eq.'iso') then

            do i=1,nbsphere
               eps=epsilon(i,1,1)
               eps0=epscouche(numerocouche(zs(i),neps,nepsmax ,zcouche))
               call poladiffcomp(aretecube,eps,eps0,k0,dddis,methode
     $              ,ctmp)
               polarisa(i,1,1)=ctmp
               polarisa(i,2,2)=ctmp
               polarisa(i,3,3)=ctmp

            enddo
         else
            do i=1,nbsphere
               do ii=1,3
                  do jj=1,3
                     epsani(ii,jj)=epsilon(i,ii,jj)
                  enddo
               enddo
               call polaepstenscomp(aretecube,epsani,eps0,k0,dddis
     $              ,methode,inv,polaeps)
               do ii=1,3
                  do jj=1,3
                     polarisa(i,ii,jj)=polaeps(ii,jj)
                  enddo
               enddo
            enddo
         endif
      endif

      end
