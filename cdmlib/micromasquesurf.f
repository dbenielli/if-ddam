      subroutine micromasquesurf(masque,imaxinc,imaxk0,nbitermic
     $     ,ntypemic,nfft2d,deltak,deltakx,numaperinc,numaperinc2,kcnax
     $     ,kcnay,aretecube ,k0,xs,ys,nxm,nym ,nzm,nbsphere,nquicklens
     $     ,indice0 ,nstop,infostr)
      implicit none
      integer imaxinc,ntypemic,nquicklens,nstop,nbitermic,sizemask
     $     ,nfft2d,nxm,nym ,nzm,nbsphere,i,j,idelta,jdelta,ii,imaxk0
     $     ,imul,k
      double precision deltakinc,k0,aretecube,deltakm,xmin,xmax ,ymin
     $     ,ymax,numaperinc,numaperinc2,kcnax,kcnay,d,pi,numaperk
     $     ,numaperk2,deltak,deltaky,deltakx,kxinc,kyinc,indice0
      DOUBLE PRECISION,DIMENSION(nxm*nym*nzm)::xs,ys
      double complex masque(nfft2d*nfft2d)
      character(64) infostr

c     verifie si les ouvertures numériques sont bonnes suivant le type
c     de microscope.
      write(*,*) 'Compute the mask'
      if (ntypemic.eq.1) then         
         if (numaperinc.ge.1.d0.or.numaperinc.le.0.d0) then
            infostr='NA inc strictly between 0 and 1'
            nstop=1
            return
         endif
c     dark field avec anneau
      elseif (ntypemic.eq.2 .or. ntypemic.eq.5) then
         if (numaperinc2.ge.numaperinc) then
            infostr='NA inc 2 larger than NA inc!'
            nstop=1
            return
         endif
         if (numaperinc2.lt.0.d0) then
            infostr='NA inc 2 smaller than 0'
            nstop=1
            return
         endif
         if (ntypemic.eq.5) then
      		write(*,*) 'amod',kcnax,'phase',kcnay
      		endif
c     shriefeld
      elseif (ntypemic.eq.3) then
         d=dsqrt(kcnax*kcnax+kcnay*kcnay)/indice0
         if (d+numaperinc.ge.1.d0) then
            infostr='pupil larger than n'
            nstop=1
            return
         endif
         if (numaperinc.le.0.d0) then
            infostr='NA inc strictly larger than 0'
            nstop=1
            return
         endif
         if (dabs(kcnax)/indice0.ge.1.d0) then
            infostr='X position of the pupil larger than NA'
            nstop=1
            return
         endif
         if (dabs(kcnay)/indice0.ge.1.d0) then
            infostr='Y position of the pupil larger than NA'
            nstop=1
            return
         endif
      endif
c     calcul de deltak      
      xmax=-1.d300
      xmin=1.d300
      ymax=-1.d300
      ymin=1.d300
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i)
!$OMP DO SCHEDULE(STATIC) REDUCTION(max:xmax,ymax)
!$OMP& REDUCTION(min:xmin,ymin)      
      do i=1,nbsphere
         xmax=max(xmax,xs(i))
         xmin=min(xmin,xs(i))
         ymax=max(ymax,ys(i))
         ymin=min(ymin,ys(i))
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      pi=dacos(-1.d0)
      deltakm=pi/max(xmax-xmin,ymax-ymin)
     
      numaperk=k0*numaperinc*indice0
      numaperk2=k0*numaperinc2*indice0
      write(*,*) 'indice',indice0,numaperk/k0,numaperk2/k0
      if (nquicklens.eq.1) then
         
         deltakx=2.d0*pi/(aretecube*dble(nfft2d))

         if (deltakx.ge.numaperk) then
            nstop=1
          infostr='In FFT lens nfft2d too small:Increase size of FFT 1'
            return
         endif        
         imul=idint(deltakm/deltakx)
         if (imul.eq.0) then
            nstop=1
          infostr='In FFT lens nfft2d too small:Increase size of FFT 2'
            return            
         endif
 223     deltak=deltakx*dble(imul)

         imaxinc=nint(numaperk/deltak)+1

         if (imaxinc.le.3) then
            imul=imul-1
            if (imul.eq.0) then
               nstop=1
          infostr='In FFT lens nfft2d too small:Increase size of FFT 3'
               return
            endif
            goto 223
         endif
         write(*,*) 'Final delta k incident   : ',deltak/k0,'m-1',imul
         write(*,*) 'Final delta k diffracted : ',deltakx/k0,'m-1'
         if (ntypemic.eq.2 .or. ntypemic.eq.5) then
            if (deltak.gt.(numaperk-numaperk2)/2.d0) then
                imul=imul-1
                if (imul.eq.0) then
                   nstop=1
          infostr='In FFT lens nfft2d too small:Increase size of FFT 3'
                   return
                endif
                goto 223
            endif
         endif
       
      else
         k=0
 224     deltakx=2.d0*pi/(dble(nfft2d)*aretecube)/dble(2**k)
         imaxk0=nint(numaperk/deltakx)+1

         if (imaxk0.le.5) then
            k=k+1
            write(*,*) 'Change delta k diffracted:',deltakx,'m-1',k
            goto 224
         endif
         imul=idint(deltakm/deltakx)
             
 222     deltak=deltakx*dble(imul)
         if (imul.eq.0) then
            k=k+1
            write(*,*) 'Change delta k diffracted:',deltakx,'m-1',k
            goto 224
         endif
         imaxinc=nint(numaperk/deltak)+1
        
         if (imaxinc.le.4) then
            imul=imul-1
            write(*,*) 'Change delta k incident:',deltak,'m-1',k
            goto 222
         endif
         if (ntypemic.eq.2 .or. ntypemic.eq.5) then
            if (deltak.gt.(numaperk-numaperk2)/2.d0) then
               imul=imul-1
               write(*,*) 'Change delta k incident:',deltak,'m-1',k
               goto 222
            endif
         endif
         write(*,*) 'Final delta k incident   : ',deltak/k0,'m-1'
         write(*,*) 'Final delta k diffracted : ',deltakx/k0,'m-1'
         write(*,*) 'ratio                    : ',deltak/deltakx
      endif

      deltaky=deltakx
      sizemask=2*imaxinc+1
      if (sizemask.gt.100) then
         nstop=1
         infostr='Too many illumination=too many time'
         return
      endif
      
      write(*,*)  'sizemask',sizemask,imaxinc,ntypemic
c     alloue le masque
    
      write(*,*) 'prout',numaperk,numaperk2,deltak,imaxinc
c     calcul le masque suivant l'option
      if (ntypemic.eq.1) then
         ii=0
         do idelta=-imaxinc,imaxinc
            do jdelta=-imaxinc,imaxinc
               kxinc=idelta*deltak
               kyinc=jdelta*deltak
               i=imaxinc+idelta+1
               j=imaxinc+jdelta+1
               if (kxinc*kxinc+kyinc*kyinc.le.numaperk*numaperk) then
                  masque(i+(j-1)*(2*imaxinc+1))=1.d0
                  ii=ii+1
               else
                  masque(i+(j-1)*(2*imaxinc+1))=1.d100
               endif
            enddo
         enddo
         
      elseif (ntypemic.eq.2.or.ntypemic.eq.5) then
         ii=0
         do idelta=-imaxinc,imaxinc
            do jdelta=-imaxinc,imaxinc
               kxinc=idelta*deltak
               kyinc=jdelta*deltak
               i=imaxinc+idelta+1
               j=imaxinc+jdelta+1
               if (kxinc*kxinc+kyinc*kyinc.le.numaperk*numaperk .and.
     $              kxinc*kxinc+kyinc*kyinc.ge.numaperk2*numaperk2) then
                  masque(i+(j-1)*(2*imaxinc+1))=1.d0
                  ii=ii+1
               else
                  masque(i+(j-1)*(2*imaxinc+1))=1.d100                
               endif
            enddo
         enddo
      elseif (ntypemic.eq.3) then
         write(*,*) 'Numerical aperture:',numaperk/k0
         write(*,*) 'Position of NA (x):',kcnax
         write(*,*) 'Position of NA (y):',kcnay

         d=dsqrt(kcnax*kcnax+kcnay*kcnay)/indice0
         imaxinc=nint((d+numaperinc)*k0*indice0/deltak)
         ii=0
         do idelta=-imaxinc,imaxinc
            do jdelta=-imaxinc,imaxinc
               kxinc=idelta*deltak
               kyinc=jdelta*deltak
               i=imaxinc+idelta+1
               j=imaxinc+jdelta+1
               if ((kxinc-kcnax*k0)**2+(kyinc-kcnay*k0)**2 .le. numaperk
     $              *numaperk) then               
                  masque(i+(j-1)*(2*imaxinc+1))=1.d0
                  ii=ii+1
               else
                  masque(i+(j-1)*(2*imaxinc+1))=1.d100
               endif
            enddo
         enddo
      endif

      nbitermic=ii
      if (nbitermic.le.8) then
         nstop=1
         infostr='Not enough illumination: increase size FFT'
         return 
      endif

      write(*,*) 'Number of incidence',nbitermic
      end
