c**************************************************************************
c**************************************************************************
c**************************************************************************
      subroutine ZBICGSTABL(XI,XR,B,Xsol,NORM,WRK,ALPHA,RHO,OMEGA,L,nr
     $     ,nu,J,LDA,NDIM,NLAR,NOU,NSTAT,STEPERR,NLOOP,MAXIT,TOLE,TOL)
      IMPLICIT NONE

      INTEGER I,J,NLOOP,MAXIT,L,LMAX,nr,nu,ITNO

C     constant
      PARAMETER (LMAX=10)

C     array
      INTEGER LDA,NDIM,NLAR,NOU,NSTAT,STEPERR,ii
      DOUBLE COMPLEX B(LDA),WRK(LDA,NLAR),XR(LDA),XI(LDA),XSOL(LDA)

c     Local Scalars
      DOUBLE PRECISION TOL,NORM,TOLE,RESIDU
      DOUBLE COMPLEX ALPHA,BETA,OMEGA,RHO,RHO1,GAMMA0,GAMMA(LMAX)
     $     ,GAMMAP(LMAX),GAMMAPP(LMAX),SIGMA(LMAX),TAU(LMAX,LMAX),ctmp

c     en entre nou=0,nloop=0

      IF (NOU.EQ.1) GOTO 10
      IF (NOU.EQ.2) GOTO 20
      IF (NOU.EQ.3) GOTO 30

      NSTAT = 0
      STEPERR = -1
c     r0tilde=1
c     u=2,L+2  
c     r=L+3,2L+3     
      nu=2
      nr=L+3
      if (nlar.lt.2*L+3) then
         NSTAT=-1
         STEPERR=0
         return
      endif
 
c     initialise
      NOU=1  
      NSTAT=0  
      ctmp=0.D0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  REDUCTION(+:ctmp)        
      DO ii=1,NDIM
         Xsol(ii)=xi(ii)    
         ctmp=ctmp+B(ii)*DCONJG(B(ii))
      ENDDO
!$OMP ENDDO 
!$OMP END PARALLEL       
      NORM=dsqrt(cdabs(ctmp))
      RETURN 
c     r=(b-Ax) CALCUL DU RESIDU
 10   RHO=(1.d0,0.d0)
      alpha=(0.d0,0.d0)
      omega=(1.d0,0.d0)
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)       
      DO ii=1,NDIM
         WRK(ii,1)=B(ii)-XR(ii)
         WRK(ii,nr)=WRK(ii,1)
      ENDDO
!$OMP ENDDO 
!$OMP END PARALLEL
      
      itno=-1
 100  itno=itno+1
    
      rho = -omega * rho
     
      if (cdabs(rho).eq.0.d0) then
         NSTAT=-1
         STEPERR=1
         return
      endif
C     Bi-CG Part
      J=-1
 200  J=J+1
     
      rho1=0.d0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  REDUCTION(+:rho1)       
      do ii=1,ndim
         rho1=rho1+dconjg(WRK(ii,1))*WRK(ii,nr+j)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
      beta=alpha*rho1/rho
    
      rho=rho1
      if (cdabs(rho).eq.0.d0) then
         NSTAT=-1
         STEPERR=1
         return
      endif
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  COLLAPSE(2)    
      do i=0,j
         do ii=1,ndim
            WRK(ii,nu+i)=WRK(ii,nr+i)-beta*WRK(ii,nu+i)
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)      
      do ii=1,ndim
         xi(ii)=WRK(ii,nu+j)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL     
      NOU=2
      RETURN
      
 20   gamma0=(0.d0,0.d0)
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  REDUCTION(+:gamma0)   
      do ii=1,ndim
         WRK(ii,nu+j+1)=XR(ii)
         gamma0=gamma0+dconjg(WRK(ii,1))*XR(ii)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL     

      if (cdabs(gamma0).eq.0.d0) then
         NSTAT=-1
         STEPERR=2
         return
      endif
      alpha=rho/gamma0
      do i=0,j
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)           
         do ii=1,ndim
            WRK(ii,nr+i)=WRK(ii,nr+i)-alpha*WRK(ii,nu+i+1)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL           
      enddo
      
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)      
      do ii=1,ndim
         XI(ii)=WRK(ii,nr+j)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL      
      nou=3
      return
      
 30   nou=3
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
      do ii=1,ndim
         WRK(ii,nr+j+1)=XR(ii)
         xsol(ii)=xsol(ii)+alpha*WRK(ii,nu)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
      if (J.LT.L-1) goto 200
c     end of Bi-CG part
      
c     Minimum Residual part
      do j=1,L
         do i=1,j-1
            tau(i,j)=(0.d0,0.d0)
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:tau)               
            do ii=1,ndim
               tau(i,j)=tau(i,j)+dconjg(WRK(ii,nr+j))*WRK(ii,nr+i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL            
            tau(i,j)=tau(i,j)/sigma(i)
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)            
            do ii=1,ndim
               WRK(ii,nr+j)=WRK(ii,nr+j)-tau(i,j)*WRK(ii,nr+i)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL            
         enddo
         sigma(j)=(0.d0,0.d0)
         gammap(j)=(0.d0,0.d0)
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:sigma,gammap)          
         do ii=1,ndim
            sigma(j)=sigma(j)+dconjg(WRK(ii,nr+j))*WRK(ii,nr+j)
            gammap(j)=gammap(j)+dconjg(WRK(ii,nr))*WRK(ii,nr+j)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         gammap(j)=gammap(j)/sigma(j)
      enddo

      gamma(L)=gammap(L)
      omega=gamma(L)
      do j=L-1,1,-1
         gamma(j)=gammap(j)
         do i=j+1,L
            gamma(j)=gamma(j)-tau(j,i)*gamma(i)
         enddo
      enddo
      
      do j=1,L-1
         gammapp(j)=gamma(j+1)
         do i=j+1,L-1
            gammapp(j)=gammapp(j)+tau(j,i)*gamma(i+1)
         enddo
      enddo
      
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
      do ii=1,ndim
         xsol(ii)=xsol(ii)+gamma(1)*WRK(ii,nr)
         WRK(ii,nr)=WRK(ii,nr)-gammap(L)*WRK(ii,nr+L)
         WRK(ii,nu)=WRK(ii,nu)-gamma(L)*WRK(ii,nu+L)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      do j=1,L-1
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)         
         do ii=1,ndim
            WRK(ii,nu)=WRK(ii,nu)-gamma(j)*WRK(ii,nu+j)
            xsol(ii)=xsol(ii)+gammapp(j)*WRK(ii,nr+j)
            WRK(ii,nr)=WRK(ii,nr)-gammap(j)*WRK(ii,nr+j)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
      enddo

      ctmp=0.d0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ctmp)  
      do ii=1,ndim
         ctmp=ctmp+dconjg(WRK(ii,nr))*WRK(ii,nr)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
      RESIDU=dsqrt(cdabs(ctmp))/NORM
      write(*,*) 'RESIDU',RESIDU,ITNO
      if (RESIDU.le.TOL) then
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)           
          do ii=1,ndim
            xi(ii)=xsol(ii)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL   
         NSTAT=1  
         nou=4
         return
      endif

      goto 100
      
      END
