c     cette routine multiplie un vecteur par la matrice inverse estimée
c     de la matrice toeplitz initialie.

      subroutine preconditionneursurfsdet(polarisa,a ,nplanm ,nmatim,n1m
     $     ,nbs,matind,matindplan ,matindice ,matrange ,nx,ny ,nz
     $     ,nbsphere ,ndipole,nxm,nym ,nzm ,ninterp,aretecube,k0 ,sdetnn
     $     ,ipvtnn ,infostr ,nstop)
      implicit none
      integer nx,ny,nz,nbsphere,nstop,nxm,nym,nzm,ntest,nz3,ndipole,ic
     $     ,il,ninterp
      double precision x,y,z,x0,y0,z0,aretecube,k0,test,tmp,tmp1,tmp2
      double complex polarisa(nxm*nym*nzm,3,3),propaesplibre(3,3)

      integer nplanm,n1m,nbs,nmatim
      double precision a(0:2*n1m*n1m),sphi,cphi,s2phi,c2phi
      integer matindice(nplanm,nmatim) ,matind(0:2*n1m*n1m)
     $     ,matindplan(nzm,nzm)
      double complex matrange(nbs,5),Ixx,Ixy,Ixz,Izx,Izz,Sxx,Sxy,Sxz,Syy
     $     ,Szx,Syz,Szy

      character(64) infostr

      integer i,j,k,ii,jj,kk,ix,iy,iz,i2,j2,k2,kkk,l,ll,k1,j1,nn
      double precision dn1x,dn2x,dn1y,dn2y
      double complex polamoy,dn11c,dn12c,dn21c,dn22c
      double complex sdetnn(3*nzm,3*nzm ,nxm*nym)

      integer n1,n2,n3
      double precision dnxy,normefrobenius

      integer job,lda,info,lwork,NRHS
      double complex  deterz(2)
      
      integer nmaxcompo
      double complex , dimension (:,:), allocatable :: sdettmp
      double complex , dimension (:), allocatable :: Txx,Txy,Txz ,Tyy
     $     ,Tyz ,Tzx, Tzy, Tzz,chanxx,chanxy,chanxz,chanyy,chanyz,chanzx
     $     , chanzy, chanzz,worktmp ,work
      integer , dimension (:),allocatable ::ipvttmp
      integer jx,jy,ipvtnn(nzm*3,nxm*nym)
      double complex , dimension (:,:), allocatable :: spola
      integer nav1,nav2,nap1,nap2,n,nlong,kkav1,kkav2,kkap1,kkap2
      double precision long,If1,If2,If3,If4
      double precision xa(10),dinterp
      double complex ya(10),dy

      integer FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD
      integer*8 plan2b,plan2f
      integer valuesf(8),valuesi(8)
      double precision tf,ti
      character(64) message
      character(8)  :: date
      character(10) :: time
      character(5)  :: zone
      
      FFTW_FORWARD=-1
      FFTW_BACKWARD=+1
      FFTW_ESTIMATE=64
      nz3=nz*3

c     ****************************************
c     calcul polarisabilité moyenne
c     ****************************************
c         message='Chan'
c         call cpu_time(ti)
c         call date_and_time(date,time,zone,valuesi)
      polamoy=0.d0
      k=0
                                !$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k) 
                                !$OMP DO SCHEDULE(STATIC) REDUCTION(+:polamoy)         
      do i=1,nbsphere
         polamoy=polamoy+polarisa(i,2,2)
         if (cdabs(polarisa(i,2,2)).ne.0) k=k+1
      enddo
                                !$OMP ENDDO 
                                !$OMP END PARALLEL         
      polamoy=polamoy/dble(k)
      write(*,*) 'polamoy',polamoy,ndipole
      normefrobenius=0.d0
      tmp=0.d0
      tmp1=0.d0
      tmp2=0.d0


      dnxy=dble(nx*ny)

c     Il faut effectuer une TF de longueur nx sur ny*nz, cad ny*nz FFT
c     de longueur nx
      x0=0.d0
      y0=0.d0
      z0=0.d0
c     boucle sur ny et nz
      nmaxcompo=nx*ny
      allocate(Txx(1:nmaxcompo))
      allocate(Txy(1:nmaxcompo))
      allocate(Txz(1:nmaxcompo))
      allocate(Tyy(1:nmaxcompo))
      allocate(Tyz(1:nmaxcompo))
      allocate(Tzx(1:nmaxcompo))
      allocate(Tzy(1:nmaxcompo))
      allocate(Tzz(1:nmaxcompo))
      allocate(chanxx(1:nmaxcompo))
      allocate(chanxy(1:nmaxcompo))
      allocate(chanxz(1:nmaxcompo))
      allocate(chanyy(1:nmaxcompo))
      allocate(chanyz(1:nmaxcompo))
      allocate(chanzx(1:nmaxcompo))
      allocate(chanzy(1:nmaxcompo))
      allocate(chanzz(1:nmaxcompo))
      allocate(spola(nx,ny))
      
#ifdef USE_FFTW
      call dfftw_plan_dft_2d(plan2b,nx,ny,chanxx,chanxx ,FFTW_BACKWARD
     $     ,FFTW_ESTIMATE)
      call dfftw_plan_dft_2d(plan2f,nx,ny,chanxx,chanxx ,FFTW_FORWARD
     $     ,FFTW_ESTIMATE)
#endif

      n=4
      ya=0.d0
      xa=0.d0
      dy=0.d0
      dinterp=dble(ninterp)

      write(*,*) 'ninterp',ninterp
      do il=1,nz
         do ic=1,nz
         
c     calcul du propagateur
            if (ninterp.eq.0) then
              
               do j=1,ny
                  do k=1,nx               
                     x=aretecube*dble(k-1)
                     y=aretecube*dble(j-1)
                     z=aretecube*dble(ic-1)
                     z0=aretecube*dble(il-1)
                     
                     n1=(k-1)*(k-1)+(j-1)*(j-1)
                     kk=matindice(matindplan(ic,il),matind(n1))
                     Ixx=matrange(kk,1)
                     Ixy=matrange(kk,2)
                     Ixz=matrange(kk,3)
                     Izz=matrange(kk,4)
                     Izx=matrange(kk,5)
                     
                     if (il.le.ic) then
                        sphi=aretecube*dble(k-1)/a(n1)
                        cphi=aretecube*dble(j-1)/a(n1)
                        s2phi=2.d0*sphi*cphi
                        c2phi=cphi*cphi-sphi*sphi
                        
c                        call propa_espace_libre(x,y,z,x0,y0,z0,k0
c     $                       ,propaesplibre)
c                        write(*,*) 'yyyyyyyyyyyyy',j,k,il,ic
c                        write(*,*) Ixx+c2phi*Ixy,propaesplibre(1,1)
c                        write(*,*) -s2phi*Ixy,propaesplibre(1,2)
c                        write(*,*) sphi*Ixz,propaesplibre(1,3)
c                        write(*,*) Ixx-c2phi*Ixy,propaesplibre(2,2)
c                        write(*,*) cphi*Ixz,propaesplibre(2,3)
c                        write(*,*) sphi*Izx,propaesplibre(3,1)
c                        write(*,*) cphi*Izx,propaesplibre(3,2)
c                        write(*,*) Izz,propaesplibre(3,3)
                        Txx(k+nx*(j-1))=-(Ixx+c2phi*Ixy)
                        Txy(k+nx*(j-1))=-(-s2phi*Ixy)
                        Txz(k+nx*(j-1))=-(sphi*Ixz)
                        Tyy(k+nx*(j-1))=-(Ixx-c2phi*Ixy)
                        Tyz(k+nx*(j-1))=-(cphi*Ixz)
                        Tzx(k+nx*(j-1))=-(sphi*Izx)
                        Tzy(k+nx*(j-1))=-(cphi*Izx)
                        Tzz(k+nx*(j-1))=-(Izz)
                     else
                        
                        sphi=aretecube*dble(k-1)/a(n1)
                        cphi=aretecube*dble(j-1)/a(n1)
                        s2phi=2.d0*sphi*cphi
                        c2phi=cphi*cphi-sphi*sphi
                        
c                        call propa_espace_libre(x,y,z,x0,y0,z0,k0
c     $                       ,propaesplibre)
c                        write(*,*) 'yyyyyyyyyyyyy',j,k,il,ic
c                        write(*,*) Ixx+c2phi*Ixy,propaesplibre(1,1)
c                        write(*,*) -s2phi*Ixy,propaesplibre(1,2)
c                        write(*,*) -sphi*Izx,propaesplibre(1,3)
c                        write(*,*) Ixx-c2phi*Ixy,propaesplibre(2,2)
c                        write(*,*) -cphi*Izx,propaesplibre(2,3)
c                        write(*,*) -sphi*Ixz,propaesplibre(3,1)
c                        write(*,*) -cphi*Ixz,propaesplibre(3,2)
c                        write(*,*) Izz,propaesplibre(3,3)
                        Txx(k+nx*(j-1))=-(Ixx+c2phi*Ixy)
                        Txy(k+nx*(j-1))=-(-s2phi*Ixy)
                        Txz(k+nx*(j-1))=-(-sphi*Izx)
                        Tyy(k+nx*(j-1))=-(Ixx-c2phi*Ixy)
                        Tyz(k+nx*(j-1))=-(-cphi*Izx)
                        Tzx(k+nx*(j-1))=-(-sphi*Ixz)
                        Tzy(k+nx*(j-1))=-(-cphi*Ixz)
                        Tzz(k+nx*(j-1))=-(Izz)
                     endif
                     
                  enddo
               enddo
            else
               do j=1,ny
                  do k=1,nx               
                     x=aretecube*dble(k-1)
                     y=aretecube*dble(j-1)
                     z=aretecube*dble(ic-1)
                     z0=aretecube*dble(il-1)
                     
                     n1=(k-1)*(k-1)+(j-1)*(j-1)

                     if (n1.eq.0) then
                        kk=matindice(matindplan(ic,il),1)
                        Ixx=matrange(kk,1)
                        Ixy=matrange(kk,2)
                        Ixz=matrange(kk,3)
                        Izz=matrange(kk,4)
                        Izx=matrange(kk,5)
                        long=1.d300
                     else
                        long=dsqrt(dble(n1*ninterp*ninterp))
                        nlong=nint(long) 
                        if (dabs(dble(nlong)-long).le.1.d-10) then
c     test pour savoir si long est un entier
                           kk=matindice(matindplan(ic,il),nlong+1)
                           Ixx=matrange(kk,1)
                           Ixy=matrange(kk,2)
                           Ixz=matrange(kk,3)
                           Izz=matrange(kk,4)
                           Izx=matrange(kk,5)
                        else
                           nav1=floor(long)
                           nav2=nav1-1
                           nap1=ceiling(long)
                           nap2=nap1+1
                           kkav1=matindice(matindplan(ic,il),nav1+1)
                           kkav2=matindice(matindplan(ic,il),nav2+1)
                           kkap1=matindice(matindplan(ic,il),nap1+1)
                           kkap2=matindice(matindplan(ic,il),nap2+1)

c     interpolation par une fonction rationelle a quatre abscisses

                           xa(1)=dble(nav2)
                           xa(2)=dble(nav1)
                           xa(3)=dble(nap1)
                           xa(4)=dble(nap2)


                           ya(1)=matrange(kkav2,1)
                           ya(2)=matrange(kkav1,1)
                           ya(3)=matrange(kkap1,1)
                           ya(4)=matrange(kkap2,1)
                           call ratint(xa,ya,n,long,Ixx,dy)

                           ya(1)=matrange(kkav2,2)
                           ya(2)=matrange(kkav1,2)
                           ya(3)=matrange(kkap1,2)
                           ya(4)=matrange(kkap2,2)
                           call ratint(xa,ya,n,long,Ixy,dy)

                           ya(1)=matrange(kkav2,3)
                           ya(2)=matrange(kkav1,3)
                           ya(3)=matrange(kkap1,3)
                           ya(4)=matrange(kkap2,3)
                           call ratint(xa,ya,n,long,Ixz,dy)

                           ya(1)=matrange(kkav2,4)
                           ya(2)=matrange(kkav1,4)
                           ya(3)=matrange(kkap1,4)
                           ya(4)=matrange(kkap2,4)
                           call ratint(xa,ya,n,long,Izz,dy)

                           ya(1)=matrange(kkav2,5)
                           ya(2)=matrange(kkav1,5)
                           ya(3)=matrange(kkap1,5)
                           ya(4)=matrange(kkap2,5)
                           call ratint(xa,ya,n,long,Izx,dy)


                        endif 
                     endif
c                     write(*,*) 'rrr',Ixx,Izz,Ixy,Izx,Ixz
                     if (il.le.ic) then
                        sphi=dble(k-1)/long*dinterp
                        cphi=dble(j-1)/long*dinterp
                        s2phi=2.d0*sphi*cphi
                        c2phi=cphi*cphi-sphi*sphi
c                        write(*,*) 'sss',sphi,cphi,c2phi,s2phi,dble(k-1)
c     $                       ,long/dinterp
c                        call propa_espace_libre(x,y,z,x0,y0,z0,k0
c     $                       ,propaesplibre)
c                        write(*,*) 'xxxxxxxxxxxxxxx',j,k,il,ic
c                        write(*,*) Ixx+c2phi*Ixy,propaesplibre(1,1)
c                        write(*,*) -s2phi*Ixy,propaesplibre(1,2)
c                        write(*,*) sphi*Ixz,propaesplibre(1,3)
c                        write(*,*) Ixx-c2phi*Ixy,propaesplibre(2,2)
c                        write(*,*) cphi*Ixz,propaesplibre(2,3)
c                        write(*,*) sphi*Izx,propaesplibre(3,1)
c                        write(*,*) cphi*Izx,propaesplibre(3,2)
c                        write(*,*) Izz,propaesplibre(3,3)
                        Txx(k+nx*(j-1))=-(Ixx+c2phi*Ixy)
                        Txy(k+nx*(j-1))=-(-s2phi*Ixy)
                        Txz(k+nx*(j-1))=-(sphi*Ixz)
                        Tyy(k+nx*(j-1))=-(Ixx-c2phi*Ixy)
                        Tyz(k+nx*(j-1))=-(cphi*Ixz)
                        Tzx(k+nx*(j-1))=-(sphi*Izx)
                        Tzy(k+nx*(j-1))=-(cphi*Izx)
                        Tzz(k+nx*(j-1))=-(Izz)
                     else
                        
                        sphi=dble(k-1)/long*dinterp
                        cphi=dble(j-1)/long*dinterp
                        s2phi=2.d0*sphi*cphi
                        c2phi=cphi*cphi-sphi*sphi
                        
c                        call propa_espace_libre(x,y,z,x0,y0,z0,k0
c     $                       ,propaesplibre)
c                        write(*,*) 'zzzzzzzzzzzzzzz',j,k,il,ic
c                        write(*,*) Ixx+c2phi*Ixy,propaesplibre(1,1)
c                        write(*,*) -s2phi*Ixy,propaesplibre(1,2)
c                        write(*,*) -sphi*Izx,propaesplibre(1,3)
c                        write(*,*) Ixx-c2phi*Ixy,propaesplibre(2,2)
c                        write(*,*) -cphi*Izx,propaesplibre(2,3)
c                        write(*,*) -sphi*Ixz,propaesplibre(3,1)
c                        write(*,*) -cphi*Ixz,propaesplibre(3,2)
c                        write(*,*) Izz,propaesplibre(3,3)
                        Txx(k+nx*(j-1))=-(Ixx+c2phi*Ixy)
                        Txy(k+nx*(j-1))=-(-s2phi*Ixy)
                        Txz(k+nx*(j-1))=-(-sphi*Izx)
                        Tyy(k+nx*(j-1))=-(Ixx-c2phi*Ixy)
                        Tyz(k+nx*(j-1))=-(-cphi*Izx)
                        Tzx(k+nx*(j-1))=-(-sphi*Ixz)
                        Tzy(k+nx*(j-1))=-(-cphi*Ixz)
                        Tzz(k+nx*(j-1))=-(Izz)
                     endif
                     
                  enddo
               enddo
            endif

c     calcul de spola

c            kk=nx*ny*(ic-1)
c            do ix=1,nx
c               do iy=1,ny
c                  spola(ix,iy)=0.d0
c                  do jx=1,nx-ix+1
c                     do jy=1,ny-iy+1
c                        spola(ix,iy)=spola(ix,iy)+(polarisa(jx+nx *(jy-1
c     $                       )+kk,2,2))
c                     enddo
c                  enddo
c                  spola(ix,iy)=spola(ix,iy)/dnxy
c                  write(*,*) 'spola', spola(ix,iy),ix,iy,il,ic
c               enddo
c            enddo

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,k1,j1)
!$OMP& PRIVATE(dn11c,dn21c,dn12c,dn22c,dn1x,dn2x,dn1y,dn2y)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx    
                  dn1x=dble(k-1)
                  dn2x=dble(nx-k+1)
                  dn1y=dble(j-1) 
                  dn2y=dble(ny-j+1)
c                  dn11c=spola(1,1)+spola(k,j)-spola(1,j)-spola(k,1)
c                  dn12c=spola(1,j)-spola(k,j)
c                  dn21c=spola(k,1)-spola(k,j)
c                  dn22c=spola(k,j)
c                  write(*,*) 'dn11c',dn11c,j,k
c                  write(*,*) 'dn2x*dn2y',dn2x*dn2y/dnxy,spola(k,j)
c     $                 /polamoy
c                  write(*,*) 'dn1x*dn1y',dn1x*dn1y/dnxy,(spola(1,1)
c     $                 +spola(k,j)-spola(1,j)-spola(k,1))/polamoy
c                  write(*,*) 'dn1x*dn2y',dn1x*dn2y/dnxy,(spola(1,j)
c     $                 -spola(k,j))/polamoy
c                  write(*,*) 'dn2x*dn1y',dn2x*dn1y/dnxy,(spola(k,1)
c     $                 -spola(k,j))/polamoy
                  
                  k1=nx-k+2
                  
                  j1=ny-j+2
                  
                  if (k.eq.1) then
                     k1=1
                  endif
                  if (j.eq.1) then
                     j1=1
                  endif

c                  chanxx(k1+nx*(j1-1))=dn11c*Txx(k1+nx*(j1-1))+dn12c
c     $                 *Txx(k1+nx*(j-1))+dn21c*Txx(k+nx*(j1-1))+dn22c
c     $                 *Txx(k+nx*(j-1))              
c                  chanxy(k1+nx*(j1-1))=dn11c*Txy(k1+nx*(j1-1))-dn12c
c     $                 *Txy(k1+nx*(j-1))-dn21c*Txy(k+nx*(j1-1))+dn22c
c     $                 *Txy(k+nx*(j-1))
c                  chanxz(k1+nx*(j1-1))=dn11c*Txz(k1+nx*(j1-1))+dn12c
c     $                 *Txz(k1+nx*(j-1))-dn21c*Txz(k+nx*(j1-1))-dn22c
c     $                 *Txz(k+nx*(j-1))
c                  chanyy(k1+nx*(j1-1))=dn11c*Tyy(k1+nx*(j1-1))+dn12c
c     $                 *Tyy(k1+nx*(j-1))+dn21c*Tyy(k+nx*(j1-1))+dn22c
c     $                 *Tyy(k+nx*(j-1))
c                  chanyz(k1+nx*(j1-1))=dn11c*Tyz(k1+nx*(j1-1))-dn12c
c     $                 *Tyz(k1+nx*(j-1))+dn21c*Tyz(k+nx*(j1-1))-dn22c
c     $                 *Tyz(k+nx*(j-1))
c                  chanzx(k1+nx*(j1-1))=dn11c*Tzx(k1+nx*(j1-1))+dn12c
c     $                 *Tzx(k1+nx*(j-1))-dn21c*Tzx(k+nx*(j1-1))-dn22c
c     $                 *Tzx(k+nx*(j-1))
c                  chanzy(k1+nx*(j1-1))=dn11c*Tzy(k1+nx*(j1-1))-dn12c
c     $                 *Tzy(k1+nx*(j-1))+dn21c*Tzy(k+nx*(j1-1))-dn22c
c     $                 *Tzy(k+nx*(j-1))
c                  chanzz(k1+nx*(j1-1))=dn11c*Tzz(k1+nx*(j1-1))+dn12c
c     $                 *Tzz(k1+nx*(j-1))+dn21c*Tzz(k+nx*(j1-1))+dn22c
c     $                 *Tzz(k+nx*(j-1))

                  chanxx(k1+nx*(j1-1))=(dn1x*dn1y*Txx(k1+nx*(j1-1))
     $                 +dn1x*dn2y*Txx(k1+nx*(j-1))+dn2x*dn1y*Txx(k+nx
     $                 *(j1-1))+dn2x*dn2y*Txx(k+nx*(j-1)))/dnxy *polamoy
                  chanxy(k1+nx*(j1-1))=(dn1x*dn1y*Txy(k1+nx*(j1-1))
     $                 -dn1x*dn2y*Txy(k1+nx*(j-1))-dn2x*dn1y*Txy(k+nx
     $                 *(j1-1))+dn2x*dn2y*Txy(k+nx*(j-1)))/dnxy *polamoy
                  chanxz(k1+nx*(j1-1))=(dn1x*dn1y*Txz(k1+nx*(j1-1))
     $                 +dn1x*dn2y*Txz(k1+nx*(j-1))-dn2x*dn1y*Txz(k+nx
     $                 *(j1-1))-dn2x*dn2y*Txz(k+nx*(j-1)))/dnxy *polamoy
                  chanyy(k1+nx*(j1-1))=(dn1x*dn1y*Tyy(k1+nx*(j1-1))
     $                 +dn1x*dn2y*Tyy(k1+nx*(j-1))+dn2x*dn1y*Tyy(k+nx
     $                 *(j1-1))+dn2x*dn2y*Tyy(k+nx*(j-1)))/dnxy *polamoy
                  chanyz(k1+nx*(j1-1))=(dn1x*dn1y*Tyz(k1+nx*(j1-1))
     $                 -dn1x*dn2y*Tyz(k1+nx*(j-1))+dn2x*dn1y*Tyz(k+nx
     $                 *(j1-1))-dn2x*dn2y*Tyz(k+nx*(j-1)))/dnxy *polamoy
                  chanzx(k1+nx*(j1-1))=(dn1x*dn1y*Tzx(k1+nx*(j1-1))
     $                 +dn1x*dn2y*Tzx(k1+nx*(j-1))-dn2x*dn1y*Tzx(k+nx
     $                 *(j1-1))-dn2x*dn2y*Tzx(k+nx*(j-1)))/dnxy *polamoy
                  chanzy(k1+nx*(j1-1))=(dn1x*dn1y*Tzy(k1+nx*(j1-1))
     $                 -dn1x*dn2y*Tzy(k1+nx*(j-1))+dn2x*dn1y*Tzy(k+nx
     $                 *(j1-1))-dn2x*dn2y*Tzy(k+nx*(j-1)))/dnxy *polamoy
                  chanzz(k1+nx*(j1-1))=(dn1x*dn1y*Tzz(k1+nx*(j1-1))
     $                 +dn1x*dn2y*Tzz(k1+nx*(j-1))+dn2x*dn1y*Tzz(k+nx
     $                 *(j1-1))+dn2x*dn2y*Tzz(k+nx*(j-1)))/dnxy *polamoy
c                  write(*,*) 'chanxx',chanxx(k1+nx*(j1-1)),j,k
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL


            if (il.eq.ic) then
               chanxx(1)=chanxx(1)+1.d0
               chanyy(1)=chanyy(1)+1.d0
               chanzz(1)=chanzz(1)+1.d0
            endif

c            do k=1,nx
c               do j=1,ny
c                  write(*,*) 'chanxx',chanxx(k+nx*(j-1)),k,j
c                  write(*,*) 'chanxy',chanxy(k+nx*(j-1)),k,j
c                  write(*,*) 'chanxz',chanxz(k+nx*(j-1)),k,j
c                  write(*,*) 'chanyy',chanyy(k+nx*(j-1)),k,j
c                  write(*,*) 'chanyz',chanyz(k+nx*(j-1)),k,j
c                  write(*,*) 'chanzx',chanzx(k+nx*(j-1)),k,j
c                  write(*,*) 'chanzy',chanzy(k+nx*(j-1)),k,j
c                  write(*,*) 'chanzz',chanzz(k+nx*(j-1)),k,j
c               enddo
c            enddo
#ifdef USE_FFTW
            call dfftw_execute_dft(plan2b, chanxx, chanxx)   
            call dfftw_execute_dft(plan2b, chanxy, chanxy)   
            call dfftw_execute_dft(plan2b, chanxz, chanxz)   
            call dfftw_execute_dft(plan2b, chanyy, chanyy)   
            call dfftw_execute_dft(plan2b, chanyz, chanyz)   
            call dfftw_execute_dft(plan2b, chanzx, chanzx)   
            call dfftw_execute_dft(plan2b, chanzy, chanzy)   
            call dfftw_execute_dft(plan2b, chanzz, chanzz)   
#else 
            call fftsingletonz2d(chanxx,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanxy,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanxz,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanyy,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanyz,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanzx,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanzy,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanzz,nx,ny,FFTW_BACKWARD)
#endif


c            write(*,*) 'FFT',il,ic
c            do k=1,nx
c               do j=1,ny
c                  write(*,*) 'fftchanxx',chanxx(k+nx*(j-1)),k,j
c                  write(*,*) 'fftchanxy',chanxy(k+nx*(j-1)),k,j
c                  write(*,*) 'fftchanxz',chanxz(k+nx*(j-1)),k,j
c                  write(*,*) 'fftchanyy',chanyy(k+nx*(j-1)),k,j
c                  write(*,*) 'fftchanyz',chanyz(k+nx*(j-1)),k,j
c                  write(*,*) 'fftchanzx',chanzx(k+nx*(j-1)),k,j
c                  write(*,*) 'fftchanzy',chanzy(k+nx*(j-1)),k,j
c                  write(*,*) 'fftchanzz',chanzz(k+nx*(j-1)),k,j
c               enddo
c            enddo
            
            ix=3*(il-1)
            iy=3*(ic-1)
c     réécrit ce vecteur dans sa partie
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,ii,jj)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx
                  ii=k+nx*(j-1)
                  jj=j+ny*(k-1)	
c     jj=ii
                  Sdetnn(ix+1,iy+1,jj)=chanxx(ii)
                  Sdetnn(ix+1,iy+2,jj)=chanxy(ii)
                  Sdetnn(ix+1,iy+3,jj)=chanxz(ii)
                  Sdetnn(ix+2,iy+1,jj)=chanxy(ii)
                  Sdetnn(ix+2,iy+2,jj)=chanyy(ii)
                  Sdetnn(ix+2,iy+3,jj)=chanyz(ii)
                  Sdetnn(ix+3,iy+1,jj)=chanzx(ii)
                  Sdetnn(ix+3,iy+2,jj)=chanzy(ii)
                  Sdetnn(ix+3,iy+3,jj)=chanzz(ii)    
c                  write(*,*) 'sdet',jj,il,ic,'ii',ii
c                  write(*,*) Sdetnn(ix+1,iy+1,jj),Sdetnn(ix+1,iy+2,jj)
c     $                 ,Sdetnn(ix+1,iy+3,jj)
c                  write(*,*) Sdetnn(ix+2,iy+1,jj),Sdetnn(ix+2,iy+2,jj)
c     $                 ,Sdetnn(ix+2,iy+3,jj)
c                  write(*,*) Sdetnn(ix+3,iy+1,jj),Sdetnn(ix+3,iy+2,jj)
c     $                 ,Sdetnn(ix+3,iy+3,jj)
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL


c     fin boucle ic et il sur les z
         enddo
      enddo

      deallocate(Txx)
      deallocate(Txy)
      deallocate(Txz)
      deallocate(Tyy)
      deallocate(Tyz)
      deallocate(Tzx)
      deallocate(Tzy)
      deallocate(Tzz)
      deallocate(chanxx)
      deallocate(chanxy)
      deallocate(chanxz)
      deallocate(chanyy)
      deallocate(chanyz)
      deallocate(chanzx)
      deallocate(chanzy)
      deallocate(chanzz)
      deallocate(spola)
      
      k=1
      job=11
      lda=nz3
      lwork=4*nz3
      allocate(Sdettmp(nz3,nz3))
      allocate(worktmp(nz3))
      allocate(work(lwork))
      allocate(ipvttmp(nz3))

!     $OMP PARALLEL DEFAULT(SHARED)  PRIVATE(l,kk,kkk,ix,iy,i,j)
!     $OMP& PRIVATE(Sdettmp,ipvttmp,info,deterz,worktmp,work)
!     $OMP DO SCHEDULE(STATIC)           
      do l=1,nx*ny
         
         do kk=1,nz
            do kkk=1,nz
               ix=3*(kk-1)
               iy=3*(kkk-1)
               Sdettmp(ix+1,iy+1)=Sdetnn(ix+1,iy+1,l) 
               Sdettmp(ix+1,iy+2)=Sdetnn(ix+1,iy+2,l) 
               Sdettmp(ix+1,iy+3)=-Sdetnn(ix+1,iy+3,l) 
               Sdettmp(ix+2,iy+1)=Sdetnn(ix+2,iy+1,l) 
               Sdettmp(ix+2,iy+2)=Sdetnn(ix+2,iy+2,l) 
               Sdettmp(ix+2,iy+3)=-Sdetnn(ix+2,iy+3,l) 
               Sdettmp(ix+3,iy+1)=-Sdetnn(ix+3,iy+1,l) 
               Sdettmp(ix+3,iy+2)=-Sdetnn(ix+3,iy+2,l) 
               Sdettmp(ix+3,iy+3)=Sdetnn(ix+3,iy+3,l)
            enddo
         enddo     
         call ZGETRF( nz3, nz3, sdettmp, nz3, IPVttmp, INFO )
         if (INFO.ne.0) then
            write(*,*) 'probleme3 in zgefa.f',info,i
            stop
         endif
         do i=1,nz3
            do j=1,nz3
               Sdetnn(i,j,l)=Sdettmp(i,j)
            enddo
            IPVtnn(i,l)=IPVttmp(i)
         enddo
      enddo
!     $OMP ENDDO 
!     $OMP END PARALLEL
    
      deallocate(Sdettmp)
      deallocate(worktmp)
      deallocate(work)
      deallocate(ipvttmp)
       
      
      end
