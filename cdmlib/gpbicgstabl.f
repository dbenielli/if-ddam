c$$$      implicit none
c$$$      integer LDA,NDIM,NLAR,NOU,NSTAT,STEPERR,NLOOP,MAXIT,i,j,L,JJ
c$$$      parameter(lda=100,nlar=20)
c$$$      double precision NORM,TOL,TOLE
c$$$      double complex XI(lda),XR(lda),B(lda),WRK(lda,nlar),ALPHA,BETA
c$$$     $     ,SIGMA,RHO,mat(lda,lda),xsol(lda),icomp
c$$$      integer nr,np,ns,nq
c$$$      
c$$$      L=3
c$$$      ndim=100
c$$$      maxit=100
c$$$      icomp=(0.d0,1.d0)
c$$$      do i=1,ndim
c$$$         do j=1,ndim
c$$$            mat(i,j)=0.d0
c$$$         enddo
c$$$         mat(i,i)=(1.d0,0.d0)*dble(i)*icomp
c$$$         b(i)=(1.d0,0.d0)
c$$$         xi(i)=0.d0
c$$$      enddo
c$$$
c$$$      tol=1.d-6
c$$$      tole=1.d0
c$$$      nloop=0
c$$$      nou=0
c$$$
c$$$ 10   call GPBICGSTABL(XI,XR,B,Xsol,NORM,WRK,ALPHA,BETA,SIGMA,RHO ,L,LDA
c$$$     $     ,NDIM,NLAR,NOU,nr,np,ns,nq,JJ,NSTAT,STEPERR,NLOOP,MAXIT,TOLE
c$$$     $     ,TOL)
c$$$
c$$$      write(*,*) nloop,nou,ALPHA,BETA,SIGMA,RHO,NSTAT
c$$$
c$$$      write(*,*) 'TOL',TOLE,tol,nstat
c$$$      if (nstat.lt.0) then
c$$$         write(*,*) 'stop nstat',nstat,STEPERR
c$$$         stop
c$$$      endif
c$$$      
c$$$      do i=1,ndim
c$$$         xr(i)=0.d0
c$$$         do j=1,ndim
c$$$            xr(i)=xr(i)+mat(i,j)*xi(j)
c$$$         enddo
c$$$         write(*,*) 'xr xi',xr(i),xi(i)
c$$$      enddo
c$$$      if (nstat.ne.1) goto 10
c$$$
c$$$
c$$$      write(*,*) '********************',NLOOP
c$$$      DO I=1,NDIM
c$$$         WRITE(*,*) 'SOL',Xsol(I)
c$$$      ENDDO
c$$$
c$$$c     calcul erreur relative
c$$$      tole=0.d0
c$$$      do i=1,ndim
c$$$         tole=tole+cdabs(xr(i)-b(i))
c$$$      enddo
c$$$      tole=tole/norm
c$$$      write(*,*) 'tole',tole
c$$$
c$$$      end
c****************************************
      subroutine GPBICGSTABL(XI,XR,B,Xsol,NORM,WRK,ALPHA,BETA,SIGMA,RHO
     $     ,L,LDA,NDIM,NLAR,NOU,nr,np,ns,nq,J,NSTAT,STEPERR,ITNO,MAXIT
     $     ,TOLE,TOL)
      IMPLICIT NONE

      INTEGER I,J,ii,MAXIT,L,LMAX,nr,np,ns,nq
      double precision RESIDU
C     constant
      PARAMETER (LMAX=10)

C     array
      INTEGER LDA,NDIM,NLAR,NOU,NSTAT,STEPERR,ITNO
      DOUBLE COMPLEX B(LDA),WRK(LDA,NLAR),XR(LDA),XI(LDA),XSOL(LDA)

c     Local Scalars
      DOUBLE PRECISION TOL,NORM,TOLE
      DOUBLE COMPLEX ALPHA,BETA,SIGMA,RHO

      integer LDALMAX
      parameter (LDALMAX=LMAX+1)
      double complex mat(LDALMAX,LDALMAX),vect(LDALMAX),zeta(LMAX)
     $     ,deterz(2) ,work(LDALMAX),icomp,ctmp,eta
      integer ipvt(LDAlMAX),job,info
      
      
c     en entre nou=0,nloop=0
      NSTAT = 0
      STEPERR = -1

      IF (NOU.EQ.1) GOTO 10
      IF (NOU.EQ.2) GOTO 20
      IF (NOU.EQ.3) GOTO 30
      IF (NOU.EQ.4) GOTO 40
      IF (NOU.EQ.5) GOTO 50
      
c     r0tilde =1
c     y	      =2
c     z	      =3
c     u	      =4
c     r'      =5
c     p'      =6
c     v       =7
c     r_i,i=0,L,=nr->nr+L  nr=8
c     p_i,i=0,L,=np->np+L  np=nr+L+1
c     q_i,i=0,L-1,=nq->nq+L-1 nq=np+L+1
c     s_i,i=0,L-2,=ns->ns+L-2 ns=nq+L

      if (nlar.lt.4*L+8) then
         NSTAT  = -1
         STEPERR = 10
         return
      endif

c     initialise et calcul norme
      NOU=1  
      ctmp=0.D0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  REDUCTION(+:ctmp)        
      DO ii=1,NDIM
         Xsol(ii)=xi(ii)    
         ctmp=ctmp+B(ii)*DCONJG(B(ii))
      ENDDO
!$OMP ENDDO 
!$OMP END PARALLEL       
      NORM=dsqrt(cdabs(ctmp))
      write(*,*) 'NORM',NORM
      RETURN 
c     r0=(b-Ax) CALCUL DU RESIDU
 10   nr=8
      np=nr+L+1
      nq=np+L+1
      ns=nq+L
      RHO=0.d0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  REDUCTION(+:RHO)       
      do ii=1,NDIM
         WRK(ii,1)=B(ii)-XR(ii)   ! r0 tilde
         WRK(ii,nr)=WRK(ii,1)   ! r0
         WRK(ii,np)=WRK(ii,1)   ! p0
         WRK(ii,3)=0.d0         !z=0
         RHO=RHO+dconjg(WRK(ii,1))*WRK(ii,nr)
      enddo 
!$OMP ENDDO 
!$OMP END PARALLEL  
c     **********************************************************
c     boucle d'initialisation 1,...,L
      J=0
 200  J=J+1

c     x_i=p_{j-1}
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  
      do ii=1,ndim
         xi(ii)=WRK(ii,np+J-1)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL       
      NOU=2
      RETURN

 20   sigma=(0.d0,0.d0)
c     p_j=Ap_{j-1} sigma=r_0^* p_j
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:sigma)       
      do ii=1,ndim
         WRK(ii,np+J)=xr(ii)
         sigma=sigma+dconjg(WRk(ii,1))*xr(ii)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
      if (cdabs(sigma).eq.0.d0) then
         NSTAT=-1
         STEPERR=1
         return
      endif
c     alpha=rho/sigma
      alpha=rho/sigma
c     x=x+alpha p_0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
      do ii=1,ndim
         xsol(ii)=xsol(ii)+alpha*WRK(ii,np)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
c     r_i=r_i-alpha p_{i+1}; i=0,J-1
      do i=0,J-1
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)         
         do ii=1,ndim
            WRK(ii,nr+i)=WRK(ii,nr+i)-alpha*WRK(ii,np+i+1)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL        
      enddo
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)      
      do ii=1,ndim
        xi(ii)=WRK(ii,nr+J-1)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      nou=3
      return

 30   rho=0.d0

c     calcul r_j=A r_{j-1} et rho=r_0^* r_j
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:rho)      
      do ii=1,ndim
         WRK(ii,nr+J)=xr(ii)
         rho=rho+dconjg(WRK(ii,1))*xr(ii)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL     
c     beta=rho/sigma
      beta=rho/sigma
c     p_i=r_i-beta p_i; i=0,J
      do i=0,J
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)         
         do ii=1,ndim
            WRK(ii,np+i)=WRK(ii,nr+i)-beta*WRK(ii,np+i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL          
      enddo

      if (J.le.L-1) goto 200

c     **********************************************************
c     initialisation pour la grande boucle
c     r'=r_0, p'=P_0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)       
      do ii=1,ndim
         WRK(ii,5)=WRK(ii,nr)
         WRK(ii,6)=WRK(ii,np)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL       
c     s_i=r_{i+1}; i=0,L-2
      do i=0,L-2
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)         
         do ii=1,ndim
            WRK(ii,ns+i)=WRK(ii,nr+i+1)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL          
      enddo
c     q_i=p_{i+1}; i=0,L-1
      do i=0,L-1
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)         
         do ii=1,ndim
            WRK(ii,nq+i)=WRK(ii,np+i+1)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
      enddo      
c     Calcul de la matrice: M^TM et second Membre M^T r0 avec M=[r_1,...,r_L]
      mat=0.d0
      zeta=0.d0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii,i,j)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2) REDUCTION(+:mat)      
      do ii=1,ndim
         do j=1,L
            do i=1,j
               mat(i,j)=mat(i,j)+dconjg(WRK(ii,nr+i))*WRK(ii,nr+j)
            enddo
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      do j=1,L-1
         do i=j+1,L
            mat(i,j)=dconjg(mat(j,i))
         enddo
      enddo

      
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii,i)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2) REDUCTION(+:zeta) 
      do ii=1,ndim
         do i=1,L
            zeta(i)=zeta(i)+dconjg(WRK(ii,nr+i))*WRK(ii,nr)
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL        
c     résoud le système: solution dans bc
      call ZGETRF(L,L, mat,LDALMAX, IPVt, INFO )
      if (INFO.ne.0) then
         write(*,*) 'problem in zgetrf precondionner',info
         stop
      endif
      ii=1
      call  zgetrs( 'N',L, ii , mat,LDALMAX ,IPVt,zeta,LDALMAX,INFO)

c     initialise z=0+zeta_i r_{i-1}
      do i=1,L
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
         do ii=1,ndim
            WRK(ii,3)=WRK(ii,3)+zeta(i)*WRK(ii,nr+i-1)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      enddo
      
c     calcul x=x+z
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)     
      do ii=1,ndim
         xsol(ii)=xsol(ii)+WRK(ii,3)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL          
c     initialise r_0=r_0-zera_i r_i et p_0=p_0-zera_i p_i
      do i=1,L
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
         do ii=1,ndim       
            WRK(ii,nr)=WRK(ii,nr)-zeta(i)*WRK(ii,nr+i)
            WRK(ii,np)=WRK(ii,np)-zeta(i)*WRK(ii,np+i)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL   
         enddo

c     *****************************************************
c     commence la grande boucle
c     *****************************************************      
      itno=-1
 100  itno=itno+1

      rho=0.d0
c     y=r'-r_0 et u=p'-p_0 rho=r_O* *r_0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:rho)
      do ii=1,ndim
         WRK(ii,2)=WRK(ii,5)-WRK(ii,nr)
         WRK(ii,4)=WRK(ii,6)-WRK(ii,np)
         rho=rho+dconjg(WRK(ii,1))*WRK(ii,nr)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL       
      J=0
 300  J=J+1
      if (J.gt.1) then
c     s_i=s_i-alpha q_{i+1}
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(i,ii)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2)   
         do i=0,L-j         
            do ii=1,ndim
               WRK(ii,ns+i)=WRK(ii,ns+i)-alpha*WRK(ii,nq+i+1)
            enddo
         enddo            
!$OMP ENDDO 
!$OMP END PARALLEL             

c     q_i=s_i-beta q_i
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(i,ii)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2)    
         do i=0,L-j            
            do ii=1,ndim
               WRK(ii,nq+i)=WRK(ii,ns+i)-beta*WRK(ii,nq+i)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL             
      endif
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)       
      do ii=1,ndim
         xi(ii)=WRK(ii,np+j-1)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL      
      nou=4
      return
      
 40   sigma=0.d0
c     p_j=A p_{j-1} v=q_0-p_1 et sigma = r_0^* p_j
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:sigma)      
      do ii=1,ndim
         WRK(ii,np+j)=xr(ii)
         WRK(ii,7)=WRK(ii,nq)-WRK(ii,np+1)
         sigma=sigma+dconjg(WRK(ii,1))*xr(ii)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL      
      if (cdabs(sigma).eq.0.d0) then
         NSTAT=-1
         STEPERR=4
         return
      endif
c     alpha=rho/sigma
      alpha=rho/sigma
c     x=x+alpha p_0; z=z-alpha u; y= y -alpha v
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)       
      do ii=1,ndim
         xsol(ii)=xsol(ii)+alpha*WRK(ii,np)
         WRK(ii,3)=WRK(ii,3)-alpha*WRK(ii,4)
         WRK(ii,2)=WRK(ii,2)-alpha*WRK(ii,7)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL               
c     r_i=r_i-alpha p_{i+1}; i=0, J-1
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(i,ii)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2)
      do i=0,j-1             
         do ii=1,ndim
            WRK(ii,nr+i)=WRK(ii,nr+i)-alpha*WRK(ii,np+i+1)
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL           

!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)          
      do ii=1,ndim
         xi(ii)=WRK(ii,nr+j-1)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL      
      nou=5
      return
      
 50   rho=0.D0
c     r_j=A r_{j-1} rho= r_0^* r_j
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  REDUCTION(+:rho)    
      do ii=1,ndim
         WRK(ii,nr+j)=xr(ii)
         rho=rho+dconjg(WRK(ii,1))*xr(ii)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL      
c     beta=rho/sigma
      beta=rho/sigma
c     p_i=r_i-beta p_{i}; i=0,J
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii,i)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2)
      do i=0,j        
         do ii=1,ndim
            WRK(ii,np+i)=WRK(ii,nr+i)-beta*WRK(ii,np+i)
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL          

c     u=y-beta*u
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)      
      do ii=1,ndim
         WRK(ii,4)=WRK(ii,2)-beta*WRK(ii,4)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
         
      if (J.le.L-1) goto 300
      
c     r'=r_0 et p'=p_0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)      
      do ii=1,ndim
         WRK(ii,5)=WRK(ii,nr)
         WRK(ii,6)=WRK(ii,np)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
c     s_i=r_{i+1}; i=0,L-2
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(i,ii)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2)
      do i=0,L-2        
         do ii=1,ndim
            WRK(ii,ns+i)=WRK(ii,nr+i+1)
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL        

c     q_i=p_{i+1}; i=0,L-1
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(i,ii)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2)
      do i=0,L-1        
         do ii=1,ndim
            WRK(ii,nq+i)=WRK(ii,np+i+1)
         enddo
      enddo         
!$OMP ENDDO 
!$OMP END PARALLEL  

c     Calcul de la matrice: M^TM et second Membre M^T r0 avec M=[r_1,...,r_L]
      mat=0.d0
      vect=0.d0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii,i,j)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2) REDUCTION(+:mat)           
      do ii=1,ndim  
         do j=1,L
            do i=1,j
               mat(i,j)=mat(i,j)+dconjg(WRK(ii,nr+i))*WRK(ii,nr+j)
            enddo
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

 
      
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii,i)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2) REDUCTION(+:vect,mat)           
      do ii=1,ndim
         do i=1,L 
            vect(i)=vect(i)+dconjg(WRK(ii,nr+i))*WRK(ii,nr)
            mat(i,L+1)=mat(i,L+1)+dconjg(WRK(ii,nr+i))*WRK(ii,2)
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      do j=1,L
         do i=j+1,L+1
            mat(i,j)=dconjg(mat(j,i))
         enddo
      enddo

      
c     calcul la derniere ligne de la matrice 
c     calcul le dernier element de la matrice
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:mat,vect)       
      do ii=1,ndim
         mat(L+1,L+1)=mat(L+1,L+1)+dconjg(WRK(ii,2))*WRK(ii,2)
         vect(L+1)=vect(L+1)+dconjg(WRK(ii,2))*WRK(ii,nr)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
c     résoud le système: solution dans bc

      call ZGETRF(L+1,L+1, mat,LDALMAX, IPVt, INFO )
      if (INFO.ne.0) then
         write(*,*) 'problem in zgetrf precondionner',info
         stop
      endif
      ii=1
      call  zgetrs( 'N',L+1, ii , mat,LDALMAX ,IPVt,vect,LDALMAX,INFO)
c     Trouve le vecteur solution zeta,eta
      do i=1,L
         zeta(i)=vect(i)
      enddo
      eta=vect(L+1)
      if (L.eq.8) then
         ctmp=0.d0
c     calcul z=eta*z
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  REDUCTION(+:ctmp)     
         do ii=1,ndim
            WRK(ii,3)=eta*WRK(ii,3)+zeta(1)*WRK(ii,nr)+zeta(2)*WRK(ii
     $           ,nr+1)+zeta(3)*WRK(ii,nr+2)+zeta(4)*WRK(ii,nr+3)
     $           +zeta(5) *WRK(ii,nr+4)+zeta(6)*WRK(ii,nr+5)+zeta(7)
     $           *WRK(ii,nr+6)+zeta(8)*WRK(ii,nr+7)
            xsol(ii)=xsol(ii)+WRK(ii,3)
            WRK(ii,nr)=WRK(ii,nr)-eta*WRK(ii,2)-zeta(1)*WRK(ii,nr+1)
     $           -zeta(2)*WRK(ii,nr+2)-zeta(3)*WRK(ii,nr+3)-zeta(4)
     $           *WRK(ii,nr+4)-zeta(5)*WRK(ii,nr+5)-zeta(6)*WRK(ii,nr+6)
     $           -zeta(7)*WRK(ii,nr+7)-zeta(8)*WRK(ii,nr+8)
            WRK(ii,np)=WRK(ii,np)-eta*WRK(ii,4)-zeta(1)*WRK(ii,np+1)
     $           -zeta(2)*WRK(ii,np+2)-zeta(3)*WRK(ii,np+3)-zeta(4)
     $           *WRK(ii,np+4)-zeta(5)*WRK(ii,np+5)-zeta(6)*WRK(ii,np+6)
     $           -zeta(7)*WRK(ii,np+7)-zeta(8)*WRK(ii,np+8)
            ctmp=ctmp+dconjg(WRK(ii,nr))*WRK(ii,nr)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL        

c     r_0=r_0-zeta_i * r_i et p_0=p_0-zeta_i * p_i; i=1,L 
c     calcul residu
      else
c     calcul z=eta*z
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii,i)
!$OMP DO  SCHEDULE(STATIC)         
         do ii=1,ndim
            WRK(ii,3)=eta*WRK(ii,3)
            do i=1,L
               WRK(ii,3)=WRK(ii,3)+zeta(i)*WRK(ii,nr+i-1)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL        
c     z=z+zeta_i r_{i-1}

c     x=x+z
c     r_0=r_0-eta*y et p_0=p_0-eta*u
         ctmp=0.d0
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii,i)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ctmp)
         do ii=1,ndim
            xsol(ii)=xsol(ii)+WRK(ii,3)
            WRK(ii,nr)=WRK(ii,nr)-eta*WRK(ii,2)
            WRK(ii,np)=WRK(ii,np)-eta*WRK(ii,4)
            do i=1,L
               WRK(ii,nr)=WRK(ii,nr)-zeta(i)*WRK(ii,nr+i)
               WRK(ii,np)=WRK(ii,np)-zeta(i)*WRK(ii,np+i)
            enddo
            ctmp=ctmp+dconjg(WRK(ii,nr))*WRK(ii,nr)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
c     r_0=r_0-zeta_i * r_i et p_0=p_0-zeta_i * p_i; i=1,L 
c     calcul residu
      endif
      
      RESIDU=dsqrt(dreal(ctmp))/NORM
      write(*,*) 'RESIDU',RESIDU,tol,NORM,ITNO,MAXIT
c     critere convergence
      if (RESIDU.le.TOL) then
         NSTAT=1       
         nou=6
         return
      endif

      if (ITNO.le.MAXIT) goto 100

      NSTAT=-1
      STEPERR=7
      return

      END
