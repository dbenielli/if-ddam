c     routine qui calcul le champ incident non linéaire à travers les
c     lentilles.
      subroutine incidentnolinearfarfield(nbsphere,nx,ny,nz,nxm,nym,nzm
     $     ,nquickdiffracte,xs,ys,zs,aretecube,FF0,Ediffkzpos
     $     ,Ediffkzneg,Efourierincxpos ,Efourierincypos ,Efourierinczpos
     $     ,Efourierincxneg ,Efourierincyneg ,Efourierinczneg
     $     ,Efourierxpos ,Efourierypos,Efourierzpos,Efourierxneg
     $     ,Efourieryneg ,Efourierzneg,dcouche ,zcouche,epscouche
     $     ,nepsmax,neps ,numaperref ,numapertra,tabfft2,k0 ,deltakx
     $     ,deltaky ,indice0,indicen,deltax,imaxk0,nfft2dtmp ,nfft2d
     $     ,ncote ,plan2f,nstop,infostr)
      implicit none
      integer nbsphere,nx,ny,nz,nxm,nym,nzm ,nfft2dtmp,nfft2d,imaxk0
     $     ,nepsmax ,neps,ncote,nquickdiffracte ,nstop
      double precision k0,aretecube,deltakx ,deltaky,rloin,tmp
     $     ,dcouche(nepsmax),zcouche(0:nepsmax),numaperref ,numapertra
     $     ,deltax,indice0,indicen
      DOUBLE PRECISION,DIMENSION(nxm*nym*nzm)::xs,ys,zs
      integer tabfft2(nfft2d)
      double complex Efourierincxpos(nfft2d*nfft2d)
     $     ,Efourierincypos(nfft2d*nfft2d),Efourierinczpos(nfft2d
     $     *nfft2d),Efourierincxneg(nfft2d*nfft2d)
     $     ,Efourierincyneg(nfft2d*nfft2d),Efourierinczneg(nfft2d
     $     *nfft2d),Efourierxpos(nfft2d*nfft2d),Efourierypos(nfft2d
     $     *nfft2d),Efourierzpos(nfft2d*nfft2d),Efourierxneg(nfft2d
     $     *nfft2d),Efourieryneg(nfft2d*nfft2d),Efourierzneg(nfft2d
     $     *nfft2d),FF0(3*nxm*nym*nzm) ,Ediffkzpos(nfft2d,nfft2d ,3)
     $     ,Ediffkzneg(nfft2d,nfft2d,3),epscouche(0:nepsmax+1)
      character(64) infostr
      integer*8 plan2f

      integer i,j,ii,jj,k,kk,indicex,indicey,nfft2d2,indice
      double precision kx,ky,kz,z,vol,pi
      double complex ctmp,Emx,Emy,Emz,icomp,stenseur(3,3)

      icomp=(0.d0,1.d0)
      pi=dacos(-1.d0)
      
      write(*,*) 'deltak',deltakx,deltaky,ncote
      write(*,*) 'FF0energie',FF0(1:20)
      write(*,*) 'ff',nbsphere,nx,ny,nz,nxm,nym,nzm ,nfft2dtmp,nfft2d,k0
     $     ,aretecube,nquickdiffracte
      nfft2d2=nfft2d/2

c     sauvegarde champ diffracte par la structure

      if (ncote.eq.0.or.ncote.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,ii,jj,kx,ky,kz,indice,ctmp)   
!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)          
         do i=-imaxk0,imaxk0
            do j=-imaxk0,imaxk0
               ii=imaxk0+i+1
               jj=imaxk0+j+1
               indice=i+nfft2d2+1+nfft2d*(j+nfft2d2)
               Efourierxpos(indice)=Ediffkzpos(ii,jj,1)
               Efourierypos(indice)=Ediffkzpos(ii,jj,2)
               Efourierzpos(indice)=Ediffkzpos(ii,jj,3)
c               write(*,*) 'rrr1',Ediffkzpos(ii,jj,1),Ediffkzpos(ii,jj,2)
c     $              ,Ediffkzpos(ii,jj,3),i,j
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
      endif

      if (ncote.eq.0.or.ncote.eq.-1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,ii,jj,kx,ky,kz,indice,ctmp)   
!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)       
         do i=-imaxk0,imaxk0
            do j=-imaxk0,imaxk0
               ii=imaxk0+i+1
               jj=imaxk0+j+1             
               indice=i+nfft2d2+1+nfft2d*(j+nfft2d2)
               Efourierxneg(indice)=Ediffkzneg(ii,jj,1)
               Efourieryneg(indice)=Ediffkzneg(ii,jj,2)
               Efourierzneg(indice)=Ediffkzneg(ii,jj,3)
c               write(*,*) 'rrr2',Ediffkzneg(ii,jj,1),Ediffkzneg(ii,jj,2)
c     $              ,Ediffkzneg(ii,jj,3),i,j
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL           
      endif

c     calcul champ diffracté par la polarisation induite
      if (nquickdiffracte.eq.1) then
         tmp=1.d0
         rloin=1.d0
         call diffractefft2dsurf2(nbsphere,nx,ny,nz,nxm,nym,nzm
     $        ,nfft2dtmp,nfft2d,tabfft2,k0,xs,ys,zs,aretecube
     $        ,Efourierincxpos ,Efourierincypos,Efourierinczpos,FF0
     $        ,imaxk0,deltakx ,deltaky ,Ediffkzpos,Ediffkzneg,rloin
     $        ,rloin,tmp ,tmp,nepsmax ,neps ,dcouche ,zcouche,epscouche
     $        ,ncote ,nstop ,infostr,plan2f)
c         do i=-imaxk0,imaxk0
c            do j=-imaxk0,imaxk0
c               ii=imaxk0+i+1
c               jj=imaxk0+j+1
c                kx=dble(i)*deltakx
c                  ky=dble(j)*deltaky
c                  if (indicen*indicen*k0*k0
c     $                 *0.9999d0-kx*kx-ky*ky.gt.0.d0) then
c                     write(*,*) 'diff2a',Ediffkzpos(ii,jj,:),i,j
c                  endif
c            enddo
c         enddo

      else
         write(*,*) 'deltakx lent',deltakx

c     initialization table
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j)   
!$OMP DO SCHEDULE(STATIC)  COLLAPSE(2)
         do i=1,nfft2d
            do j=1,nfft2d
               Ediffkzpos(i,j,1)=0.d0
               Ediffkzpos(i,j,2)=0.d0
               Ediffkzpos(i,j,3)=0.d0
               Ediffkzneg(i,j,1)=0.d0
               Ediffkzneg(i,j,2)=0.d0
               Ediffkzneg(i,j,3)=0.d0
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

c     compute the diffracted field
         do i=-imaxk0,imaxk0               
            if (i.ge.0) then
               indicex=i+1
            else
               indicex=nfft2d+i+1
            endif
            kx=deltakx*dble(i)
            do j=-imaxk0,imaxk0
               
               if (j.ge.0) then
                  indicey=j+1
               else
                  indicey=nfft2d+j+1
               endif
               ky=deltaky*dble(j)
               ii=imaxk0+i+1
               jj=imaxk0+j+1

               if (ncote.eq.1.or.ncote.eq.0) then
c     calcul champ dessus
                  if (k0*k0*indicen*indicen *0.9999d0-kx*kx-ky
     $                 *ky.gt.0.d0) then  
                     z=1.d0
                     kz=dsqrt(k0*k0*indicen*indicen-kx*kx-ky*ky)
                     call  tenseurmulticoucheloinfft(kx,ky,kz,z,zs(1)
     $                    ,k0,nepsmax,neps,dcouche,zcouche ,epscouche
     $                    ,Stenseur)
c                     write(*,*) 'ij nl',i,j
                     ctmp=cdexp(-icomp*(kx*xs(1)+ky*ys(1)))
                     Emx=(Stenseur(1,1)*FF0(1)+Stenseur(1,2)*FF0(2)
     $                    +Stenseur(1,3)*FF0(3))*ctmp
                     Emy=(Stenseur(2,1)*FF0(1)+Stenseur(2,2)*FF0(2)
     $                    +Stenseur(2,3)*FF0(3))*ctmp
                     Emz=(Stenseur(3,1)*FF0(1)+Stenseur(3,2)*FF0(2)
     $                    +Stenseur(3,3)*FF0(3))*ctmp
c                     write(*,*) 'Enl',FF0(1),FF0(2),FF0(3),Emx,Emy
c     $                    ,Emz

c!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(k,kk,Stenseur,ctmp)   
c!$OMP DO SCHEDULE(STATIC) REDUCTION(+:Emx,Emy,Emz)      
                     do k=2,nbsphere
                        kk=3*(k-1)
                        if (zs(k).ne.zs(k-1)) then
                           call  tenseurmulticoucheloinfft(kx,ky,kz,z
     $                          ,zs(k),k0,nepsmax,neps,dcouche,zcouche
     $                          ,epscouche,Stenseur)                         
                        endif
                        ctmp=cdexp(-icomp*(kx*xs(k)+ky*ys(k)))
c                        write(*,*) 'tout',k,kx,ky,kz,z,xs(k),ys(k),zs(k)
c     $                       ,k0,Stenseur,ctmp

                        Emx=Emx+(Stenseur(1,1)*FF0(kk+1)+Stenseur(1,2)
     $                       *FF0(kk+2)+Stenseur(1,3)*FF0(kk+3))*ctmp
                        Emy=Emy+(Stenseur(2,1)*FF0(kk+1)+Stenseur(2,2)
     $                       *FF0(kk+2)+Stenseur(2,3)*FF0(kk+3))*ctmp
                        Emz=Emz+(Stenseur(3,1)*FF0(kk+1)+Stenseur(3,2)
     $                       *FF0(kk+2)+Stenseur(3,3)*FF0(kk+3))*ctmp
c                        write(*,*) 'Enlt',FF0(kk+1),FF0(kk+2),FF0(kk
c     $                       +3),Emx,Emy,Emz
c                        write(*,*) 'EEnl',(Stenseur(1,1)*FF0(kk+1)
c     $                       +Stenseur(1,2)*FF0(kk+2)+Stenseur(1,3)
c     $                       *FF0(kk+3))*ctmp
                     enddo        
c!$OMP ENDDO 
c!$OMP END PARALLEL  
c                     write(*,*) 'Enl',Emx,Emy,Emz
                     Ediffkzpos(ii,jj,1)=Emx
                     Ediffkzpos(ii,jj,2)=Emy
                     Ediffkzpos(ii,jj,3)=Emz
                  endif
               endif

c     calcul champ dessous
               if (ncote.eq.-1.or.ncote.eq.0) then
                  if (k0*k0*indice0*indice0 *0.9999d0-kx*kx-ky
     $                 *ky.gt.0.d0) then    
                     z=-1.d0
                     kz=-dsqrt(k0*k0*indice0*indice0-kx*kx-ky*ky)
                     call  tenseurmulticoucheloinfft(kx,ky,kz,z,zs(1)
     $                    ,k0,nepsmax,neps,dcouche,zcouche ,epscouche
     $                    ,Stenseur)
                     ctmp=cdexp(-icomp*(kx*xs(1)+ky*ys(1)))
                     Emx=(Stenseur(1,1)*FF0(1)+Stenseur(1,2)*FF0(2)
     $                    +Stenseur(1,3)*FF0(3))*ctmp
                     Emy=(Stenseur(2,1)*FF0(1)+Stenseur(2,2)*FF0(2)
     $                    +Stenseur(2,3)*FF0(3))*ctmp
                     Emz=(Stenseur(3,1)*FF0(1)+Stenseur(3,2)*FF0(2)
     $                    +Stenseur(3,3)*FF0(3))*ctmp
c!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(k,kk,ctmp,Stenseur)   
c!$OMP DO SCHEDULE(STATIC) REDUCTION(+:Emx,Emy,Emz)                         
                     do k=2,nbsphere
                        kk=3*(k-1)
                        if (zs(k).ne.zs(k-1)) then
                           call  tenseurmulticoucheloinfft(kx,ky,kz,z
     $                          ,zs(k),k0,nepsmax,neps,dcouche,zcouche
     $                          ,epscouche,Stenseur)
                        endif
                        ctmp=cdexp(-icomp*(kx*xs(k)+ky*ys(k)))
                        Emx=Emx+(Stenseur(1,1)*FF0(kk+1)+Stenseur(1,2)
     $                       *FF0(kk+2)+Stenseur(1,3)*FF0(kk+3))*ctmp
                        Emy=Emy+(Stenseur(2,1)*FF0(kk+1)+Stenseur(2,2)
     $                       *FF0(kk+2)+Stenseur(2,3)*FF0(kk+3))*ctmp
                        Emz=Emz+(Stenseur(3,1)*FF0(kk+1)+Stenseur(3,2)
     $                       *FF0(kk+2)+Stenseur(3,3)*FF0(kk+3))*ctmp
                     enddo
c!$OMP ENDDO 
c!$OMP END PARALLEL            
                     Ediffkzneg(ii,jj,1)=Emx
                     Ediffkzneg(ii,jj,2)=Emy
                     Ediffkzneg(ii,jj,3)=Emz
                  endif
               endif

            enddo
         enddo


      endif
    
c      write(*,*) 'diff4'

      write(*,*) indicen,k0,deltakx,deltaky,nfft2d,nfft2d2
c      do i=-imaxk0,imaxk0
c            do j=-imaxk0,imaxk0             
c               indice=i+nfft2d2+1+nfft2d*(j+nfft2d2)
c               kx=dble(i)*deltakx
c               ky=dble(j)*deltaky
c               if (indicen*indicen*k0*k0 *0.9999d0-kx*kx-ky*ky.gt.0.d0)
c     $              then
c                  write(*,*) 'diff3',Efourierincypos(indice),i,j
c               endif
c            enddo
c         enddo

c     additionne l'incident avec le diffracté pour avoir le total+ passe
c     dans Fourier.

c     calul du champ total dans le domaine de Fourier.
      if (ncote.eq.0.or.ncote.eq.1) then
         write(*,*) 'imaxk0',imaxk0,deltakx,indicen,k0,numapertra
     $        ,deltakx,deltaky
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,ii,jj,kx,ky,indice)   
!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)          
         do i=-imaxk0,imaxk0
            do j=-imaxk0,imaxk0
               ii=imaxk0+i+1
               jj=imaxk0+j+1
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky
               if (indicen*indicen*k0*k0*0.9999d0 -kx*kx-ky*ky.gt.0.d0)
     $              then
                  indice=i+nfft2d2+1+nfft2d*(j+nfft2d2)
                  kz=dsqrt(indicen*indicen*k0*k0-kx*kx-ky*ky)
                  ctmp=-2.d0*pi*icomp*kz
                  Efourierincxpos(indice)=(Efourierincxpos(indice)+
     $                 Efourierxpos(indice))/ctmp
                  Efourierincypos(indice)=(Efourierincypos(indice)+
     $                 Efourierypos(indice))/ctmp
                  Efourierinczpos(indice)=(Efourierinczpos(indice)+
     $                 Efourierzpos(indice))/ctmp
c                   write(*,*) 'diff4',Efourierincypos(indice)
               endif
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL   
      endif

   
      write(*,*) 'ima',imaxk0,indice0,ncote,k0
      
      if (ncote.eq.0.or.ncote.eq.-1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,ii,jj,kx,ky,indice)   
!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)       
         do i=-imaxk0,imaxk0
            do j=-imaxk0,imaxk0
               ii=imaxk0+i+1
               jj=imaxk0+j+1
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky
               if (indice0*indice0*k0*k0*0.9999d0 -kx*kx-ky*ky.gt.0.d0)
     $              then
                  indice=i+nfft2d2+1+nfft2d*(j+nfft2d2)
                  kz=dsqrt(indice0*indice0*k0*k0-kx*kx-ky*ky)
                  ctmp=-2.d0*pi*icomp*kz
c                  write(*,*) 'diff1',Efourierincxneg(indice),
c     $                 Efourierxneg(indice)
c                  write(*,*) 'diff2',Efourierincyneg(indice),
c     $                 Efourieryneg(indice)
                  Efourierincxneg(indice)=(Efourierincxneg(indice)+
     $                 Efourierxneg(indice))/ctmp
                  Efourierincyneg(indice)=(Efourierincyneg(indice)+
     $                 Efourieryneg(indice))/ctmp
                  Efourierinczneg(indice)=(Efourierinczneg(indice)+
     $                 Efourierzneg(indice))/ctmp                
               endif
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL           
      endif

c     recopie dans Ediffkz

      if (ncote.eq.0.or.ncote.eq.1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,ii,jj,kx,ky,kz,indice,ctmp)   
!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)          
         do i=-imaxk0,imaxk0
            do j=-imaxk0,imaxk0
               ii=imaxk0+i+1
               jj=imaxk0+j+1
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky
               indice=i+nfft2d2+1+nfft2d*(j+nfft2d2)
               Ediffkzpos(ii,jj,1)=Efourierxpos(indice)
               Ediffkzpos(ii,jj,2)=Efourierypos(indice)
               Ediffkzpos(ii,jj,3)=Efourierzpos(indice)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
      endif


      if (ncote.eq.0.or.ncote.eq.-1) then
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,ii,jj,kx,ky,kz,indice,ctmp)   
!$OMP DO SCHEDULE(DYNAMIC) COLLAPSE(2)       
         do i=-imaxk0,imaxk0
            do j=-imaxk0,imaxk0
               ii=imaxk0+i+1
               jj=imaxk0+j+1
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky
               indice=i+nfft2d2+1+nfft2d*(j+nfft2d2)
               Ediffkzneg(ii,jj,1)=Efourierxneg(indice)
               Ediffkzneg(ii,jj,2)=Efourieryneg(indice)
               Ediffkzneg(ii,jj,3)=Efourierzneg(indice)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL           
      endif

      
      return
      end
