c     propagateur d espace libre
      subroutine propa_espace_librec(x,y,z,x0,y0,z0,k0,epsh,
     $     propaesplibre)
      implicit none
      integer i,j
      double precision x,y,z,x0,y0,z0,k0,Id,Rab,Rtenseur,Rvect,pi,Rab2
      double complex propaesplibre,const1,const2,const3,epsh,k0c
      dimension propaesplibre(3,3),Id(3,3),Rtenseur(3,3),Rvect(3)

      pi=dacos(-1.d0)
      Rab2=0.d0
      Rvect(1)=(x-x0)
      Rvect(2)=(y-y0)
      Rvect(3)=(z-z0)

      k0c=k0*cdsqrt(epsh)
      
      do i=1,3
         do j=1,3
            Id(i,j)=0.d0
            if (i.eq.j) Id(i,i)=1.d0
            Rtenseur(i,j)=Rvect(i)*Rvect(j)
         enddo
         Rab2=Rab2+Rvect(i)*Rvect(i)
      enddo
      Rab=dsqrt(Rab2)

      if (Rab.eq.0.d0) then
         do i=1,3
            do j=1,3
               propaesplibre(i,j)=(0.d0,0.d0) 
	   enddo
         enddo
      else
         const1=((1.d0,0.d0)/Rab-(0.d0,1.d0)*k0c)/Rab2
         const2=k0c*k0c/Rab*(1.d0,0.d0)
         const3=cdexp((0.d0,1.d0)*k0c*Rab)/epsh
         do i=1,3
            do j=1,3
               propaesplibre(i,j)=((3.d0*Rtenseur(i,j)/Rab2-Id(i,j))
     $              *const1+(Id(i,j)-Rtenseur(i,j)/Rab2)*const2)
     $              *const3
            enddo
         enddo
      endif
      end
