      subroutine saveincidentfield(nmat,FF0,tabdip,ndipole,nmax,nlar,wrk
     $     ,file_id,group_idnf)

#ifdef USE_HDF5
      use HDF5
#endif
      
      implicit none
      integer nmat,ndipole,nmax,tabdip(nmax),i,k,ii,nlar

      double complex FF0(3*nmax),wrk(3*nmax,nlar)

#ifndef USE_HDF5
      integer,parameter:: hid_t=4
#endif   
      character(LEN=100) :: datasetname
      integer(hid_t) :: file_id
      integer(hid_t) :: group_idnf
      integer :: dim(4)

      
      if (nmat.eq.1) return
      if (nmat.eq.0) then

         open(540,file='incidentfieldxlin.mat')
         open(541,file='incidentfieldylin.mat')
         open(542,file='incidentfieldzlin.mat')
         open(543,file='incidentfieldlin.mat')

      do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)                   
               write(540,*) dreal(FF0(ii+1)),dimag(FF0(ii+1))
               write(541,*) dreal(FF0(ii+2)),dimag(FF0(ii+2))
               write(542,*) dreal(FF0(ii+3)),dimag(FF0(ii+3))
               write(543,*) dsqrt(dreal(FF0(ii+1)
     $              *dconjg(FF0(ii+1))+FF0(ii+2)
     $              *dconjg(FF0(ii +2))+FF0(ii+3)
     $              *dconjg(FF0(ii+3))))
            else
               write(540,*) 0.d0,0.d0
               write(541,*) 0.d0,0.d0
               write(542,*) 0.d0,0.d0
               write(543,*) 0.d0
            endif
         enddo

        

         close(540)
         close(541)
         close(542)
         close(543)
         
      elseif (nmat.eq.2) then

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii)   
!$OMP DO SCHEDULE(STATIC)        
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)       
               wrk(i,1)=FF0(ii+1)
               wrk(i,2)=FF0(ii+2)
               wrk(i,3)=FF0(ii+3)
               wrk(i,4)= dsqrt(dreal(FF0(ii+1)*dconjg(FF0(ii+1))+FF0(ii
     $              +2)*dconjg(FF0(ii+2))+FF0(ii+3)*dconjg(FF0(ii+3))))
            else
               wrk(i,1)=0.d0
               wrk(i,2)=0.d0
               wrk(i,3)=0.d0
               wrk(i,4)=0.d0 
            endif
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
         dim(1)=ndipole
         dim(2)=nmax*3
         datasetname='Linear incident field modulus'
         call hdf5write1d(group_idnf,datasetname,dreal(wrk(:,4)),dim)
         datasetname='Linear incident field x component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(: ,1)),dim)
         datasetname='Linear incident field x component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,1)),dim)
         datasetname='Linear incident field y component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,2)),dim)
         datasetname='Linear incident field y component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,2)),dim)
         datasetname='Linear incident field z component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,3)),dim)
         datasetname='Linear incident field z component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,3)),dim)
      endif
      
      return
      end
c     ************************************************************
      subroutine savelocalfield(nmat,FFloc,tabdip,ndipole,nmax,wrk
     $     ,file_id,group_idnf)

#ifdef USE_HDF5
      use HDF5
#endif
      
      implicit none
      integer nmat,ndipole,nmax,tabdip(nmax),i,k,ii

      double complex FFloc(3*nmax),wrk(3*nmax,12)

#ifndef USE_HDF5
      integer,parameter:: hid_t=4
#endif   
      character(LEN=100) :: datasetname
      integer(hid_t) :: file_id
      integer(hid_t) :: group_idnf
      integer :: dim(4)

      
      if (nmat.eq.1) return
      if (nmat.eq.0) then

         open(540,file='localfieldxlin.mat')
         open(541,file='localfieldylin.mat')
         open(542,file='localfieldzlin.mat')
         open(543,file='localfieldlin.mat')

         
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)                   
               write(540,*) dreal(FFloc(ii+1)),dimag(FFloc(ii+1))
               write(541,*) dreal(FFloc(ii+2)),dimag(FFloc(ii+2))
               write(542,*) dreal(FFloc(ii+3)),dimag(FFloc(ii+3))
               write(543,*) dsqrt(dreal(FFloc(ii+1)
     $              *dconjg(FFloc(ii+1))+FFloc(ii+2)
     $              *dconjg(FFloc(ii +2))+FFloc(ii+3)
     $              *dconjg(FFloc(ii+3))))
            else
               write(540,*) 0.d0,0.d0
               write(541,*) 0.d0,0.d0
               write(542,*) 0.d0,0.d0
               write(543,*) 0.d0
            endif
         enddo

         close(540)
         close(541)
         close(542)
         close(543)
         
      elseif (nmat.eq.2) then

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii)   
!$OMP DO SCHEDULE(STATIC)        
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)       
               wrk(i,1)=FFloc(ii+1)
               wrk(i,2)=FFloc(ii+2)
               wrk(i,3)=FFloc(ii+3)
               wrk(i,4)= dsqrt(dreal(FFloc(ii+1)*dconjg(FFloc(ii+1))
     $              +FFloc(ii+2)*dconjg(FFloc(ii+2))+FFloc(ii+3)
     $              *dconjg(FFloc(ii+3)))) 
            else
               wrk(i,1)=0.d0
               wrk(i,2)=0.d0
               wrk(i,3)=0.d0
               wrk(i,4)=0.d0 
            endif
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
         dim(1)=ndipole
         dim(2)=nmax*3
         datasetname='Linear local field modulus'
         call hdf5write1d(group_idnf,datasetname,dreal(wrk(:,4)),dim)
         datasetname='Linear local field x component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(: ,1)),dim)
         datasetname='Linear local field x component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,1)),dim)
         datasetname='Linear local field y component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,2)),dim)
         datasetname='Linear local field y component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,2)),dim)
         datasetname='Linear local field z component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,3)),dim)
         datasetname='Linear local field z component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,3)),dim)
      endif
      
      return
      end
c******************************************************
      subroutine savemacrofield(nmat,FFloc,tabdip,ndipole,nmax,wrk,
     $     aretecube,k0,zs,neps,nepsmax,zcouche,epscouche,epsilon
     $     ,file_id ,group_idnf)

#ifdef USE_HDF5
      use HDF5
#endif

      implicit none
      integer nmat,ndipole,nmax,tabdip(nmax),nsens,neps,nepsmax,i,k,ii
     $     ,jj,numerocouche
      double precision  aretecube,k0,zs(nmax)
      double complex FFloc(3*nmax),wrk(3*nmax,12),Eloc(3),Em(3),eps0
     $     ,epsani(3,3),zcouche(0:nepsmax),epscouche(0:nepsmax+1)
      double complex epsilon(nmax,3,3)


#ifndef USE_HDF5
      integer,parameter:: hid_t=4
#endif   
      
      character(LEN=100) :: datasetname
      integer(hid_t) :: file_id
      integer(hid_t) :: group_idnf
      integer :: dim(4)

      
      if (nmat.eq.1) return
      nsens=1

      if (nmat.eq.0) then

         open(540,file='macroscopicfieldxlin.mat')
         open(541,file='macroscopicfieldylin.mat')
         open(542,file='macroscopicfieldzlin.mat')
         open(543,file='macroscopicfieldlin.mat')

         
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)
               Eloc(1)= FFloc(ii+1)
               Eloc(2)= FFloc(ii+2)
               Eloc(3)= FFloc(ii+3)
               do ii=1,3
                  do jj=1,3
                     epsani(ii,jj)=epsilon(k,ii,jj)
                  enddo
               enddo 
               eps0=epscouche(numerocouche(zs(k),neps,nepsmax
     $              ,zcouche))
               call local_macro_surf(Eloc,Em,epsani,eps0,aretecube,k0
     $              ,nsens)
               
               write(540,*) dreal(Em(1)),dimag(Em(1))
               write(541,*) dreal(Em(2)),dimag(Em(2))
               write(542,*) dreal(Em(3)),dimag(Em(3)) 
               write(543,*) dsqrt(dreal(Em(1)*dconjg(Em(1))+Em(2)
     $              *dconjg(Em(2))+Em(3)*dconjg(Em(3))))
            else
               write(540,*) 0.d0,0.d0
               write(541,*) 0.d0,0.d0
               write(542,*) 0.d0,0.d0
               write(543,*) 0.d0
            endif
         enddo

         close(540)
         close(541)
         close(542)
         close(543)
         
      elseif (nmat.eq.2) then

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(i,k,ii,jj,Eloc,epsani,Em,eps0)
!$OMP DO SCHEDULE(STATIC)        
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)  
               Eloc(1)= FFloc(ii+1)
               Eloc(2)= FFloc(ii+2)
               Eloc(3)= FFloc(ii+3)
               do ii=1,3
                  do jj=1,3
                     epsani(ii,jj)=epsilon(k,ii,jj)
                  enddo
               enddo
               eps0=epscouche(numerocouche(zs(k),neps,nepsmax ,zcouche))
               call local_macro_surf(Eloc,Em,epsani,eps0 ,aretecube,k0
     $              ,nsens)
               wrk(i,1)=Em(1)
               wrk(i,2)=Em(2)
               wrk(i,3)=Em(3)
               wrk(i,4)=dsqrt(dreal(Em(1)*dconjg(Em(1))+Em(2)
     $              *dconjg(Em(2))+Em(3)*dconjg(Em(3))))
            else
               wrk(i,1)=0.d0
               wrk(i,2)=0.d0
               wrk(i,3)=0.d0
               wrk(i,4)=0.d0 
            endif
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
         dim(1)=ndipole
         dim(2)=nmax*3

         datasetname='Linear macroscopic field modulus'
         call hdf5write1d(group_idnf,datasetname,dreal(wrk(:,4)),dim)
         datasetname='Linear macroscopic field x component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(: ,1)),dim)
      datasetname='Linear macroscopic field x component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,1)),dim)
         datasetname='Linear macroscopic field y component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,2)),dim)
      datasetname='Linear macroscopic field y component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,2)),dim)
         datasetname='Linear macroscopic field z component real part'
         call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,3)),dim)
      datasetname='Linear macroscopic field z component imaginary part'
         call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,3)),dim)
      endif
      
      return
      end

