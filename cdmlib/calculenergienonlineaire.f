c     subroutine qui calcul l'energie en réflexion et tranmission dans
c     le cas non linéaire a partir des champs diffractés 
      subroutine calculenergienonlineaire(imax,nfft2d,nfft2dmax,k0
     $     ,deltakx,deltaky,indice0,indicen,Efourierincxpos
     $     ,Efourierincypos,Efourierinczpos ,Efourierincxneg
     $     ,Efourierincyneg,Efourierinczneg ,Efourierxpos ,Efourierypos
     $     ,Efourierzpos,Efourierxneg ,Efourieryneg ,Efourierzneg
     $     ,fluxref,fluxtrans,nstop ,infostr)
      implicit none

c     INPUT
      integer nfft2dmax,nfft2d,imax
      double precision k0,deltakx ,deltaky,indice0,indicen
      double complex Efourierincxpos(nfft2d*nfft2d)
     $     ,Efourierincypos(nfft2d*nfft2d),Efourierinczpos(nfft2d
     $     *nfft2d),Efourierincxneg(nfft2d*nfft2d)
     $     ,Efourierincyneg(nfft2d*nfft2d),Efourierinczneg(nfft2d
     $     *nfft2d),Efourierxpos(nfft2d*nfft2d),Efourierypos(nfft2d
     $     *nfft2d),Efourierzpos(nfft2d*nfft2d),Efourierxneg(nfft2d
     $     *nfft2d),Efourieryneg(nfft2d*nfft2d),Efourierzneg(nfft2d
     $     *nfft2d)


c     OUTPUT
      integer nstop
      character(64) infostr
      double precision fluxtrans,fluxref

c     INTERNE VARIABLE
      integer i,j,ii,jj,indice,nfft2d2
      double precision kx,ky,kp2,kz,tmp,pi
      double complex const

      pi=dacos(-1.d0)
      nfft2d2=nfft2d/2  
      fluxtrans=0.d0
      fluxref=0.d0
      write(*,*) 'imax',imax,indice0,indicen,k0,deltakx,deltaky
      
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,ii,jj,kx,ky,kp2,kz) 
!$OMP& PRIVATE(indice,const)
!$OMP DO SCHEDULE(STATIC) REDUCTION(+:fluxtrans,fluxref) COLLAPSE(2)
      do i=-imax,imax
         do j=-imax,imax
            ii=imax+i+1
            jj=imax+j+1
            kx=dble(i)*deltakx
            ky=dble(j)*deltaky
            kp2=kx*kx+ky*ky
            if (indicen*indicen*k0*k0-kp2.gt.0.d0) then
               kz=dsqrt(indicen*indicen*k0*k0-kp2)
               indice=i+nfft2d2+1+nfft2d*(j+nfft2d2) 
               fluxtrans=fluxtrans+(cdabs(Efourierincxpos(indice))**2
     $              +cdabs(Efourierincypos(indice))**2
     $              +cdabs(Efourierinczpos(indice))**2)*kz
c               write(*,*) 'tra',(cdabs(Efourierincxpos(indice))**2
c     $              +cdabs(Efourierincypos(indice))**2
c     $              +cdabs(Efourierinczpos(indice))**2),ii,jj,fluxtrans
            endif
            if (indice0*indice0*k0*k0-kp2.gt.0.d0) then
               kz=dsqrt(indice0*indice0*k0*k0-kp2)
               indice=i+nfft2d2+1+nfft2d*(j+nfft2d2) 
               fluxref=fluxref+(cdabs(Efourierincxneg(indice))**2
     $              +cdabs(Efourierincyneg(indice))**2
     $              +cdabs(Efourierinczneg(indice))**2)*kz
c               write(*,*) 'ref',ii,jj,fluxref
            endif
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
c      write(*,*) 'ffffffff',fluxref,fluxtrans
      tmp=4.d0*pi*pi*deltaky*deltakx/(k0*8.d0*pi*1.d-7*299792458.d0)
      write(*,*) 'rrr',tmp,pi,deltaky,deltakx,k0
      fluxref=fluxref*tmp
      fluxtrans=fluxtrans*tmp
      write(*,*) 'ffffffff',fluxref,fluxtrans
      return
      end
