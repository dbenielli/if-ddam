c$$$      implicit none
c$$$      integer LDA,NDIM,NLAR,NOU,STATUS,STEPERR,NLOOP,MAXIT,i,j,IM
c$$$      parameter(lda=200,nlar=12)
c$$$      double precision NORM,TOL,TOLE
c$$$      double complex XI(lda),XR(lda),B(lda),WRK(lda,nlar) ,mat(lda,lda)
c$$$     $     ,diago(lda),ALPHA,BETA,ETA,DZETA,R0RN,icomp
c$$$      
c$$$  
c$$$      ndim=10
c$$$      icomp=(0.d0,1.d0)
c$$$      do i=1,ndim
c$$$         do j=1,ndim
c$$$            mat(i,j)=0.d0
c$$$         enddo
c$$$         mat(i,i)=(1.d0,0.d0)*dble(i)*icomp
c$$$         b(i)=(1.d0,0.d0)
c$$$         diago(i)=1.d0
c$$$         xi(i)=(0.d0,0.d0)
c$$$      enddo
c$$$      
c$$$      tol=1.d-4
c$$$      tole=tol
c$$$      nloop=0
c$$$      nou=0
c$$$      MAXIT=10
c$$$      
c$$$      do i=1,ndim
c$$$         b(i)=b(i)*diago(i)
c$$$         xi(i)=xi(i)/diago(i)
c$$$      enddo
c$$$      
c$$$ 10   call ssbicgsafe2(Xi,XR,B,lda,ndim,nlar,nou,WRK,NLOOP,MAXIT ,TOLE
c$$$     $     ,NORM,ALPHA,BETA,ETA,DZETA,R0RN,STATUS,STEPERR)
c$$$      
c$$$c      write(*,*) 'donnees',nloop,nou,ALPHA,BETA,ETA,DZETA,R0RN
c$$$      
c$$$c      write(*,*) 'TOL',tol,STATUS
c$$$      if (STATUS.lt.0) then
c$$$         write(*,*) 'stop nstat',STATUS,STEPERR
c$$$         stop
c$$$      endif
c$$$      
c$$$      do i=1,ndim
c$$$         xi(i)=xi(i)*diago(i)
c$$$      enddo
c$$$      
c$$$      do i=1,ndim
c$$$         xr(i)=0.d0
c$$$         do j=1,ndim
c$$$            xr(i)=xr(i)+mat(i,j)*xi(j)
c$$$         enddo
c$$$c         write(*,*) 'xr xi',xr(i),xi(i)
c$$$      enddo
c$$$      
c$$$      do i=1,ndim
c$$$         xr(i)=xr(i)*diago(i)
c$$$      enddo
c$$$      
c$$$      
c$$$      if (STATUS.ne.1) goto 10
c$$$  
c$$$      
c$$$      write(*,*) '********************',NLOOP,tole
c$$$      DO I=1,NDIM
c$$$         WRITE(*,*) 'SOL',Xi(I),1.d0/mat(i,i)
c$$$      ENDDO
c$$$
c$$$c     calcul erreur relative
c$$$      tole=0.d0
c$$$      do i=1,ndim
c$$$         tole=tole+cdabs(xr(i)-b(i))
c$$$      enddo
c$$$      tole=tole/norm
c$$$      write(*,*) 'tole',tole
c$$$      stop
c$$$      if (tole.ge.tol) goto 10
c$$$
c$$$  
c$$$      end
c*************************************************
c     Viet Hutnh and Hiroshi Suito
      SUBROUTINE ssbicgsafe2(Xi,XR,B,lda,ndim,nlar,nou,WRK,ITNO,MAXIT
     $     ,TOL,NORM,ALPHA,BETA,ETA,DZETA,ffa,STATUS,STEPERR)
      IMPLICIT NONE

      INTEGER ii,nou,ndim,ITNO,LDA,MAXIT,STATUS,STEPERR,nlar
      DOUBLE COMPLEX B(lda),WRK(lda,nlar),xi(lda),xr(lda)

*     .. Local Scalars ..
      DOUBLE COMPLEX ALPHA,BETA,ETA,DZETA,aa,bb,cc,dd,ee,ff,gg,hh,ctmp1
     $     ,ctmp2,ctmp3,ctmp4,ffa
      DOUBLE PRECISION TOL,NORM,RESIDU
c      write(*,*) 'NORM',NORM
      IF (NOU.EQ.1) GOTO 10
      IF (NOU.EQ.2) GOTO 20
      IF (NOU.EQ.3) GOTO 30
      IF (NOU.EQ.4) GOTO 40

      STATUS = 0
      STEPERR = -1

c     1:r0
c     2:p
c     3:r
c     4:y
c     5:t
c     6:s=Ar
c     7:o
c     8:u
c     9:w=Au
c     10:z
c     11:x
c     12:t ancien

c     calcul norme et Ax0
      NORM=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:NORM)
      do ii=1,ndim
         WRK(ii,11)=xi(ii)
         NORM=NORM+cdabs(B(ii))**2.d0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
     
      NORM=dsqrt(NORM)

      nou=1
      return

c     initialise r0=b-Ax0,rOt=rO, p=u=t=y=z=0
 10   ctmp1=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)  REDUCTION(+:ctmp1)     
      do ii=1,ndim
         WRK(ii,1)=B(ii)-xr(ii)
         WRK(ii,3)=WRK(ii,1)
         WRK(ii,2)=0.d0
         WRK(ii,4)=0.d0
         WRK(ii,5)=0.d0
         WRK(ii,8)=0.d0
         WRK(ii,10)=0.d0
         ctmp1=ctmp1+WRK(ii,1)*dconjg(WRK(ii,1))
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

c     calcul residu initial
      
      write(*,*) 'residu initial',cdabs(cdsqrt(ctmp1))/norm

c     commence la boucle
      ITNO=-1
 100  ITNO=ITNO+1
c      write(*,*) 'ITNO',ITNO

c***************************
c     calcul s=Ar
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO SCHEDULE(STATIC)
      do ii=1,ndim
         xi(ii)=WRK(ii,3)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL       
      nou=2
      return

c     met s=Ar en mémoire
 20   aa=0.d0
      bb=0.d0
      cc=0.d0
      dd=0.d0
      ee=0.d0
      ff=0.d0
      gg=0.d0
      hh=0.d0
       
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO SCHEDULE(STATIC)
      do ii=1,ndim
         WRK(ii,6)=xr(ii)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL  
c***************************

      if (ITNO.eq.0) then

c     calcul  f=(r0*,r), d=(s,r), g=(r0*,s), a=(s,s)
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ff,dd,gg,aa)          
         do ii=1,ndim
            ff=ff+dconjg(WRK(ii,1))*WRK(ii,3)
            dd=dd+dconjg(WRK(ii,6))*WRK(ii,3)
            gg=gg+dconjg(WRK(ii,1))*WRK(ii,6)
            aa=aa+dconjg(WRK(ii,6))*WRK(ii,6)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         if (gg.eq.0.d0.or.aa.eq.0.d0) then
            STATUS=-1
            STEPERR=1
            return 
         endif
         
c     calcul de alpha=r0r/r0s
         ALPHA=ff/gg
         DZETA=dd/aa
         BETA=0.d0
         ETA=0.d0

         ffa=ff
      else 

c     calcul f=(r0*,r), d=(s,r), g=(r0*,s), a=(s,s)
c     calcul b=(y,y), c=(s,y), e=(y,r) h=(r0*,ti-1)

!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ff,dd,gg,aa,bb,cc,ee,hh)
         do ii=1,ndim
            ff=ff+dconjg(WRK(ii,1))*WRK(ii,3)
            dd=dd+dconjg(WRK(ii,6))*WRK(ii,3)
            gg=gg+dconjg(WRK(ii,1))*WRK(ii,6)
            aa=aa+dconjg(WRK(ii,6))*WRK(ii,6)

            bb=bb+dconjg(WRK(ii,4))*WRK(ii,4)
            cc=cc+dconjg(WRK(ii,6))*WRK(ii,4)
            ee=ee+dconjg(WRK(ii,4))*WRK(ii,3)
            hh=hh+dconjg(WRK(ii,1))*WRK(ii,5)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         ctmp1=DZETA*ffa
         if (ctmp1.eq.0.d0) then
            STATUS=-1
            STEPERR=2
            return 
         endif
         BETA=ALPHA*ff/ctmp1

         ctmp1=gg+BETA*hh

         if (ctmp1.eq.0.d0) then
            STATUS=-1
            STEPERR=3
            return 
         endif

         ALPHA=ff/ctmp1

         ctmp1=aa*bb-dconjg(cc)*cc
         if (ctmp1.eq.0.d0) then
            STATUS=-1
            STEPERR=4
            return 
         endif
         
         DZETA=(bb*dd-cc*ee)/ctmp1
         ETA=(aa*ee-dconjg(cc)*dd)/ctmp1
         ffa=ff
      endif


c     calcul de p=r+beta*(p-u) et o=s+beta*t et u=dzeta*o+eta*(y+beta*u)
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) 
      do ii=1,ndim
         WRK(ii,2)=WRK(ii,3)+BETA*(WRK(ii,2)-WRK(ii,8))
         WRK(ii,7)=WRK(ii,6)+BETA*WRK(ii,5)
         WRK(ii,8)=DZETA*WRK(ii,7)+ETA*(WRK(ii,4)+beta*WRK(ii,8))
         xi(ii)=WRK(ii,8)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      nou=3
      return
c     calcul de w=Ap
 30   RESIDU=0.d0


c     calcul de t=o-w,z=dzeta*r+eta*z-alpha*u,y=dzeta*s+eta*y-alpha*w,
c     x=x+alpha*p+z et r=r-alpha*o-y
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:RESIDU) 
      do ii=1,ndim
         WRK(ii,9)=xr(ii)
         WRK(ii,5)=WRK(ii,7)-WRK(ii,9)
         WRK(ii,10)=DZETA*WRK(ii,3)+ETA*WRK(ii,10)-ALPHA*WRK(ii,8)
         WRK(ii,4)=DZETA*WRK(ii,6)+ETA*WRK(ii,4)-ALPHA*WRK(ii,9)
         WRK(ii,11)=WRK(ii,11)+ALPHA*WRK(ii,2)+WRK(ii,10)
         WRK(ii,3)=WRK(ii,3)-ALPHA*WRK(ii,7)-WRK(ii,4)
         RESIDU=RESIDU+cdabs(WRK(ii,3))**2.d0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      RESIDU=dsqrt(RESIDU)/NORM
c     if (mod(ITNO,50).EQ.0) write(*,*) 'RESIDU',RESIDU
      write(*,*) 'RESIDU',RESIDU,ITNO
      if (RESIDU.le.TOL) then
         STATUS=1
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)        
         do ii=1,ndim
            xi(ii)=WRK(ii,11)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
         nou=4
         return
 40   endif

      if (ITNO.le.MAXIT) goto 100

      STATUS=1
!$OMP PARALLEL   DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)        
      do ii=1,ndim
         xi(ii)=WRK(ii,11)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL   

      END
